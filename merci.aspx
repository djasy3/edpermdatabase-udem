﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="merci.aspx.cs" Inherits="merci" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title> Merci &agrave; vous </title>
        <link rel="stylesheet" type="text/css" href="App_Themes/css/skeleton.css" media="all"/>
        <link rel="stylesheet" type="text/css" href="App_Themes/css/merci.css" media="all"/>
</head>
<body>
    <form id="form1" runat="server">
<div id="wrapper">
       <div class="container">
             <!-- brand slogan starts -->
            <div class="offset-by-three ten columns brand_slogan">
                <p class="twelve columns brand">
                    &Eacute;ducation permanente
                </p> <!-- brand or institution name -->
                <p class="offset-by-three ten columns slogan">
                    L'universit&eacute; en tout temps.
                </p> <!-- slogan -->
            </div> <!-- brand_slogan -->

            <div class="twelve columns thanks_note">
                <p> Merci beaucoup &agrave; vous :) </p>
            </div><!-- end thanks note -->

             <!-- footer starts -->
            <div class="twelve columns footer">
                <a class="one column" href="http://www.umoncton.ca"><img src="images/logo_udem.png"></a>
                <p class="five columns"> &copy; 2015, Universit&eacute; de Moncton.
                 Tous droits r&eacute;serv&eacute;s</p>
            </div> <!-- footer -->
         </div> <!-- container -->
     </div> <!-- end wrapper -->
    </form>
</body>
</html>
