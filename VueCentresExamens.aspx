﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermProd.master" AutoEventWireup="true" CodeFile="VueCentresExamens.aspx.cs" Inherits="VueCentresExamens" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="App_Themes/css/detail_vue.css" rel="stylesheet"/>
    <script src="App_Themes/js/views.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ajax" runat="server" />
    <asp:UpdatePanel ID="updtPanel" runat="server">
        <ContentTemplate>
            <div class="ten columns alerts_admin">
        <span><asp:Label ID="va" runat="server" /></span>
    </div><!-- end alerts admin -->
    <p class="twelve columns view_title">
        <span> 
            Prière de cliquer sur un centre d'examen pour accéder aux d&eacute;tails de celui-ci <br />
            <asp:Label ID="erreur" runat="server" ForeColor="Red" />
        </span>
    </p> <!-- end view title -->
    <div class="twelve columns course_filter">
        <span class="three columns filter_title"> Options de filtrage </span>
        <asp:DropDownList ID="lstSession" runat="server" CssClass="two columns" OnSelectedIndexChanged="lstSession_SelectedIndexChanged" AutoPostBack="true"/>
        <asp:DropDownList ID="lstCours" runat="server" CssClass="two columns" OnSelectedIndexChanged="lstCours_SelectedIndexChanged" AutoPostBack="true"/>
        <asp:DropDownList ID="typeExamen" runat="server" CssClass="two columns" OnSelectedIndexChanged="typeExamen_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Value="intra" Selected="True">Intra</asp:ListItem>
            <asp:ListItem Value="final">Final</asp:ListItem>
        </asp:DropDownList>
    </div><!-- end filter -->
    <div class="three columns course_list">
        <asp:ListView ID="lstCentreExams" runat="server"  GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder">
            <LayoutTemplate>
                <ul class="twelve columns">
                     <asp:PlaceHolder ID="gpePlcHolder" runat="server" />
                </ul>
            </LayoutTemplate>
            <GroupTemplate>
                <asp:PlaceHolder ID="itmPlcHolder" runat="server" />
            </GroupTemplate>
            <ItemTemplate>
                <li><asp:LinkButton ID="villeCentreExam" runat="server" Text='<%#Eval("le_ville") %>' CommandArgument='<%#Eval("le_id") %>' ssClass="five columns" OnClick="villeCentreExam_Click" /></li>
            </ItemTemplate>
            <EmptyDataTemplate>
                <ul class="twelve columns">
                    <li>Aucun centre disponible !</li>
                </ul>
            </EmptyDataTemplate>
        </asp:ListView>
    </div><!-- end course list -->
    <div class="nine columns course_gate" style="display:block;">
        <asp:ListView ID="affichageCours" runat="server"  GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder" >
            <LayoutTemplate>
                <ul class="twelve columns gate_child">
                    <asp:PlaceHolder ID="gpePlcHolder" runat="server" />
                </ul>
            </LayoutTemplate>
            <GroupTemplate>
                <asp:PlaceHolder ID="itmPlcHolder" runat="server" />
            </GroupTemplate>
            <ItemTemplate>
                <li class="twelve columns">
                     <div class="twelve columns liRow">
                         <span class="four columns student_name"> <asp:Label ID="nomCours" runat="server" Text='<%#Eval("c_sigle") %>' /> </span><br />
                         <div class="six columns exam_intra">
                             <span class="twelve columns section_title">Type d'examen : <%#Eval("ce_TypeExam") %></span><br />
                             <p class="twelve columns info_couple">
                                <span class="six columns info_name">Superviseur:</span>
                                <span class="six columns info_value"><%#Eval("sup_nom").ToString()+" "+Eval("sup_prenom").ToString() %></span><br />
                             </p>
                             <p class="twelve columns info_couple">
                                <span class="six columns info_name">Couriel du superviseur</span>
                                <span class="six columns info_value"><%#Eval("sup_couriel") %></span><br />
                             </p>
                             <p class="twelve columns info_couple">
                                <span class="six columns info_name">Adresse du superviseur</span>
                                <span class="six columns info_value"><%#Eval("sup_adrsCiv") %></span><br />
                             </p>
                             <p class="twelve columns info_couple">
                                <span class="six columns info_name">Nombre d'étudiants</span>
                                <span class="six columns info_value"><%# SessionAccess.nbreEtudiant(SId, int.Parse(Eval("c_id").ToString())) %> / <%# SessionAccess.nbreEtudiantInscrit(SId, int.Parse(Eval("c_id").ToString())) %></span><br />
                             </p>
                         </div>
                         <div class="six columns exam_final">
                             <span class="twelve columns section_title">Ententes</span>
                             <p class="twelve columns info_couple">
                                <span class="six columns info_name">Copies scannées envoyées</span>
                                <span class="six columns info_value"><asp:CheckBox ID="cpElctrk" runat="server" Checked='<%# Eval("res_cpElectr") %>' OnCheckedChanged="cpElctrk_CheckedChanged" AutoPostBack="true" /></span>
                             </p>
                             <p class="twelve columns info_couple">
                                <span class="six columns info_name">Copies papiers envoyées</span>
                                <span class="six columns info_value"><asp:CheckBox ID="cpPapier" runat="server"  Checked='<%# Eval("res_cpPapier") %>' AutoPostBack="true" OnCheckedChanged="cpPapier_CheckedChanged" /></span>
                             </p>
                             <p class="twelve columns info_couple">
                                <span class="six columns info_name">Trousse envoyée</span>
                                <span class="six columns info_value"><asp:CheckBox ID="trousseSent" runat="server" Checked='<%# Eval("res_trousse") %>' AutoPostBack="true" OnCheckedChanged="trousseSent_CheckedChanged" /></span>
                             </p>
                             <p class="twelve columns info_couple">
                                <span class="six columns info_name">Lieu confirmé</span>
                                <span class="six columns info_value"><asp:CheckBox ID="lieuConfrm" runat="server" Checked='<%# Eval("res_lieu") %>' AutoPostBack="true" OnCheckedChanged="lieuConfrm_CheckedChanged" /></span>
                             </p>
                         </div>
                         <div class="twelve columns note_canvas">
                             <asp:TextBox ID="notes" runat="server" class="twelve columns" placeholder="ecrire ici" Text='<%#Eval("res_note") %>' AutoPostBack="true" OnTextChanged="notes_TextChanged" />
                        </div><!-- end of note canvas -->
                     </div>
                </li>
            </ItemTemplate>
            <EmptyDataTemplate>
                <ul class="twelve columns gate_child">
                    <li class="twelve columns">Aucun examen confirmé pour ce centre</li>
                </ul>
            </EmptyDataTemplate>
        </asp:ListView>
    </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

