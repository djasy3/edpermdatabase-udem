﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermProd.master" AutoEventWireup="true" CodeFile="MontageSession.aspx.cs" Inherits="MontageSession" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="App_Themes/css/form_admin.css"/>
    <title>Montage sessions</title>
    <script type="text/javascript">
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "createSession";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
<asp:ScriptManager ID="ajax" runat="server"/>
        <div class="ten columns alerts">
            <%--<span> Creation du cours, reussie !! alert_admin </span>--%>
        </div><!-- end alerts admin -->
        <span><asp:Label ID="erreur" runat="server" /></span><br />
        <div class="ten columns mount_session"> <!-- This replaces the form tag -->
            <!-- nav tabs to list the steps -->
        <div id="Tabs" role="tabpanel">
            <ul class="nav nav-tabs twelve columns" role="tablist" >
                <li role="presentation" class="active">
                <a href="#createSession" aria-controls="createSession" role="tab" data-toggle="tab">Session</a>
                </li>
                <li role="presentation">
                <a href="#createCourse" aria-controls="createCourse" role="tab" data-toggle="tab">Cours</a>
                </li>
                <li role="presentation">
                <a href="#createStudent" aria-controls="createStudent" role="tab" data-toggle="tab">Étudiants</a>
                </li>
                <li role="presentation">
                <a href="#createMessages" aria-controls="createMessages" role="tab" data-toggle="tab">Messages</a>
                </li>
                <li role="presentation">
                <a href="#createCities" aria-controls="createCities" role="tab" data-toggle="tab">Villes d'examens</a>
                </li>
            </ul> <!-- end of tabs ul -->   
        <div class="tab-content"> <!-- purely for the sake of bootstrap -->
        <div role="tabpanel" class="eight columns tab-pane active first_step" id="createSession">
            <asp:UpdatePanel ID="updtOne" runat="server">
                <ContentTemplate>
                    <span><asp:Label ID="alertSession" runat="server" /></span><br />
                    <p class="twelve columns"><b>Informations sur la session</b></p>
                    <asp:TextBox ID="nom_session" runat="server" CssClass="twelve columns" name="" placeholder="nom de session (ex: Hiver2015)"/>
                    <asp:TextBox ID="annee" runat="server" CssClass="twelve columns" name="" placeholder="ann&eacute;e (ex: 201501 pour Hiver, 201502 pour Print/été, 201503 pour Automne)"/>
                    <asp:Button ID="btnSession" runat="server" Text="Enregistrer une nouvelle sesssion" OnClick="btnSession_Click"/>
                </ContentTemplate>
                <%--<Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSession" />
                </Triggers>--%>
            </asp:UpdatePanel>
        </div> <!-- end first setp -->
        <div role="tabpanel" class="twelve columns tab-pane second_step" id="createCourse">
            <asp:UpdatePanel ID="updtTwo" runat="server">
                <ContentTemplate>
                    <span><asp:Label ID="alertCours" runat="server" /></span><br />
                    <div class="twelve columns import_file">
                    <p class="six columns">
                        <span>Importer à partir d'un fichier excel</span>
                    </p>
                    <asp:FileUpload ID="importFichier" runat="server" CssClass="four columns" />
                    <asp:Button ID="importerCours" runat="server" Text="Importer cours" OnClick="importerCours_Click" />
                </div> <!-- end import file -->
                <div class="six columns insert_manually">
                    <p> Ajouter manuellement le cours </p>
                    <asp:TextBox ID="sigleCours" runat="server" CssClass="twelve columns" placeholder="sigle du cours (ex:INFO1001)" />
                    <asp:TextBox ID="nomCours" runat="server" CssClass="twelve columns"  placeholder="titre du cours (ex:Paradigmes de programmation I)"/>
                    <asp:TextBox ID="dateIntra" runat="server" CssClass="twelve columns" placeholder="date intra (format :AAAA-MM-JJ  HH:MM:SS) "/>
                    <asp:TextBox ID="dateFinal" runat="server" CssClass="twelve columns" placeholder="date final (format :AAAA-MM-JJ  HH:MM:SS) "/>
                    <asp:Button ID="btnAddCours"  runat="server" Text="Ajouter un cours" OnClick="btnAddCours_Click"/>
                    <asp:Button ID="btnUpdtCours" runat="server" Text="Mettre à jour un cours" OnClick="btnUpdtCours_Click" />
                    <asp:Button ID="btnDelCours" runat="server" Text="Effacer un cours" OnClick="btnDelCours_Click" />
                    <asp:Button ID="btnClean" runat="server" Text="Clear" OnClick="btnClean_Click"/>
                </div><!-- end insert manually -->
                <div class="five columns courseInstant">
                    <p> Liste des cours</p>
                    <asp:ListView ID="lstCours2" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder">
                        <LayoutTemplate>
                        <ul class="twelve columns">
                            <asp:PlaceHolder ID="gpePlcHolder" runat="server" />
                        </ul>
                        </LayoutTemplate>
                        <GroupTemplate>
                            <asp:PlaceHolder ID="itmPlcHolder" runat="server" />
                        </GroupTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:LinkButton ID="detailsCours" runat="server" Text='<%# Eval("c_sigle") %>' CommandArgument='<%# Eval("c_id") %>' OnClick="detailsCours_Click" />
                            </li>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <li>Pas de cours disponible</li>
                        </EmptyDataTemplate>
                    </asp:ListView><!-- end DB populated list -->
                </div><!-- end course instant -->
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="importerCours" />
                </Triggers>
            </asp:UpdatePanel>
    </div> <!-- end second setp -->
         
    <div class="twelve columns tab-pane third_step" id="createStudent">
        <asp:UpdatePanel ID="updtThree" runat="server">
            <ContentTemplate>
                <span><asp:Label ID="alertStudent" runat="server" /></span><br />
                <p class="twelve columns step_title">
                    Ajouter les étudiant(e)s à partir d'une liste
                </p>
                <div class="twelve columns">
                    <p class="six columns">
                        <span>Selectionner la session </span>
                    </p>
                    <asp:DropDownList ID="lstSession" runat="server" CssClass="five columns availableCourses" />
                </div>
                <div class="twelve columns">
            <p class="six columns">
                <span>Choisir un cours pour ajouter les étudiants </span>
            </p>
            <asp:DropDownList ID="lstCours" runat="server" CssClass="five columns availableCourses" />
        </div><!-- end div1 -->
        <div class="twelve columns">
            <p class="six columns">
                <span> Veuillez importer la liste des étudiants s.v.p</span>
            </p>
            <asp:FileUpload ID="importListEtudiant" runat="server" CssClass="five columns" />
        </div><!-- end div2 -->
        <asp:Button ID="btnAddStudents" runat="server" Text="Importer la liste des étudiants" OnClick="btnAddStudents_Click"/>
        <div class="twelve columns imported_students">
            <!-- placeholder to show imported students -->
        </div><!-- imported students -->
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID ="btnAddStudents" />
            </Triggers>
        </asp:UpdatePanel>    
    </div> <!-- end of third step -->
        <div class="twelve columns tab-pane fourth_step" id="createMessages">
            <asp:UpdatePanel ID="updtFour" runat="server">
                <ContentTemplate>
                    <span><asp:Label ID="alertMsg" runat="server" /></span><br />
                    <!-- create messages here -->
            <p>Samples des message à envoyer à l'étudiant</p>
            <div class="six columns">
                <asp:TextBox ID="objetMessage" runat="server" CssClass="twelve columns" placeholder="Objet du message" />
                <asp:TextBox ID="corpsMessage" runat="server" CssClass="twelve columns" placeholder="Corps du message" TextMode="MultiLine" />
                <asp:Button ID="btnSent" runat="server" Text="Enregistrer le message" OnClick="btnSent_Click" />
                <asp:Button ID="btnUpdtMsg" runat="server" Text="Modifier le message" OnClick="btnUpdtMsg_Click"/>
                <asp:Button ID="btnDltMsg" runat="server" Text="Effacer le message" OnClick="btnDltMsg_Click" />
                <asp:Button ID="btnClear" runat="server" Text="Reset" OnClick="btnClear_Click" />
            </div>
            <div class="five columns courseInstant">
                <p>Messages déjà enregistrés</p>
                <asp:ListView ID="lstMsgs" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder">
                    <LayoutTemplate>
                        <ul class="twelve columns">
                            <asp:PlaceHolder ID="gpePlcHolder" runat="server" />
                        </ul>
                    </LayoutTemplate>
                    <GroupTemplate>
                        <asp:PlaceHolder ID="itmPlcHolder" runat="server" />
                    </GroupTemplate>
                    <ItemTemplate>
                        <li>
                            <asp:LinkButton ID="lnkbtnMsgs" runat="server" Text='<%#Eval("obj") %>' CommandArgument='<%#Eval("id") %>' OnClick="lnkbtnMsgs_Click" /></li>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <ul class="twelve columns">
                            <li >Pas de message disponible, veuillez en enregistrer</li>
                        </ul> 
                    </EmptyDataTemplate>
                </asp:ListView>
            </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="twelve columns tab-pane fifth_step" id="createCities">
            <asp:UpdatePanel ID="updtFive" runat="server">
                <ContentTemplate>
                    <span><asp:Label ID="alertVilleExam" runat="server" /></span><br />
                    <!-- create cities here -->
            <div class="six columns insert_manually">
            <fieldset>
                <legend>Ajouter une ville d'examen </legend>
                <asp:TextBox ID="m_adres" runat="server" placeholder="Adresse"/>
                <asp:TextBox ID="m_ville" runat="server" placeholder="Ville" />
                <asp:TextBox ID="m_etat" runat="server" placeholder="Province/Etat" />
                <asp:TextBox ID="m_pays" runat="server" placeholder="Pays" />
            </fieldset>
            <asp:Button ID="addnewtown" runat="server" Text="Ajouter une nouvelle ville d'examen" OnClick="addnewtown_Click" />
                <asp:Button ID="updtTown" runat="server" Text="Mettre à jour les info d'une ville" OnClick="updtTown_Click" />
                <asp:Button ID="delTown" runat="server" Text="Effacer la ville" OnClick="delTown_Click" />
                <asp:Button ID="clrTown" runat="server" Text="Reset" OnClick="clrTown_Click" />
                </div>
            <div class="five columns courseInstant">
                <p>Liste des villes disponibles</p>
                <asp:ListView ID="lstVilleDispo" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder" >
                <LayoutTemplate>
                <ul class="twelve columns">
                    <asp:PlaceHolder ID="gpePlcHolder" runat="server" />
                </ul>
                </LayoutTemplate>
                <GroupTemplate>
                    <asp:PlaceHolder ID="itmPlcHolder" runat="server" />
                </GroupTemplate>
                <ItemTemplate>
                    <li>
                        <asp:LinkButton ID="detailsVilles" runat="server" Text='<%# Eval("le_ville") %>' CommandArgument='<%# Eval("le_id") %>' OnClick="detailsVilles_Click" />
                    </li>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <li>Aucune ville enregistrée</li>
                </EmptyDataTemplate>
                </asp:ListView>
            </div>
        </div>
                </ContentTemplate>
            </asp:UpdatePanel>
    </div><!-- end of tab-content -->
    </div>   
    </div>
            </div>
    <asp:HiddenField ID="TabName" runat="server" />
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
</asp:Content>

