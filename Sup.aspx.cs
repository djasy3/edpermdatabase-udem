﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Sup2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentSession sess = SessionAccess.getCurrentSession();
        if (!IsPostBack)
        {
            lst_cours.DataSource = SessionAccess.lstCoursRsrve(sess.Id);
            lst_cours.DataTextField = "c_sigle";
            lst_cours.DataValueField = "ce_exam";
            lst_cours.DataBind();
            //
            lst_ville.DataSource = SessionAccess.lstVilleRsrve(sess.Id);
            lst_ville.DataTextField = "le_ville";
            lst_ville.DataValueField = "ce_lieu";
            lst_ville.DataBind();
        }
    }
}