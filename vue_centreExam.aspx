﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="vue_cours.aspx.cs" Inherits="vue_cours" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title> Vue des centres d'examen </title>
        <!-- CSS -->
    <link href="App_Themes/css/bootstrap.css" rel="stylesheet" />
    <link href="App_Themes/css/skeleton.css" rel="stylesheet" />
    <link href="App_Themes/css/flaticon.css" rel="stylesheet" />
    <link href="App_Themes/css/scrollpane.css" rel="stylesheet" />
    <link href="App_Themes/css/frame.css" rel="stylesheet" />
    <link href="App_Themes/css/course_view.css" rel="stylesheet"/>
        <!-- JS -->
    <script src="App_Themes/js/jquery.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <!-- <script type="text/javascript" src="./js/html5shiv.js"></script> -->
        <!-- <script type="text/javascript" src="./js/modernizr-latest.js"></script> -->
</head>
<body>
    <form id="form1" runat="server">
<div id="wrapper">
          <header class="blue_bar">
            <div class="container">
                <div class="three columns logo">
                    <p class="">
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">
               <div class="ten columns alerts_admin">
                 <span> Creation du cours, reussie !! </span>
               </div><!-- end alerts admin -->
               <p class="twelve columns view_title">
                   <span> Prière de cliquer sur un centre d'examen pour accéder aux d&eacute;tails de celui-ci </span>
               </p> <!-- end view title -->
                <div class="twelve columns course_filter">
                     <span class="three columns filter_title"> Options de filtrage </span>
                    <asp:DropDownList ID="DropDownList3" runat="server" CssClass="two columns"/>
                    <asp:DropDownList ID="DropDownList4" runat="server" CssClass="two columns"/>
                    <asp:DropDownList ID="list3" runat="server" CssClass="two columns"/>
                    <asp:DropDownList ID="DropDownList2" runat="server" CssClass="two columns"/>
                   <!--  <input type="button" value="critere_1">
                     <input type="button" value="critere_2">
                     <input type="button" value="critere_3">
                     <input type="button" value="getTypeExamasdafs"> -->
                </div><!-- end filter -->
                <div class="seven columns course_list" style="display:block;">
                    <ul class="twelve columns">
                        <li>
                            <a class="five columns" href="detailsCentreExam.aspx"> Bathurst </a>
                            <div class="offset-by-three four columns summary">
                                <span style=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                            </div>
                        </li>
                        <li>
                            <a class="five columns" href="#"> Campbelton </a>
                            <div class="offset-by-three four columns summary">
                                <span style=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                            </div>
                        </li>
                        <li> <a href="#"> Edmundston </a></li>
                        <li> <a href="#"> Fredericton </a></li>
                        <li> <a href="#"> Moncton </a></li>
                        <li> <a href="#"> Shippagan </a></li>
                        <li> <a href="#"> Montreal </a></li>
                        <li> <a href="#"> Ouagadougou </a></li>
                    </ul>
                </div><!-- end course list -->
               
              </div><!-- container -->
            </div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
            <footer> <!-- release the display to see the footer -->
               <div class="container">
                <div class="one column logo">
                        <a href="#">
                            <img src="images/logo_udem_foot.png"/>
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="four columns copyright">
                    <span class="twelve columns"> &copy;copyright 2015 - tous droits reservés </span>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div> <!-- wrapper -->
    </form>
</body>
</html>
