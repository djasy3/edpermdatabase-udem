﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="prod_insc_etudiant.aspx.cs" Inherits="prod_insc_etudiant" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<title> Renseignements etudiant(e)s et superviseur(e)s</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
        
        <!-- CSS -->
    <link href="App_Themes/css/bootstrap.css" rel="stylesheet" />
    <link href="App_Themes/css/skeleton.css" rel="stylesheet" />
    <link href="App_Themes/css/flaticon.css" rel="stylesheet" />
    <link href="App_Themes/css/scrollpane.css" rel="stylesheet" />
    <link href="App_Themes/css/frame.css" rel="stylesheet" />
    <link href="App_Themes/css/form_etudiant.css" rel="stylesheet"/>
        <!-- JS -->
    <script src="App_Themes/js/jquery.js" type="text/javascript"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="App_Themes/js/custom_validate.js" type="text/javascript"></script>
    <script src="App_Themes/js/form_etudiant.js" type="text/javascript"></script>

        <!-- <script type="text/javascript" src="./js/html5shiv.js"></script> -->
        <!-- <script type="text/javascript" src="./js/modernizr-latest.js"></script> -->
</head>
<body id="Body1" runat="server">
    <form id="inscription_etudiant_form" runat="server">
        <asp:ScriptManager ID="ajax" runat="server" />
<div id="wrapper" runat="server">
          <header class="blue_bar">
            <div class="container">
                <div class="three columns logo">
                    <p>
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="three columns current_section">
                    <p class="" style="visibility:hidden;">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p>
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p>
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">
                <div class="ten columns studentBooking"> <!-- previously a form tag -->
 <!-- *********************************** coordonnées de l'etudiant **************************************** -->
          <div class="twelve columns personal_infos" style="display:;">
            <p> Informations personnelles </p>
            <div class="twelve columns inputHolder">
              <asp:TextBox runat="server" ID="nom" CssClass="seven columns last_n" placeholder="Nom" autofocus="autofocus"/>
              <span class="five columns tip last_n"> Nom 2 characters</span>
            </div> <!-- end first name -->
            <div class="twelve columns inputHolder">
                <asp:TextBox runat="server" ID="prenom" CssClass="seven columns first_n" placeholder="Prenom"/>
                <span class="five columns tip first_n"> Prenom 2 characters</span>
            </div>
            <div class="twelve columns inputHolder">
                <asp:TextBox runat="server" ID="ni" CssClass="seven columns ni" name="" type="text"  placeholder="NI"/>
                <span class="five columns tip ni"> Entrer selon le modele: A00XXXXXX</span>
            </div>
            <div  class="twelve columns inputHolder">
                <asp:TextBox runat="server" ID="email" CssClass="seven columns email" type="text" placeholder="Email"/>
                <span class="five columns tip email"> b@cde.xyz</span>
            </div>
            <div class="twelve columns inputHolder">
                <asp:TextBox ID="adresse" runat="server" CssClass="seven columns adresse" placeholder="Adresse"/>
                <!-- <span class="five columns tip"> at least 2 characters</span> -->
             </div>
            <div class="twelve columns inputHolder">
                <asp:TextBox runat="server" ID="phone_jour" CssClass="seven columns tel_j" type="text" placeholder="Telephone jour"/>
                <span class="five columns tip tel_j"> at least 2 characters</span>
            </div>
            <div  class="twelve columns inputHolder">
                <asp:TextBox runat="server" ID="phone_nuit" CssClass="seven columns tel_n" type="text" placeholder="Telephone nuit"/>
                <!-- <span class="five columns tip tel_n"> at least 2 characters</span> -->
              </div>
            <a href="#" class="twelve columns next">
            <span style="display:block;" class="eleven columns first">
                            À propos des examens et superviseurs  </span>
            <!-- <img class="one column" src="./images/next.png" alt="prochaine étape"> -->
            <span id="Span1" style="margin-left:-1.5px;" runat="server" class="flaticon-right127 next-icon one column"></span>
            </a>
            </div> <!-- End of informations personnelles -->

 <!-- *********************************** choix des exams et des villes ************************************* -->
                    <div  class="twelve columns second_panel" style="display:none;">
                    <div class="exam_and_city">
                    <div class="choose_course">
                    <p class="six columns"> Je composerai pour le cours de </p>
                    <asp:DropDownList ID="lstCours" runat="server" CssClass="five columns cours_offerts"></asp:DropDownList>
                    </div> <!-- choix cours -->

                    <div class="twelve columns yesNoSamePlace">
<%--                        <asp:UpdatePanel ID="btnPanel" runat="server">
                            <ContentTemplate>--%>
                    <p class="twelve columns">
                        <span class="eight columns">
                                    Souhaitez-vous composer les examens intra et final à un meme endroit ?
                                  <asp:Label ID="test" runat="server" ForeColor="Red" />  
                         </span>
                                <asp:Button ID="YesBtn" Text="Oui" runat="server" CssClass="two columns yesBoth"
                        data-toggle="collapse" data-target="#is_in_nb" aria-expanded="false"
                                aria-controls="is_in_nb" OnClick="YesBtn_Click" OnClientClick="return false" />
                                <asp:Button ID="NoBtn" type="button" runat="server" CssClass="two columns noBoth" Text="Non"
                        data-toggle="collapse" data-target="#is_out_of_nb" aria-expanded="false"
                                aria-controls="is_out_of_nb" OnClick="NoBtn_Click" OnClientClick="return false" />
                        <input type="checkbox" id="yesBehindFile" runat="server" class="yes_clicked"/>
                        <input type="checkbox" id="noBehindFile" runat="server" class="no_clicked" />
                    </p>
                           <%-- </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div><!-- end yes/no sameplace -->
                    <!-- if student wants to write his exams at the same place-->
                <div class="collapse" id="is_in_nb">
                    <div class="twelve columns geo_choose"><!-- he chooses if he will be inside NB or outside NB-->
                      <input type="checkbox" id="renameThis" runat="server" class=" one column geo_in_check" />
                      <p role="button" class="five columns geo_in"> au nouveau brunswick</p>
                      <input type="checkbox" id="renameIt" runat="server" class="one column geo_out_check" />
                      <p role="button" class="five columns geo_out"> Hors du nouveau-brunswick</p>
                    </div><!-- choose option (in_nb or out_nb) -->
                    <div class="twelve columns geo_choose_city" style="display:none;"> <!-- below we show a list of cities according to student's previous choice-->
                    <p class="six columns"> Veuiller choisir une ville s.v.p :</p>
                    <!-- by default the list of cities are display:none, but in the JS script we display:block based on the clicked p -->
                    <asp:DropDownList runat="server" ID="lstVilleNb" CssClass="five columns singleLocation in_nb_select" style="display:none;"> </asp:DropDownList>
                   <asp:DropDownList runat="server" ID="lstVilleHnb" CssClass="five columns singleLocation out_nb_select" style="display:none;"> </asp:DropDownList>
                    <a role="button" class="flaticon-close33 one column back_to_option"></a>
                    </div><!-- make your choice of city -->
                 </div>
                 <div class="collapse" id="is_out_of_nb"><!--**** if exams are not in the same place ****-->
                    <!-- Ville de l'intra -->
                     <div class="twelve columns intra_diff_places">
                       <div class="twelve columns intra_question">
                           <span class="seven columns"> Mon examen intra s'effectuera hors du Nouveau-Brunswick:</span>
                           <input type="radio" id="radio2" runat="server" class="radio_intra_yes" />
                           <a class="two columns intra_yes" role="button"> Oui </a>
                           <input type="radio" id="radio3" runat="server" class="radio_intra_no" />
                           <a class="two columns intra_no" role="button"> Non</a>
                       </div>
                       <div class=" twelve columns differentPlace_intra_choose" style="display:none;">
                           <p class="three columns"> Choisir une ville :</p>
                           <input type="button" class="three columns intraVille" value="Details du centre" data-toggle="collapse" data-target="#suggest_intra_city" aria-expanded="false" aria-controls="suggest_intra_city"/>
                           <asp:DropDownList style="display:none;" runat="server" ID="intraLstVilleNbDifferentPlace" CssClass="offset-by-one five columns intra_in_nb"></asp:DropDownList>
                           <asp:DropDownList style="display:none;" runat="server" ID="intraLstVilleHnbDifferentPlace" CssClass="four columns intra_out_nb"></asp:DropDownList>
                           <a role="button" class="flaticon-close33 one column back_to_intra_diff"></a>
                       </div>
                     </div><!-- end intra_diff_place-->
                     <!-- suggest city intra -->
                        <div class="collapse" id="suggest_intra_city">
                           <asp:TextBox ID="adress" runat="server" CssClass="six columns building_adress" placeholder="Adresse du local" />
                           <asp:TextBox ID="cityName" runat="server" CssClass="six columns city_name" placeholder="Nom de la ville" />
                           <asp:TextBox ID="postalCode" runat="server" CssClass="six columns postal_code" placeholder="Code postal" />
                           <asp:TextBox ID="countryName" runat="server" CssClass="six columns country_name" placeholder="Pays" />
                        </div><!-- end new ville intra inputs -->    
                     <!-- end suggest city intra-->
                     <div class="twelve columns final_diff_places">
                       <div class="twelve columns final_question">
                           <span class="seven columns"> Mon examen final s'effectuera hors du Nouveau-Brunswick:</span>
                           <input type="radio" id="radioFinal" runat="server" class="radio_final_yes" />
                           <a class="two columns final_yes" role="button"> oui </a>
                           <input type="radio" id="radio1" runat="server" class="radio_final_no" />
                           <a class="two columns final_no" role="button">non </a>
                       </div>
                       <div class="twelve columns differentPlace_final_choose" style="display:none;">
                           <p class="three columns"> Choisir une ville :</p>
                           <input type="button" class="three columns finalVille" value="Details ville" data-toggle="collapse" data-target="#suggest_final_city" aria-expanded="false" aria-controls="suggest_final_city"/>
                           <asp:DropDownList style="display:none;" runat="server" ID="finalLstVilleNbDifferentPlace" CssClass="offset-by-one five columns final_in_nb"></asp:DropDownList>
                           <asp:DropDownList style="display:none;" runat="server" ID="finalLstVilleHnbDifferentPlace" CssClass="four columns final_out_nb"></asp:DropDownList>
                           <a role="button" class="flaticon-close33 one column back_to_final_diff"></a>
                     </div>
                     </div><!-- end final_diff_places -->
                     <!-- suggest city final -->
                        <div class="collapse" id="suggest_final_city">
                           <asp:TextBox ID="TextBox1" runat="server" CssClass="six columns building_adress" placeholder="Adresse du local" />
                           <asp:TextBox ID="TextBox2" runat="server" CssClass="six columns city_name" placeholder="Nom de la ville" />
                           <asp:TextBox ID="TextBox4" runat="server" CssClass="six columns postal_code" placeholder="Code postal" />
                           <asp:TextBox ID="TextBox5" runat="server" CssClass="six columns country_name" placeholder="Pays" />
                        </div><!-- end new ville intra inputs -->
                   <!--  <div class="suggest_city">
                         <input type="text" placeholder="Adresse du centre suggéré"/>
                     </div> -->
                         <div class="collapse" id="new_city" style="display:none;">
                          <asp:TextBox ID="ville_intra_new" runat="server" CssClass="twelve columns intra" placeholder="Ville intra"/>
                          <asp:TextBox ID="ville_final_new" runat="server" CssClass="twelve columns final" placeholder="Ville final"/>
                         </div><!-- end new city details -->
                  </div> <!-- **** end both.false **** -->
                 </div> <!-- end exam and city  -->
 <!-- *********************************** coordonnées du superviseur **************************************** -->
                        <div class="twelve columns chooseOption_Sup">
                            <span class="three columns checkboxTxt"> Meme superviseur aux 2 endroits</span>
                            <input runat="server" id="sameSup" type="checkbox" class="one columns checkSameSup"/>
                            <a class="getNewSup intra_new_sup_link" href="#suggestNewSup_intra"
                               role="button" data-toggle="collapse" aria-expanded="false"aria-controls="suggestNewSup_intra" >
                               <span> Superviseur ville intra </span>
                            </a>
                            <a class="columns getNewSup final_new_sup_link" href="#suggestNewSup_final"
                               role="button" data-toggle="collapse" aria-expanded="false"aria-controls="suggestNewSup_final" >
                               <span> Superviseur ville final </span>
                            </a>
                        </div> <!-- choose option sup -->
<div class="collapse" id="suggestNewSup_intra"> <!-- shows up if student wants new sup -->
             <edPerm:ISup ID="supIntra" runat="server" />   
        </div> <!-- yes new supervisor -->
        <div class="collapse" id="suggestNewSup_final"> <!-- shows up if student wants new sup -->
             <edPerm:ISup ID="supFinal" runat="server" />   
        </div> <!-- yes new supervisor -->
        <a href="#" class="twelve columns prev">
            <span class="flaticon-back28 prev-icon one column"></span>
            <!-- <img class="one column" src="./images/prev.png" alt="prochaine étape"> -->
            <span style="display:block;" class="eleven columns"> Informations personnelles </span>
        </a>
        <asp:Button ID="btn" Text="Reserver" runat="server" CssClass="eight columns bottomForm" OnClick="btn_Click"/>
        </div> <!-- end of second panel -->
    </div> <!-- global student and supervisor info(previously form tag) -->
    </div><!-- container -->
</div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
                      <footer> <!-- release the display to see the footer -->
               <div class="container">
                <div class="one column logo">
                        <a href="#">
                            <img src="images/logo_udem_foot.png" />
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="four columns copyright">
                    <span class="twelve columns"> &copy;copyright 2015 - tous droits reservés </span>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div>
    </form>
</body>
</html>
