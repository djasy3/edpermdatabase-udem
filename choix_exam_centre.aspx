<!DOCTYPE html>
<html>
    <head>
        <title> Renseignements etudiant(e)s et superviseur(e)s</title>
        <link rel="stylesheet" type="text/css" href="./App_Themes/css/form_etudiant.css">
        <link rel="stylesheet" type="text/css" href="./App_Themes/css/frame.css">
        <link rel="stylesheet" type="text/css" href="./App_Themes/css/skeleton.css">
        <script type="text/javascript" src="./App_Themes/js/jquery.js"></script>
        <meta charset="utf-8">
    </head>
    <body>
     <div id="wrapper">
          <header class="blue_bar">
            <div class="container">
                <div class="three columns logo">
                    <p class="">
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">
                <form class="eight columns list_centre">
                  <table>
                      <tr>
                          <th> Centre d'examen </th>
                          <th> Examen intra </th>
                          <th> Examen final </th>
                      </tr>
                      <tr>
                          <td> Moncton</td>
                          <td>
                              <input type="checkbox">
                          </td>
                          <td>
                              <input type="checkbox">
                          </td>
                      </tr>
                      <tr>
                          <td> Bathurst</td>
                          <td>
                              <input type="checkbox">
                          </td>
                          <td>
                              <input type="checkbox">
                          </td>
                      </tr>
                      <tr>
                          <td> Shippagan</td>
                          <td>
                              <input type="checkbox">
                          </td>
                          <td>
                              <input type="checkbox">
                          </td>
                      </tr>
                      <tr>
                          <td> Campbellton</td>
                          <td>
                              <input type="checkbox">
                          </td>
                          <td>
                              <input type="checkbox">
                          </td>
                      </tr>
                      <tr>
                          <td> Burkina-Faso</td>
                          <td>
                              <input type="checkbox">
                          </td>
                          <td>
                              <input type="checkbox">
                          </td>
                      </tr>
                      <tr>
                          <td> Quebec</td>
                          <td>
                              <input type="checkbox">
                          </td>
                          <td>
                              <input type="checkbox">
                          </td>
                      </tr>
                      <tr>
                          <td> Edmundston </td>
                          <td>
                              <input type="checkbox">
                          </td>
                          <td>
                              <input type="checkbox">
                          </td>
                      </tr>
                      <tr>
                          <td> </td>
                          <td>
                              <input type="checkbox">
                          </td>
                          <td>
                              <input type="checkbox">
                          </td>
                      </tr>
                  </table>
                    <input type="submit">
                </form> <!-- global student and supervisor info -->
              </div><!-- container -->
            </div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
            <footer>
               <div class="container">
                <div class="three columns logo">
                        <a href="#">
                            <img>
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="two columns copyright">
                    <p class="">
                        <a href="#"> copyright 2015 - tous droits reserved  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div> <!-- wrapper -->
    </body>
</html>
