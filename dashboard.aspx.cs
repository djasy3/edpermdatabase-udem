﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentSession sess = SessionAccess.getCurrentSession();

        if (!IsPostBack)
        {
            listOfSessions.DataSource = SessionAccess.getSessions();
            listOfSessions.DataTextField = "s_nom";
            listOfSessions.DataValueField = "s_id";
            listOfSessions.DataBind();
            listOfSessions.SelectedValue = sess.Id.ToString();
            //fonction pour afficher le nombre des cours présents
        }
    }

}