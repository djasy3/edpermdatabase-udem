﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default_Index.aspx.cs" Inherits="Default_Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title> welcome </title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="App_Themes/css/skeleton.css" media="all"/>
        <link rel="stylesheet" type="text/css" href="App_Themes/css/landing_page.css" media="all"/>
    </head>
<body>
    <form id="form1" runat="server">
 <div id="wrapper">
       <div class="container">
             <!-- brand slogan starts -->
            <div class="offset-by-three ten columns brand_slogan">
                <p class="twelve columns brand">
                    &Eacute;ducation permanente
                </p> <!-- brand or institution name -->
                <p class="offset-by-three ten columns slogan">
                    L'universit&eacute; en tout temps.
                </p> <!-- slogan -->
            </div> <!-- brand_slogan -->
             <!-- authentication starts -->
            <div class="offset-by-four four columns authentication">
                <div class="login_form">
                    <asp:TextBox runat="server" ID="userNameAdmin" CssClass="twelve columns username" placeholder="identifiant"/>
                    <asp:TextBox runat="server" ID="passwordAdmin" CssClass="twelve columns password" type="password" placeholder="mot de passe"/>
                    <asp:button runat="server" ID="loginButton" CssClass="four columns submit" type="submit" Text="entrer"/>
                </div><!-- end login form -->
            </div> <!-- authentication -->
             <!-- error starts -->
            <div class="four columns errors">
                <asp:label runat="server" ID="loginErrorMessage" CssClass="twelve columns loginErrorMessage"> combinaison incorrecte </asp:label>
                <a class="twelve columns" href=""> mot de passe oubli&eacute; ?</a>
            </div> <!-- errors -->
             <!-- footer starts -->
            <div class="twelve columns footer">
                <a class="one column" href="http://www.umoncton.ca"><img src="images/logo_udem.png"></a>
                <p class="five columns"> &copy; 2015, Universit&eacute; de Moncton.
                 Tous droits r&eacute;serv&eacute;s</p>
            </div> <!-- footer -->
         </div> <!-- container -->
     </div> <!-- end wrapper -->
    </form>
</body>
</html>
