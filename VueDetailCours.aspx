﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermProd.master" AutoEventWireup="true" CodeFile="VueDetailCours.aspx.cs" Inherits="VueDetailCours" %>
<%@ PreviousPageType VirtualPath="~/VueCours.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--<link href="App_Themes/css/course_view.css" rel="stylesheet"/>--%>
    <link href="App_Themes/css/detail_vue.css" rel="stylesheet"/>
    <script src="App_Themes/js/views.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ajax" runat="server"/>
     <asp:UpdatePanel ID="details" runat="server">
        <ContentTemplate>
            <div class="ten columns alerts_admin">
                <asp:Label ID="er" runat="server" ForeColor="Red" />
            </div><!-- end alerts admin -->
            <p class="twelve columns view_title">
                <span> Prière de cliquer sur un cours pour accéder aux étudiants inscrits de ce cours<br />
                    <asp:Label ID="erreur" runat="server" ForeColor="Red" />                
                </span>
            </p> <!-- end view title -->
            <div class="twelve columns course_filter">
                <span class="three columns filter_title"> Options de filtrage </span>
                <asp:DropDownList ID="lstSession" runat="server" CssClass="two columns" OnSelectedIndexChanged="lstSession_SelectedIndexChanged" AutoPostBack="true"/>
                <asp:DropDownList ID="lieuExam" runat="server" CssClass="two columns" OnSelectedIndexChanged="lieuExam_SelectedIndexChanged" AutoPostBack="true" >
                    <asp:ListItem Selected="True" Value="all">Partout</asp:ListItem>
                    <asp:ListItem Value="nb">Au N.-B.</asp:ListItem>
                    <asp:ListItem Value="hnb">Hors du N.-B.</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="typeExam" runat="server" CssClass="two columns" OnSelectedIndexChanged="typeExam_SelectedIndexChanged" AutoPostBack="true" >
                    <asp:ListItem Selected="True" Value="both">Les deux</asp:ListItem>
                    <asp:ListItem Value="intra">Intra</asp:ListItem>
                    <asp:ListItem Value="final">Final</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="students" runat="server" CssClass="two columns" OnSelectedIndexChanged="students_SelectedIndexChanged" AutoPostBack="true" >
                    <asp:ListItem Value="tconf">Tous les étudiants</asp:ListItem>
                    <asp:ListItem Selected="True" Value="conf">Ont déjà Confirmés</asp:ListItem>
                </asp:DropDownList>
            </div><!-- end filter -->
            <div class="three columns course_list">
                <asp:ListView ID="lstCours" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder" OnItemDataBound="lstCours_ItemDataBound">
                    <LayoutTemplate>
                        <ul class="twelve columns">
                            <asp:PlaceHolder ID="gpePlcHolder" runat="server" />
                        </ul>
                    </LayoutTemplate>
                    <GroupTemplate>
                        <asp:PlaceHolder ID="itmPlcHolder" runat="server" />
                    </GroupTemplate>
                    <ItemTemplate> 
                        <li id="lcours" runat="server">
                            <asp:LinkButton ID="detailsCours" runat="server" Text='<%# Eval("c_sigle") %>' CssClass="five columns" OnClick="detailsCours_Click" CommandArgument='<%# Eval("c_id") %>' />
                        </li> 
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <li> <a href="#"> Aucun cours </a></li>
                    </EmptyDataTemplate>
                </asp:ListView>
            </div><!-- end course list -->
            <div class="nine columns course_gate">
                <asp:ListView ID="lstReservation" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder">
                    <LayoutTemplate>
                        <ul class="twelve columns gate_child">
                             <asp:PlaceHolder ID="gpePlcHolder" runat="server" />
                        </ul>
                    </LayoutTemplate>
                    <GroupTemplate>
                        <asp:PlaceHolder ID="itmPlcHolder" runat="server" />
                    </GroupTemplate>
                    <ItemTemplate>
                        <li class="twelve columns">
                            <div class="twelve columns liRow">
                                <span class="four columns student_name"><%#Eval("et_prenom").ToString()+" "+Eval("et_nom").ToString() %> </span>
                                <%--<div class="four columns msgBtn">
                                  <asp:DropDownList runat="server" ID="chooseMsg" value="Message" class="nine columns select_msg" style="margin-top:4.7px;"/>
                                  <input type="button" value="Envoi" class="three columns envoi_msg" style="margin-top:4.5px;"/>
                                </div>--%><!-- end message button -->
                                <span class="four columns"><asp:Label ID="Label1" runat="server" Text="Facture:" /></span>
                                <asp:TextBox ID="facture" runat="server" placeholder="Facture" CssClass="two columns facture" style="margin-top:3.5px;" OnTextChanged="facture_TextChanged" Text='<%#Eval("res_facture") %>' AutoPostBack="true" />CAD
                                <div class="eleven columns exam">
                                    <span class="twelve columns section_title"> Autres informations concernant l'étudiant</span>
                                    <p class="twelve columns info_couple">
                                        <span class="six columns info_name">Couriel:</span>
                                        <span class="six columns info_value">
                                            <asp:Label ID="couriel" runat="server" Text='<%#Eval("et_couriel") %>' />
                                        </span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                        <span class="six columns info_name">Téléphone:</span>
                                        <span class="six columns info_value"><%#Eval("et_telJour") %></span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Lieu choisi:</span>
                                     <span class="six columns info_value"><%#Eval("le_ville") %></span>
                                    </p>
                                </div><!-- end exam intra -->
                                <div class="eleven columns exam_final">
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Date de l'examen:</span>
                                     <span class="six columns info_value"><%#Eval("ce_date") %></span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Superviseur propos&eacute;:</span>
                                     <span class="six columns info_value"><%# Eval("sup_prenom").ToString()+" "+Eval("sup_nom").ToString() %> (<%# Eval("sup_langue") %>)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                        <span class="six columns info_name">Type d'examen:</span>
                                        <span class="six columns info_value">
                                            <asp:Label ID="typeExam" runat="server" Text='<%#Eval("ce_typeExam") %>' />
                                        </span>
                                    </p>
                                </div>
                                <div class="eleven columns note_canvas">
                                    <asp:TextBox ID="cd" runat="server" Text='<%# Eval("ce_comment") %>' CssClass="twelve columns" placeholder="Ecrire une note ici" OnTextChanged="cd_TextChanged" AutoPostBack="true" />
                                </div><!-- end of note canvas -->
                            </div>
                        </li>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        Aucun étudiant présent pour ce cours
                    </EmptyDataTemplate>
                </asp:ListView>
                
                <!-- Deuxième liste vieuw -->
                <asp:ListView ID="lstEtudiantTous" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder" OnItemDataBound="lstEtudiantTous_ItemDataBound">
                    <LayoutTemplate>
                        <asp:CheckBox ID="checkDelAll" runat="server" Text="Supprimer toute la liste !" TextAlign="Right" />
                        <asp:Button ID="delAll" runat="server" Text="Effacer" OnClick="delAll_Click" />
                        <ul class="twelve columns gate_child">
                             <asp:PlaceHolder ID="gpePlcHolder" runat="server" />
                        </ul>
                    </LayoutTemplate>
                    <GroupTemplate>
                        <asp:PlaceHolder ID="itmPlcHolder" runat="server" />
                    </GroupTemplate>
                    <ItemTemplate>
                        <li class="twelve columns" id="liste" runat="server">
                            <div class="twelve columns liRow">
                                <div class="ten columns exam_intra">
                                    <p class="twelve columns info_couple">
                                        <span class="six columns info_name">Nom:</span>
                                        <span class="six columns info_value"><%#Eval("et_nom") %></span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                        <span class="six columns info_name">Prenom</span>
                                        <span class="six columns info_value"><%#Eval("et_prenom") %></span>
                                    </p>                        
                                    <p class="twelve columns info_couple">
                                        <span class="six columns info_name">Téléphone du jour</span>
                                        <span class="six columns info_value"><%#Eval("et_telJour") %></span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                        <span class="six columns info_name">Couriel</span>
                                        <span class="six columns info_value">
                                            <asp:Label ID="couriel" runat="server" Text='<%#Eval("et_couriel") %>' /></span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                        <span class="six columns info_name">As déjà confirmé</span>
                                        <span class="six columns info_value"><asp:Label ID="ack" runat="server" /></span>
                                    </p>
                                </div>
                            <div class="ten columns note_canvas">
                                <asp:TextBox ID="cd" runat="server" Text='<%#Eval("et_notes") %>' CssClass="twelve columns" placeholder="Ecrire une note ici" OnTextChanged="cd_TextChanged" AutoPostBack="true" />
                            </div><!-- end of note canvas -->
                            <%--<div class="four columns msgBtn">
                                <asp:DropDownList runat="server" ID="chooseMsg" value="Message" class="nine columns select_msg" style="margin-top:4.7px;" OnSelectedIndexChanged="chooseMsg_SelectedIndexChanged">
                                </asp:DropDownList>
                                <%--<input type="button" value="Envoi" class="three columns envoi_msg" style="margin-top:4.5px;"/>
                                <asp:Button ID="sentMsg" runat="server" Text="Envoi" CssClass="three columns envoi_msg" style="margin-top:4.5px;" OnClick="sentMsg_Click" />
                            </div><!-- end message button --> --%>
                            </div>
                            <asp:LinkButton ID="del" runat="server" Text="Supprimer l'étudiant" OnClick="del_Click" /><br />
                            <%--<asp:LinkButton ID="updt" runat="server" Text="Mettre à jour les info de l'étudiant" />--%>
                        </li>
                </ItemTemplate>
                    <EmptyDataTemplate>
                        <li> <a href="#"> Aucun Etudiant </a></li>
                    </EmptyDataTemplate>
                 </asp:ListView>
            </div><!-- end listening gate -->
        </ContentTemplate>
    </asp:UpdatePanel>  
</asp:Content>


