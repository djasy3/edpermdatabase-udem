﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class VueCours : System.Web.UI.Page
{
    CurrentSession sessionCourante = SessionAccess.getCurrentSession();
    //public static int sid;
    private int temp;//retient la valeur temporaire de la session afin d'être postbacké
    public static int Sid { get; set; }
    //private int nrc = 0;
    public int SessionId
    {
        get { return temp; }
        set { temp = value;}
    }
    public int Nrc { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Sid = sessionCourante.Id;

        if (PreviousPage != null)//on prend le nrc de la page si l'on vient de la page vuecours !
        {
            DBoard prevPage = PreviousPage as DBoard;
            if (prevPage != null)
            {
                Sid = int.Parse(prevPage.Sid);
            }
        }
        else
            Sid = sessionCourante.Id;
        //
        if (!IsPostBack)
        {
            listeCours();
            va.Visible = false;
        }

        SessionId = Sid;
    }
    //
    public void lstSess()
    {
        //lstSession.DataSource = SessionAccess.getSessions();
        //lstSession.DataTextField = "s_nom";
        //lstSession.DataValueField = "s_id";
        //lstSession.DataBind();
        //lstSession.SelectedValue = sessionCourante.Id.ToString();
    }
    //faut avoir liste des cours inscrit pour la session
    public void listeCours()
    {
        lstCours.DataSource = SessionAccess.getListeCours();
        lstCours.DataBind();
    }
    //
    protected void detailsCours_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        Nrc = int.Parse(btn.CommandArgument);
    }
}