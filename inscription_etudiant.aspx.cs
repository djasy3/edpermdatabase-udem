﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class inscription_etudiant : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    { 
        liste_cours.DataSource = SessionAccess.getListeCours();
        liste_cours.DataTextField = "c_sigle";
        liste_cours.DataValueField = "c_id";
        liste_cours.DataBind();
        liste_cours.SelectedIndex = -1;
        //intra
        //ville in NB
        ville_intra.DataSource = ExamAccess.getVillesNb();
        ville_intra.DataTextField = "le_ville";
        ville_intra.DataValueField = "le_id";
        ville_intra.DataBind();
        //ville out nb
        ville_final.DataSource = ExamAccess.getVillesCan();
        ville_final.DataTextField = "le_ville";
        ville_final.DataValueField = "le_id";
        ville_final.DataBind();

        //examen final
        //ville in NB
        ville_intra1.DataSource = ExamAccess.getVillesNb();
        ville_intra1.DataTextField = "le_ville";
        ville_intra1.DataValueField = "le_id";
        ville_intra1.DataBind();
        //ville out nb
        ville_final1.DataSource = ExamAccess.getVillesCan();
        ville_final1.DataTextField = "le_ville";
        ville_final1.DataValueField = "le_id";
        ville_final1.DataBind();

    }
}