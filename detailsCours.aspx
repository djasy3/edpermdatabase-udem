﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="vue_cours.aspx.cs" Inherits="vue_cours" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title> Vue des cours </title>
        <!-- CSS -->
    <link href="App_Themes/css/skeleton.css" rel="stylesheet" />
    <link href="App_Themes/css/flaticon.css" rel="stylesheet" />
    <link href="App_Themes/css/frame.css" rel="stylesheet" />
    <link href="App_Themes/css/detail_vue.css" rel="stylesheet"/>
        <!-- JS -->
    <script src="App_Themes/js/jquery.js" type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="App_Themes/js/views.js" type="text/javascript"></script>

        <!-- <script type="text/javascript" src="./js/html5shiv.js"></script> -->
        <!-- <script type="text/javascript" src="./js/modernizr-latest.js"></script> -->
</head>
<body>
    <form id="form1" runat="server">
<div id="wrapper">
          <header class="blue_bar">
            <div class="container">
                <div class="three columns logo">
                    <p class="">
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">
               <div class="ten columns alerts_admin">
                 <span> Creation du cours, reussie !! </span>
               </div><!-- end alerts admin -->
               <p class="twelve columns view_title">
                   <span> Prière de cliquer sur un cours pour accéder aux étudiants inscrits de ce cours </span>
               </p> <!-- end view title -->
                <div class="twelve columns course_filter">
                     <span class="three columns filter_title"> Options de filtrage </span>
                    <asp:DropDownList ID="DropDownList3" runat="server" CssClass="two columns"/>
                    <asp:DropDownList ID="DropDownList4" runat="server" CssClass="two columns"/>
                    <asp:DropDownList ID="list3" runat="server" CssClass="two columns"/>
                    <asp:DropDownList ID="DropDownList2" runat="server" CssClass="two columns"/>
                   <!--  <input type="button" value="critere_1">
                     <input type="button" value="critere_2">
                     <input type="button" value="critere_3">
                     <input type="button" value="getTypeExamasdafs"> -->
                </div><!-- end filter -->
                <div class="three columns course_list">
                    <ul class="twelve columns">
                        <li>
                            <a class="" href="#"> info 1000 </a>
                        </li>
                        <li>
                            <a class="" href="#"> math 2000 </a>
                        </li>
                        <li> <a href="#"> math 3400 </a></li>
                        <li> <a href="#"> gmec 2200 </a></li>
                        <li> <a href="#"> adrh 2100 </a></li>
                        <li> <a href="#"> piza 1700 </a></li>
                        <li> <a href="#"> piza 1700 </a></li>
                        <li> <a href="#"> piza 1700 </a></li>
                    </ul>
                </div><!-- end course list -->
                <div class="nine columns course_gate">
                    <ul class="twelve columns gate_child">
                        <li class="twelve columns">
                            <div class="twelve columns">
                                <span class="four columns student_name"> dimitri pomitrov </span>
                                <input type="button" value="Note" class="two columns noteBtn topBtn"/>
                                <div class="four columns msgBtn">
                                  <asp:DropDownList runat="server" ID="chooseMsg" value="Message" class="nine columns select_msg topBtn"/>
                                  <input type="button" value="Envoi" class="three columns envoi_msg topBtn"/>
                                </div><!-- end message button -->
                                
                                <input type="button" value="Facture" class="two columns topBtn"/>
                                <div class="twelve columns exam_intra">
                                    <span class="twelve columns section_title">Examen intra</span>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Lieu choisi:</span>
                                     <span class="six columns info_value"> Paris(Hors du N.B)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Date de l'examen:</span>
                                     <span class="six columns info_value"> 23 Novembre 2015</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Superviseur propos&eacute;:</span>
                                     <span class="six columns info_value"> Red Man(Anglais)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Assigner un centre d'examen:</span>
                                     <asp:DropDownList runat="server" ID="rename" CssClass="six columns assign_center"></asp:DropDownList>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Facture:</span>
                                     <span class="six columns info_value"> Montant etc.</span>
                                    </p>

                                </div><!-- end exam intra -->
                                <div class="twelve columns exam_final">
                                    <span class="twelve columns section_title">Examen final</span>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Lieu choisi:</span>
                                     <span class="six columns info_value"> Paris(Hors du N.B)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Date de l'examen:</span>
                                     <span class="six columns info_value"> 24 Decembre 2015</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Superviseur propos&eacute;:</span>
                                     <span class="six columns info_value"> Chill Vapor(French)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Assigner un centre d'examen:</span>
                                     <asp:DropDownList runat="server" ID="DropDownList1" CssClass="six columns assign_center"></asp:DropDownList>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Facture:</span>
                                     <span class="six columns info_value"> Montant etc.</span>
                                    </p>
                                </div><!-- end exam final -->
                                <div class="twelve columns note_canvas">
                                    <input type="text" runat="server" id="rename2" class="twelve columns" placeholder="ecrire ici" />
                                </div><!-- end of note canvas -->
                            </div>
                        </li>
                        <li class="twelve columns">
  <div class="twelve columns">
                                <span class="four columns student_name"> dimitri pomitrov </span>
                                <input type="button" value="Note" class="two columns noteBtn topBtn"/>
                                <div class="four columns msgBtn">
                                  <asp:DropDownList runat="server" ID="DropDownList5" value="Message" class="nine columns select_msg topBtn"/>
                                  <input type="button" value="Envoi" class="three columns envoi_msg topBtn"/>
                                </div><!-- end message button -->
                                
                                <input type="button" value="Facture" class="two columns topBtn"/>
                                <div class="six columns exam_intra">
                                    <span class="twelve columns section_title">Examen intra</span>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Lieu choisi:</span>
                                     <span class="six columns info_value"> Paris(Hors du N.B)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Date de l'examen:</span>
                                     <span class="six columns info_value"> 23 Novembre 2015</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Superviseur propos&eacute;:</span>
                                     <span class="six columns info_value"> Red Man(Anglais)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Assigner un centre d'examen:</span>
                                     <asp:DropDownList runat="server" ID="DropDownList6" CssClass="six columns assign_center"></asp:DropDownList>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Facture:</span>
                                     <span class="six columns info_value"> Montant etc.</span>
                                    </p>

                                </div><!-- end exam intra -->
                                <div class="six columns exam_final">
                                    <span class="twelve columns section_title">Examen final</span>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Lieu choisi:</span>
                                     <span class="six columns info_value"> Paris(Hors du N.B)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Date de l'examen:</span>
                                     <span class="six columns info_value"> 24 Decembre 2015</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Superviseur propos&eacute;:</span>
                                     <span class="six columns info_value"> Chill Vapor(French)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Assigner un centre d'examen:</span>
                                     <asp:DropDownList runat="server" ID="DropDownList7" CssClass="six columns assign_center"></asp:DropDownList>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Facture:</span>
                                     <span class="six columns info_value"> Montant etc.</span>
                                    </p>
                                </div><!-- end exam final -->
                                <div class="twelve columns note_canvas">
                                    <input type="text" runat="server" id="Text1" class="twelve columns" placeholder="ecrire ici" />
                                </div><!-- end of note canvas -->
                            </div>
                        </li>
                        <li class="twelve columns">
                           <div class="twelve columns">
                                <span class="four columns student_name"> dimitri pomitrov </span>
                                <input type="button" value="Note" class="two columns noteBtn topBtn"/>
                                <div class="four columns msgBtn">
                                  <asp:DropDownList runat="server" ID="DropDownList8" value="Message" class="nine columns select_msg topBtn"/>
                                  <input type="button" value="Envoi" class="three columns envoi_msg topBtn"/>
                                </div><!-- end message button -->
                                
                                <input type="button" value="Facture" class="two columns topBtn"/>
                                <div class="six columns exam_intra">
                                    <span class="twelve columns section_title">Examen intra</span>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Lieu choisi:</span>
                                     <span class="six columns info_value"> Paris(Hors du N.B)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Date de l'examen:</span>
                                     <span class="six columns info_value"> 23 Novembre 2015</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Superviseur propos&eacute;:</span>
                                     <span class="six columns info_value"> Red Man(Anglais)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Assigner un centre d'examen:</span>
                                     <asp:DropDownList runat="server" ID="DropDownList9" CssClass="six columns assign_center"></asp:DropDownList>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Facture:</span>
                                     <span class="six columns info_value"> Montant etc.</span>
                                    </p>

                                </div><!-- end exam intra -->
                                <div class="six columns exam_final">
                                    <span class="twelve columns section_title">Examen final</span>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Lieu choisi:</span>
                                     <span class="six columns info_value"> Paris(Hors du N.B)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Date de l'examen:</span>
                                     <span class="six columns info_value"> 24 Decembre 2015</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Superviseur propos&eacute;:</span>
                                     <span class="six columns info_value"> Chill Vapor(French)</span>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Assigner un centre d'examen:</span>
                                     <asp:DropDownList runat="server" ID="DropDownList10" CssClass="six columns assign_center"></asp:DropDownList>
                                    </p>
                                    <p class="twelve columns info_couple">
                                     <span class="six columns info_name">Facture:</span>
                                     <span class="six columns info_value"> Montant etc.</span>
                                    </p>
                                </div><!-- end exam final -->
                                <div class="twelve columns note_canvas">
                                    <input type="text" runat="server" id="Text2" class="twelve columns" placeholder="ecrire ici" />
                                </div><!-- end of note canvas -->
                            </div>
                        </li>
                        <li class="twelve columns">
                        </li>  
                        <li class="twelve columns">  
                        </li>
                    </ul> <!-- end of gatechild -->
                </div><!-- end listening gate -->
              </div><!-- container -->
            </div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
            <footer> <!-- release the display to see the footer -->
               <div class="container">
                <div class="one column logo">
                        <a href="#">
                            <img src="images/logo_udem_foot.png"/>
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="four columns copyright">
                    <span class="twelve columns"> &copy;copyright 2015 - tous droits reservés </span>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div> <!-- wrapper -->
    </form>
</body>
</html>
