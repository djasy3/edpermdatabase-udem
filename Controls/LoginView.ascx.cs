﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_LoginView : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void lienVersPages_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList lienVersPages = (DropDownList) sender;

        switch (lienVersPages.SelectedValue)
        {
            case "dboard":
                Response.Redirect("~/DBoard.aspx");
                break;

            case "cours":
                Response.Redirect("~/VueCours.aspx");
                break;

            case "etudiants":
                Response.Redirect("~/VueDetailCours.aspx");
                break;

            case "centres":
                Response.Redirect("~/VueCentresExamens.aspx");
               break;

            case "mtgsession":
                Response.Redirect("~/MontageSession.aspx");
                break;

            case "form":
                Response.Redirect("~/FormConfirmationExam.aspx");
                break;

        }
    }
}