﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SupDetail.ascx.cs" Inherits="Controls_SupDetail" %>

<div class="twelve columns inputHolder">
              <asp:TextBox runat="server" ID="sup_nom" CssClass="seven columns sup_last_n" placeholder="Nom" autofocus="autofocus"/>
              <span class="five columns tip sup_last_n"> Nom 2 characters</span>
</div> <!-- end first name -->
<div class="twelve columns inputHolder">
    <asp:TextBox runat="server" ID="sup_prenom" CssClass="seven columns sup_first_n" placeholder="Prenom"/>
    <span class="five columns tip sup_first_n"> Prenom 2 characters</span>
</div>
<div  class="twelve columns inputHolder">
    <asp:TextBox runat="server" ID="sup_ets" CssClass="seven columns" type="text" placeholder="Nom de l'&eacute;tablissement"/>
    <!--<span class="five columns tip"> Au moins 2 charactères</span>-->
</div>
<div  class="twelve columns inputHolder">
    <asp:TextBox runat="server" ID="sup_titre" CssClass="seven columns" type="text" placeholder="Titre du poste"/>
    <!--<span class="five columns tip"> Titre 2 characteres</span>-->
</div>
<div  class="twelve columns inputHolder">
    <asp:TextBox runat="server" ID="sup_adrsCiv" CssClass="seven columns" type="text" placeholder="Adresse Civique"/>
    <!--<span class="five columns tip"> Au moins 2 charactères</span>-->
</div>
<div  class="twelve columns inputHolder">
    <asp:TextBox runat="server" ID="sup_email" CssClass="seven columns sup_email" type="text" placeholder="Email de votre institution"/>
    <span class="five columns tip sup_email"> b@cde.xyz</span>
</div>
<div class="twelve columns inputHolder">
    <asp:TextBox ID="sup_tlcp" runat="server" CssClass="seven columnse" placeholder="telecopieur"/>
    <!--<span class="five columns tip"> Au moins 2 characters</span>-->
</div>
<div class="twelve columns inputHolder">
    <asp:TextBox runat="server" ID="sup_phoneJr" CssClass="seven columns sup_tel_j" type="text" placeholder="Telephone jour"/>
    <span class="five columns tip sup_tel_j"> Au moins 2 characters</span>
</div>
<p class="twelve columns langSup">
    <span class="eight columns langSupFirstSpan">
        Communiquer avec le superviseur en :
    </span>
    <asp:DropDownList ID="sup_lang" runat="server" CssClass="three columns">
        <asp:ListItem Value="fra">Français</asp:ListItem>
        <asp:ListItem Value="eng">Anglais</asp:ListItem>
    </asp:DropDownList>
</p><!-- end of language of supervisor select -->