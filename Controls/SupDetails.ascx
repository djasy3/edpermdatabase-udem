﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SupDetails.ascx.cs" Inherits="Controls_SupDetails" %>

<asp:Panel ID="detailsSup" runat="server">
    <h3>Détails du superviseur</h3>
    <asp:Label ID="error" runat="server"/>
    <hr />
    <asp:UpdatePanel ID="updtPnl" runat="server">
        <ContentTemplate>
            <asp:Table ID="infoSup" runat="server">
        <asp:TableRow>
            <asp:TableCell>ID:</asp:TableCell>
            <asp:TableCell><asp:Label ID="sup_id" runat="server" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Nom:</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_nom" runat="server" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Prénom:</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_prenom" runat="server" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Titre du poste</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_titre" runat="server" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Nom de l'établissement</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_nomEts" runat="server" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Couriel</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_couriel" runat="server" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Adresse civique</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_adrs" runat="server" TextMode="MultiLine" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Téléphone</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_tlfn" runat="server" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Télécopieur</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_tlcp" runat="server" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Langue</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_lan" runat="server" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Commentaires</asp:TableCell><asp:TableCell><asp:TextBox ID="sup_cmnt" runat="server" TextMode="MultiLine" /></asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>Bloqué</asp:TableCell><asp:TableCell><asp:CheckBox ID="sup_blck" runat="server" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>Couleur</asp:TableCell><asp:TableCell>
                <asp:TextBox ID="sup_clr" runat="server" type="color" />
                </asp:TableCell></asp:TableRow><asp:TableRow>
            <asp:TableCell>
            </asp:TableCell><asp:TableCell>
                <asp:Button ID="sup_updt" runat="server" OnClick="sup_updt_Click" Text="Mettre à jour info" />
                <asp:Button ID="sup_insrt" runat="server" OnClick="sup_insrt_Click" Text="Ajouter un supervseur"/>
            </asp:TableCell></asp:TableRow></asp:Table><hr /><!-- on affiche les cours auxquels le superviseur sont assignés --><asp:ListView ID="lstSupExamAss" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder" >
                <LayoutTemplate>
                    <h3>Examens assignés à ce superviseur</h3><table id="tab2" style="width:30%; text-align:justify">
                        <tr>
                            <td>Examen</td><td>Type d'examen</td><td>Lieu d'examen</td></tr><asp:PlaceHolder ID="gpePlcHolder" runat="server"></asp:PlaceHolder>
                    </table>
                </LayoutTemplate>
                <GroupTemplate>
                    <tr><asp:PlaceHolder ID="itmPlcHolder" runat="server"></asp:PlaceHolder></tr>
                </GroupTemplate>
                <ItemTemplate>
                    <td><asp:Label ID="sigle" runat="server" Text='<%#Eval("c_sigle") %>' /></td>
                    <td><asp:Label ID="typeExam" runat="server" Text='<%#Eval("ce_typeExam") %>' /></td>
                    <td><asp:Label ID="ville" runat="server" Text='<%#Eval("le_ville") %>' /></td>
                </ItemTemplate>
                <EmptyDataTemplate>
                    <h3>Aucune assignation d'examen est faite pour ce superviseur</h3></EmptyDataTemplate></asp:ListView></ContentTemplate><Triggers>
        <asp:PostBackTrigger ControlID="sup_updt" />
        <asp:PostBackTrigger ControlID="sup_insrt" />
    </Triggers>
    </asp:UpdatePanel>
    <asp:ListView ID="lstSup" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder">
        <LayoutTemplate>
            <h3>Liste des superviseurs</h3><table style="width:30%; text-align:justify">
                <tr>
                    <td>ID</td><td>Nom</td><td>Prenom</td><td>Langue</td><td>Appr.</td></tr><asp:PlaceHolder ID="gpePlcHolder" runat="server"></asp:PlaceHolder>
            </table>
        </LayoutTemplate>
        <GroupTemplate>
            <tr><asp:PlaceHolder ID="itmPlcHolder" runat="server"></asp:PlaceHolder></tr>
        </GroupTemplate>
        <ItemTemplate>
            <td><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# Liens.detailsSup(Eval("sup_couriel").ToString()) %>' ToolTip='Details sur le superviseur' Text='<%# Eval("sup_id") %>' /></td>
            <td><asp:HyperLink ID="sup_lien" runat="server" NavigateUrl='<%# Liens.detailsSup(Eval("sup_couriel").ToString()) %>' ToolTip='Details sur le superviseur' Text='<%# Eval("sup_nom") %>' /></td>
            <td><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Liens.detailsSup(Eval("sup_couriel").ToString()) %>' ToolTip='Details sur le superviseur' Text='<%# Eval("sup_prenom") %>' /></td>
            <td><%# Eval("sup_langue") %></td>
            <td><asp:CheckBox ID="chck" runat="server" Checked='<%# Eval("sup_blocked") %>' /></td>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>
