﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExamHorsNB.ascx.cs" Inherits="Controls_ExamHorsNB" %>
<asp:Panel ID="suiteHnb" runat="server">
    <hr />
    <span>Une entente a été établie (ou des communications sont en cours) entre l'Université de Moncton et un établissement de votre région concernant la supervision des examens</span>
    <br />
    <asp:CheckBox ID="chckNotification" runat="server" Text="J'enverrai un courriel à encadrement@umoncton.ca le plus tôt possible afin d'être informé. "/><hr />
    <asp:Label ID="Label1" runat="server">
        Avez-vous déjà fait un examen supervisé dans le cadre d'un cours en ligne à l'Université de Moncton? <asp:Label ID="Label2" runat="server" ForeColor="Red">*</asp:Label> 
    </asp:Label>
    <asp:RadioButtonList ID="yorNExamBfr" runat="server" RepeatColumns="2" AutoPostBack="true">
        <asp:ListItem Value="oui">Oui</asp:ListItem>
        <asp:ListItem Value="non">Non</asp:ListItem>
    </asp:RadioButtonList>
    <asp:Panel ID="suiteHnb2" runat="server">
        <span>
            Je désire réutiliser les services d'un superviseur auxquels j'ai eu recours dans le passé <asp:Label ID="Label3" runat="server" ForeColor="Red">*</asp:Label>
        </span>
        <asp:RadioButtonList ID="yExamBfr" runat="server" RepeatColumns="2" AutoPostBack="true">
            <asp:ListItem Value="oui">Oui</asp:ListItem>
            <asp:ListItem Value="non">Non</asp:ListItem>
        </asp:RadioButtonList>
    </asp:Panel>
    <asp:Panel ID="suiteHnb3" runat="server">
        <span>SVP utilisez les coordonnées du même superviseur</span><br />
        <span>Année:&nbsp;</span>
        <asp:DropDownList ID="annee" runat="server" OnSelectedIndexChanged="annee_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Value="2015" Selected="True">2015</asp:ListItem>
            <asp:ListItem Value="2014">2014</asp:ListItem>
        </asp:DropDownList><br /><br />
        Vous souvenez-vous encore de la session?
        <asp:DropDownList ID="session" runat="server"></asp:DropDownList><br /><br />
        <span>Sigle du cours<asp:Label runat="server" ForeColor="Red">*&nbsp;</asp:Label></span>
        <asp:DropDownList ID="lstSigleCours" runat="server"></asp:DropDownList>
    </asp:Panel>
    <asp:Panel ID="superviseurCoordonnees" runat="server">
        <fieldset>
            <legend>Coordonnées du superviseur</legend>
            <asp:TextBox ID="sup_nom" runat="server" placeholder="Nom" required="required" />
            <asp:TextBox ID="sup_prenom" runat="server" placeholder="Prénom" required="required" /><br />
            <asp:TextBox ID="sup_poste" runat="server" placeholder="Titre du Poste" required="required" />
            <asp:TextBox ID="sup_ets" runat="server" placeholder="Nom de l'établissement" required="required" />
            <asp:TextBox ID="sup_adrs" runat="server" placeholder="Adresse civique" required="required" />
            <asp:TextBox ID="sup_email" runat="server" placeholder="E-mail" required="required" type="email" />
            <asp:TextBox ID="sup_phone" runat="server"  placeholder="Téléphone jour" type="tel" /><br />
            <asp:TextBox ID="sup_tlcpy" runat="server"  placeholder="Télécopieur" /><br />
        </fieldset>
        <span>L'éducation permanente doit communiquer avec le superviseur:</span><br />
        <asp:RadioButtonList ID="sup_langue" runat="server">
            <asp:ListItem Value="fra" Selected="True">en français</asp:ListItem>
            <asp:ListItem Value="eng">en anglais</asp:ListItem>
            <asp:ListItem Value="bil">les deux</asp:ListItem>
        </asp:RadioButtonList>
    </asp:Panel>
</asp:Panel>

