﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_SupDetail : System.Web.UI.UserControl
{
    //propriété pour les infos du superviseur
    public string SupNom
    {
        get
        {
            if (!string.IsNullOrEmpty(sup_nom.Text))
            {
                return sup_nom.Text;
            }
            else throw new Exception("Veuillez entrer le nom du superviseur");
        }
    }
    //
    public string SupPrenom
    {
        get
        {
            if (!string.IsNullOrEmpty(sup_prenom.Text))
            {
                return sup_prenom.Text;
            }
            else throw new Exception("Veuillez entrer le prénom du superviseur");
        }
    }
    //
    public string SupEts
    {
        get
        {
            if (!string.IsNullOrEmpty(sup_ets.Text))
            {
                return sup_ets.Text;
            }
            else throw new Exception("Veuillez entrer l'établissement(endroit de travail) du superviseur");
        }
    }
    //
    public string SupPoste
    {
        get
        {
            if (!string.IsNullOrEmpty(sup_titre.Text))
            {
                return sup_titre.Text;
            }
            else throw new Exception("Veuillez entrer le poste du superviseur");
        }
    }
    //
    public string SupAdrsCiv
    {
        get
        {
            if (!string.IsNullOrEmpty(sup_adrsCiv.Text))
            {
                return sup_adrsCiv.Text;
            }
            else throw new Exception("Veuillez entrer l'adresse du superviseur");
        }
    }
    //
    public string SupCouriel
    {
        get
        {
            if (!string.IsNullOrEmpty(sup_email.Text))
            {
                return sup_email.Text;
            }
            else throw new Exception("Veuillez entrer le couriel du superviseur");
        }
    }
    //
    public string SupTlcp
    {
        get { return sup_tlcp.Text; }
    }
    //
    public string SupPhone
    {
        get
        {
            if (!string.IsNullOrEmpty(sup_phoneJr.Text))
            {
                return sup_phoneJr.Text;
            }
            else throw new Exception("Veuillez entrer le numéro de téléphone du superviseur");
        }
    }
    //
    public string SupLangue
    {
        get { return sup_lang.SelectedValue; }
    }
    //
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}