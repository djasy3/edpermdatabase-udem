﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LieuxExams.ascx.cs" Inherits="Controls_LieuxExams" %>

<asp:Panel ID="lieuxExam" runat="server">
    <asp:DropDownList ID="in_out" runat="server" OnSelectedIndexChanged="in_out_SelectedIndexChanged" AutoPostBack="true">
        <asp:ListItem Value="" Selected="True">&nbsp;&nbsp;---</asp:ListItem>
        <asp:ListItem Value="nb" >Au N.-B.</asp:ListItem>
        <asp:ListItem Value="hnb">Hors du N.-B.</asp:ListItem>
    </asp:DropDownList>
    <br /><br />
    <asp:Label ID="Label1" runat="server" Text="Je me présenterai à cet endroit pour faire mon examen " /><asp:Label runat="server" ForeColor="Red">*&nbsp;</asp:Label><br />
    <asp:DropDownList ID="m_villeNb" runat="server" OnSelectedIndexChanged="m_villeNb_SelectedIndexChanged"  AutoPostBack="true"></asp:DropDownList>
    <asp:DropDownList ID="m_villeHnb" runat="server" OnSelectedIndexChanged="m_villeHnb_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
    <asp:Panel ID="blocNewAdrs" runat="server">
        <asp:Label ID="mentione" runat="server" Text="Ajouter un endroit si l'endroit ou vous voulez présenter votre examen ne figure pas sur la liste" /><br />
        <fieldset>
            <legend>Nouvelle adresse</legend>
            <asp:TextBox ID="m_adres" runat="server" placeholder="Adresse"/><br />
            <asp:TextBox ID="m_ville" runat="server" placeholder="Ville" required="required" />
            <asp:TextBox ID="m_etat" runat="server" placeholder="Province/Etat" required="required" />
            <asp:TextBox ID="m_pays" runat="server" placeholder="Pays" required="required" />
        </fieldset>
        <br />
        <asp:Label ID="entente" runat="server">Une entente a été établie (ou des communications sont en cours) entre l'Université de Moncton et un établissement de votre région concernant la supervision des examens</asp:Label>
    </asp:Panel>
</asp:Panel>

