﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LoginView.ascx.cs" Inherits="Controls_LoginView" %>

<asp:LoginView ID="cnx" runat="server">
    <AnonymousTemplate>
        <div class="three columns logo">
            <p class="">
                <a href="http://www.umoncton.ca/edperm/"> &Eacute;ducation permanente </a>
            </p>
        </div> <!-- logo -->
    </AnonymousTemplate>
    <LoggedInTemplate>
        <span>Vous êtes déjà connecté</span>
    </LoggedInTemplate>
    <RoleGroups>
        <asp:RoleGroup Roles="AdminN0">
            <ContentTemplate>
                <div class="three columns logo">
                    <p class="">
                        <a href="http://www.umoncton.ca/edperm/"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <!-- liste déroulante vers les différentes pages -->
                        <asp:DropDownList ID="lienVersPages" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lienVersPages_SelectedIndexChanged">
                            <asp:ListItem >Lien vers d'autres pages</asp:ListItem>
                            <asp:ListItem Value="dboard"> Vue Générale</asp:ListItem>
                            <asp:ListItem Value="cours">Vue des Cours</asp:ListItem>
                            <asp:ListItem Value="etudiants">Vue des Etudiants</asp:ListItem>
                            <asp:ListItem Value="centres">Vue des Centres d'examens</asp:ListItem>
                            <asp:ListItem Value="mtgsession">Montage de Session</asp:ListItem>
                            <asp:ListItem Value="form">Formulaire de réservation</asp:ListItem>
                        </asp:DropDownList>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <asp:LoginName ID="LoginName" runat="server" FormatString="{0}" Font-Size="Small" />
                        (<asp:LoginStatus ID="LoginStatus2" runat="server" Font-Size="X-Small" Font-Italic="true" LogoutAction="RedirectToLoginPage" />)
                    </p>
                </div> <!-- user menu dropDown -->
            </ContentTemplate>
        </asp:RoleGroup>
    </RoleGroups>
</asp:LoginView>