﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Controls_LieuxExams : System.Web.UI.UserControl
{
    //propriétés pour avoir les valeurs des champs
    public string InOutSelectedValue
    {
        get { return in_out.SelectedValue; }
        set { in_out.SelectedValue = value; }
    }
    //
    public string VilleNb
    {
        get { return m_villeNb.SelectedValue; }
    }
    //
    public string VilleHnb
    {
        get { return m_villeHnb.SelectedValue; }
    }
    //propriété pour renvoyé les détails sur le nouvel endroit
    public string Adresse
    {
        get { return m_adres.Text; }
    }
    //
    public string Ville
    {
        get { return m_ville.Text; }
    }
    //
    public string Etat
    {
        get { return m_etat.Text; }
    }
    //
    public string Pays
    {
        get { return m_pays.Text; }
    }
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            ListItem item = new ListItem();
            //ville in NB
            m_villeNb.DataSource = ExamAccess.getVillesNb();
            m_villeNb.DataTextField = "le_ville";
            m_villeNb.DataValueField = "le_id";
            m_villeNb.DataBind();
            //
            item = new ListItem();
            item.Text = "Autre";
            item.Value = "autre";
            m_villeNb.Items.Add(item);
            //ville out nb
            m_villeHnb.DataSource = ExamAccess.getVillesCan();
            m_villeHnb.DataTextField = "le_ville";
            m_villeHnb.DataValueField = "le_id";
            m_villeHnb.DataBind();
            //
            item = new ListItem();
            item.Text = "Autre";
            item.Value = "autre";
            m_villeHnb.Items.Add(item);
            //
            nbHnbSelectedValue();
            nbSelectedValue();
            hnbSelectedValue();
        }
        //blocNewAdrs.Visible = false;
    }
    //
    protected void in_out_SelectedIndexChanged(object sender, EventArgs e)
    {
        nbHnbSelectedValue();
    }
    protected void m_villeNb_SelectedIndexChanged(object sender, EventArgs e)
    {
        nbSelectedValue();
    }
    protected void m_villeHnb_SelectedIndexChanged(object sender, EventArgs e)
    {
        hnbSelectedValue();
    }
    //
    protected void nbHnbSelectedValue()
    {
        if (in_out.SelectedValue == "nb")
        {
            m_villeNb.Visible = true;
            m_villeHnb.Visible = false;
            //blocNewAdrs.Visible = false;
        }
        else if (in_out.SelectedValue == "hnb")
        {
            m_villeHnb.Visible = true;
            m_villeNb.Visible = false;
            //blocNewAdrs.Visible = false;
        }
        else
        {
            m_villeHnb.Visible = false;
            m_villeNb.Visible = false;
            //blocNewAdrs.Visible = false;
        }
    }
    //
    protected void nbSelectedValue()
    {
        if (m_villeNb.SelectedValue == "autre")
            blocNewAdrs.Visible = true;
        else
            blocNewAdrs.Visible = false;
    }
    //
    protected void hnbSelectedValue()
    {
        if (m_villeHnb.SelectedValue == "autre")
            blocNewAdrs.Visible = true;
        else
            blocNewAdrs.Visible = false;
    }
}