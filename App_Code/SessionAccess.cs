﻿using System;
using System.Data;
using System.Data.Common;

/// <summary>
/// Description résumée de SessionAccess
/// </summary>

public struct CurrentSession
{
    public int Id;
    public string Nom;
}
//structure qui renvoi les détails d'un cours
public struct DetailsCours
{
    public int nrc;
    public string sigle;
    public string nom;
    public DateTime dateIntra;
    public DateTime dateFinal;
}
//structure pour afficher les messages
public struct MessagePreEnregistre
{
    public string objet;
    public string corps;
}
//structure qui renvoi les détails d'une ville
public struct DetailsVille
{
    public string adresse;
    public string ville;
    public string etat;
    public string pays;
}
public static class SessionAccess
{
    static SessionAccess()
    {
        //constructeur logique !
    }
    //on obtient les noms de toutes les sessions disponibles
    public static DataTable getSessions()
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getSessions";
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //
    public static DataTable getSessions(int annee)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getSession";
        //on exécute la procédure stoquée et on retourne le résultat
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@annee";
        param.Value = annee;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //
    public static CurrentSession getCurrentSession()
    {
        int temp = 0;
        if (int.Parse(DateTime.Now.ToString("MM")) >= 1 && int.Parse(DateTime.Now.ToString("MM")) <= 4)
            temp = int.Parse(DateTime.Now.ToString("yyyy") + "01");
        else if (int.Parse(DateTime.Now.ToString("MM")) >= 5 && int.Parse(DateTime.Now.ToString("MM")) <= 8)
            temp = int.Parse(DateTime.Now.ToString("yyyy") + "02");
        else if (int.Parse(DateTime.Now.ToString("MM")) >= 9 && int.Parse(DateTime.Now.ToString("MM")) <= 12)
            temp = int.Parse(DateTime.Now.ToString("yyyy") + "03");
        else
            temp = 0;//retourne zéro si échoue
        //
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //procédure stocké
        cmd.CommandText = "getCurrentSession";
        //on passe les params
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@annee";
        param.Value = temp;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        //
        CurrentSession sessionCourante = new CurrentSession();
        sessionCourante.Id = 0;
        sessionCourante.Nom = "";

        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];
            sessionCourante.Id = int.Parse(ligne["s_id"].ToString());
            sessionCourante.Nom = ligne["s_nom"].ToString();
        }
        return sessionCourante;
    }
    //méthode pour enregistrer une nouvelle session
    public static bool setSession(string nom, int id)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "setSession";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nom";
        param.Value = nom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.Value = id;
        param.ParameterName = "@id";
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour avoir la liste des cours
    public static DataTable getListeCours()
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "GetListeCours";
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //méthode pour avoir la liste des cours auxquels l'étudiant est inscrit !
    public static DataTable getListeCours(string ni, int session)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "lstCoursInscrits";//mod c_cid ajouter 
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //
    public static DetailsCours getDetailsCours(int nrc)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getDetailsCours";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        DetailsCours cours = new DetailsCours();
        //
        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];

            cours.nrc = int.Parse(ligne["c_id"].ToString());
            cours.nom = ligne["c_nom"].ToString();
            cours.sigle = ligne["c_sigle"].ToString();
            cours.dateIntra = DateTime.Parse(ligne["dateIntra"].ToString());
            cours.dateFinal = DateTime.Parse(ligne["dateFinal"].ToString());

            return cours;
        }
        else
            throw new Exception("Cours introuvable !");
    }
    //
    public static DataTable getCoursParAnnee(int annee)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getCoursParAnnee";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@annee";
        param.Value = annee;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //méthode d'insertion des cours
    public static bool setCours(string sigle, string nom, DateTime dateIntra, DateTime dateFinal)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "setCours";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sigle";
        param.Value = sigle;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nom";
        param.Value = nom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@dateIntra";
        param.Value = dateIntra;
        param.DbType = DbType.DateTime;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@dateFinal";
        param.Value = dateFinal;
        param.DbType = DbType.DateTime;
        cmd.Parameters.Add(param);
        //on retourne true si tout se passe bien
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode d'insertion des cours
    public static bool updtCours(int nrc, string sigle, string nom, DateTime dateIntra, DateTime dateFinal)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "updtCours";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sigle";
        param.Value = sigle;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nom";
        param.Value = nom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@dtIntra";
        param.Value = dateIntra;
        param.DbType = DbType.DateTime;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@dtFinal";
        param.Value = dateFinal;
        param.DbType = DbType.DateTime;
        cmd.Parameters.Add(param);
        //on retourne true si tout se passe bien
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour effacer un cours
    public static bool delCours(int nrc)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "delCours";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //on retourne true si tout se passe bien
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour vérifier si un cours est déjà inscrit !
    public static bool coursInscrit(int nrc)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "coursInscrit";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        if (table.Rows.Count > 0)
            return true;
        else
            return false;
    }
    //méthode d'enregistrement des étudiants
    public static bool SetEtudiants(Etudiant etud)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "setEtudiant";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = etud.id;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nom";
        param.Value = etud.nom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@prenom";
        param.Value = etud.prenom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@couriel";
        param.Value = etud.couriel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@telJour";
        param.Value = etud.telJour;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@telNt";
        param.Value = etud.telSoir;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@adresse";
        param.Value = etud.adresse;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@lien";
        param.Value = Guid.NewGuid().ToString();
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@notes";
        param.Value = etud.notes;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //
    public static string getEtudiantLien(string couriel)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getEtudiantLien";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@couriel";
        param.Value = couriel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];
            return ligne["et_lien"].ToString();
        }
        else
            throw new Exception("Lien étudiant inexistant !");
    }
    //
    public static bool updateEtudiant(string ni, string nom, string prenom, string email, string adresse, string phoneJour, string phoneNuit, string note)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updateEtudiant";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nom";
        param.Value = nom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@prenom";
        param.Value = prenom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@email";
        param.Value = email;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@phoneJr";
        param.Value = phoneJour;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@phoneNt";
        param.Value = phoneNuit;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@adresse";
        param.Value = adresse;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@notes";
        param.Value = note;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);

        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour obtenir le ni de l'étudiant
    public static string getEtudiantNi(string couriel)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getEtudiantNi";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@couriel";
        param.Value = couriel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];
            return ligne["et_id"].ToString();
        }
        else
            throw new Exception("Le ni de l'étudiant est introuvable !") ;
    }
    //méthode pour obtenir les cours confirmé par un étudiant
    public static DataTable getCoursConfrimE(string ni, int session)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getCoursConfirmE";
        //params
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //méthode d'inscription
    public static bool SetInscription(int session, int nrc, string ni)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "setInscription";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@cid";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sid";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //on retourne true si tout se passe bien 
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour vérifier si l'étudiant est déjà inscrit
    public static bool estEtudiant(string ni)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "EstEtudiant";
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //on charge le retour des résultat dans une datatable
        DataTable dtTable = GenericDataAccess.ExecuteSelectCommand(cmd);
        //si la table à plusqu'une ligne donc, l'étudiant existe.
        return dtTable.Rows.Count > 0;
    }
    //méthode pour vérifier si l'étudiant est inscrit !
    public static bool estInscrit(string ni, int session, int nrc)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "EstInscrit";
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@cid";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sid";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //on charge le retour des résultat dans une datatable
        DataTable dtTable = GenericDataAccess.ExecuteSelectCommand(cmd);
        //si la table à plusqu'une ligne donc, l'étudiant existe.
        return dtTable.Rows.Count > 0;
    }
    //*************************************************************méthodes pour afficher les confirmations*************************************
    public static DataTable getConfirmation(int session, string typeExam, int nrc)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getConfirmation";
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //on charge le retour des résultat dans une datatable
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //deuxième méthode S : sans cours
    public static DataTable getConfirmationS(int session, string typeExam, int ville)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //
        cmd.CommandText = "getCentreExamConfrm";//confirmation sans un cours particulier
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //
    public static DataTable getConfirmationT(int session, string typeExam, int nrc, int ville)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //
        cmd.CommandText = "getCentreExamConfrmT";//confirmation avec un cours particulier
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //obtient le nombre de ceux qui ont déjà confirmé pour l'examen par ville
    public static int nbreConf(int session, string typeExam, int nrc, string ville)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "nbreConfirme";
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return int.Parse(GenericDataAccess.ExecuteScalar(cmd));
    }
    //set ack, le superviseur confirme la réservation de l'étudiant
    public static bool setAck(int cid, bool ack)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtAck";
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@cid";
        param.Value = cid;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ack";
        param.Value = ack;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //on confirme lors de l'inscription que l'étudiant
    public static bool setAck(int session, int nrc, string ni, bool confrm)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "setack";

        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@confrm";
        param.Value = confrm;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }

    //l'admin met une note particulière concernant l'étudiant
    public static bool setNote(int cid, string note)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtnote";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@cid";
        param.Value = cid;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@note";
        param.Value = note;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour glisser une note concernant une certaine reservation
    public static bool setNote(int nrc, int session, string typeExam, string ville, string note)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtnoteRsrvt";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@note";
        param.Value = note;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //l'admin confirme le lieu de l'examen
    public static bool cnfrmLieu(int nrc, int session, string typeExam, string ville, bool lieu)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtlieu";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@lieu";
        param.Value = lieu;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //l'admin confirme l'envoi de la copie papier
    public static bool cnfrmCpscan(int nrc, int session, string typeExam, string ville, bool cpScan)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtCpScan";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@cpscan";
        param.Value = cpScan;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //
    //l'admin confirme l'envoi de la copie papier
    public static bool cnfrmCpPapier(int nrc, int session, string typeExam, string ville, bool cpPapier)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtCpPapier";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@cpPapier";
        param.Value = cpPapier;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode qui approuve ou pas le superviseur
    public static bool supApprv(int nrc, int session, string typeExam, string ville, bool apprv)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtSupApprv";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@apprv";
        param.Value = apprv;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour confirmé l'envoi de la trousse au superviseur
    public static bool trousseSent(int nrc, int session, string typeExam, string ville, bool sent)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtSentTrousse";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sent";
        param.Value = sent;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour signaler qu'une entente à été faite !
    public static bool ententeSent(int nrc, int session, string typeExam, string ville, bool sent)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtSentEntente";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sent";
        param.Value = sent;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //détails à propo de la facture !
    public static bool detailsFacture(int nrc, int session, string typeExam, string ni, string details)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtDtlsFacture";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@details";
        param.Value = details;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour montrer les cours disponibles déjà reservé
    public static DataTable lstCoursRsrve(int sess)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "lstCoursRsve";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = sess;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //méthode pour montrer les cours disponibles déjà reservé
    public static DataTable lstVilleRsrve(int sess)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "lstVilleRsve";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = sess;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //méthode pour avoir les détails d'une ville
    public static DetailsVille lstVille(int id)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "lstVille";

        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        DetailsVille dtVille = new DetailsVille();
        //
        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];
            dtVille.adresse = ligne["le_adresse"].ToString();
            dtVille.ville = ligne["le_ville"].ToString();
            dtVille.etat = ligne["le_etat"].ToString();
            dtVille.pays = ligne["le_pays"].ToString();

            return dtVille;
        }
        else
            throw new Exception("La ville est introuvable !");
    }
    //méthode pour effacer une ville de la liste
    public static bool delVille(int id)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "delVille";

        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour update une ville
    public static bool updtVille(int id, string adresse, string ville, string etat, string pays)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtVille";

        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "adresse";
        param.Value = adresse;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@etat";
        param.Value = etat;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@pays";
        param.Value = pays;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour avoir toutes les villes
    public static DataTable lstVilleRsrve()
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "lstAllCentre";
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //méthode pour compter les étudiants déjà inscrits pour une session en générale
    public static int nbreEtudiant(int sess)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "nbreEtudiantsInscritsParSession";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = sess;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return int.Parse(GenericDataAccess.ExecuteScalar(cmd));
    }
    //méthode pour compter les étudiants inscrit à un certain cours par une session et ayant déjà confirmé pour l'examen
    public static int nbreEtudiant(int sess, int nrc)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "nbreEtudiantsConf";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = sess;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        return int.Parse(GenericDataAccess.ExecuteScalar(cmd));
    }
    //méthode pour compter les étudiants inscrit n'ayant pas déjà confirmé
    public static int nbreEtudiantInscrit(int sess, int nrc)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "nbreEtudiantsInscrits";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = sess;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        return int.Parse(GenericDataAccess.ExecuteScalar(cmd));

    }
    //méthode pour compter les centres d'examens pour chaque cours
    public static int nbreCentreExams(int sess, int nrc)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "nbreCentreExam";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = sess;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        return int.Parse(GenericDataAccess.ExecuteScalar(cmd));
    }
    //centres d'examens en générale
    public static int nbreCentreExams(int sess)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "nbreCentreGen";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = sess;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return int.Parse(GenericDataAccess.ExecuteScalar(cmd));
    }
    //méthode pour compter les cours auxquels les student se sont inscrits
    public static int nbreCours(int sess)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "nbreCoursDispoParSession";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = sess;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return int.Parse(GenericDataAccess.ExecuteScalar(cmd));
    }
    //méthode pour donner la liste des étudiants inscrit par session
    public static DataTable listeEtudiantInscrit(int session, int nrc)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "lstEtudiantsInscrits";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //méthode pour enregistrer un message !
    public static bool setMsg(string obj, string corps)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "setMsg";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@obj";
        param.Value = obj;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@corps";
        param.Value = corps;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour mettre à jour les message pré-enregistrés
    public static bool updtMsg(int id, string obj, string corps)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "updtMsg";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@titre";
        param.Value = obj;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@msg";
        param.Value = corps;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@idMsg";
        param.Value = id;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour effacer les message qu'on ne veut plus 
    public static bool dltMsg(int id)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "dltMsg";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@idMsg";
        param.Value = id;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour obtenir les ids et les titres seulement
    public static DataTable getLstMsg()
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getLstMsg";
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //méthode pour obtenir un message spécifique
    public static MessagePreEnregistre getLstMsg(int id)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getMsg";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@idMsg";
        param.Value = id;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        DataTable dt = GenericDataAccess.ExecuteSelectCommand(cmd);
        MessagePreEnregistre msg = new MessagePreEnregistre();
        if (dt.Rows.Count > 0)
        {
            DataRow ligne = dt.Rows[0];
            msg.objet = ligne["obj"].ToString();
            msg.corps = ligne["corps"].ToString();

            return msg;//on retourne le message ensuite !
        }
        else
            throw new Exception("Le message que vous essayer d'afficher n'existe pas.");
    }
    //différentes méthodes de supression de toute la liste
    //on doit commencer par obtenir le id du centre, qui est le meme que celui de la reservation
    public static bool delStud(int id, int step)
    {
        //première étape, on supprime de la réservation
        //ensuite centre d'examen
        //ensuite table d'inscription
        //enfin de la table étudiant s'il l'on souhaite !
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //
        switch (step)
        {
            case 1:
                cmd.CommandText = "delRes";
                break;
            case 2:
                cmd.CommandText = "delCe";
                break;
        }
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour effacer l'inscription
    public static bool delStud(int nrc, int session, string ni)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "delIns";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);

        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour effacer l'inscription
    public static bool delAllStud(int nrc, int session)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "delAllIns";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);

        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
}