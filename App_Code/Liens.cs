﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Description résumée de Liens
/// </summary>
public static class Liens
{
    static Liens()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    //
    private static string LienAbsolu(string lienRelatif)
    {
        //lien courant
        Uri m_uri = HttpContext.Current.Request.Url;
        //lien absolue
        string app = HttpContext.Current.Request.ApplicationPath;
        if (!app.EndsWith("/"))
        {
            app += "/";
        }
        lienRelatif = lienRelatif.TrimStart('/');
        //on retourne le chemin relatif
        return HttpUtility.UrlPathEncode(String.Format("http://{0}:{1}{2}{3}", m_uri.Host, m_uri.Port, app, lienRelatif));
    }
    //
    public static string detailsSup(string couriel)
    {
        if (couriel != "")
        {
            string lien = ExamAccess.getLienSup(couriel);
            return LienAbsolu(String.Format("Sup.aspx?lq={0}", lien));
        }
        else
        {
            return LienAbsolu(String.Format("Sup.aspx?lq=noId"));
        }
    }
    //
    public static string detailsEtud(string couriel)
    {
        if (couriel != "")
        {
            string lien = SessionAccess.getEtudiantLien(couriel);
            return LienAbsolu(String.Format("Etud.aspx?lq={0}", lien));
        }
        else
        {
            return LienAbsolu(String.Format("Etud.aspx?lq=noId"));
        }
    }
    //
    public static string detailsCours(string sigle)
    {
        if (sigle != "")
        {
            int nrc = ExamAccess.getNrc(sigle);
            return LienAbsolu(String.Format("Cours.aspx?lq={0}", nrc.ToString()));
        }
        else
        {
            return LienAbsolu(String.Format("Cours.aspx?lq=noId"));
        }
    }
    //
    //méthode pour afficher les détails
    public static string vueDetailsCours(string nrc)
    {
        if (!string.IsNullOrEmpty(nrc) || !string.IsNullOrWhiteSpace(nrc))
        {
            return LienAbsolu(String.Format("VueDetailCours.aspx?lq={0}", nrc));
        }
        else
        {
            return LienAbsolu(String.Format("VueDetailCours.aspx"));
        }
    }
    //méthodes pour envoyer vers d'autres pages
    public static string vueCours()
    {
        return LienAbsolu(String.Format("VueCours.aspx"));
    }
    //montage cours !
    public static string montageCours(string position)
    {
        return LienAbsolu(String.Format("MontageSession.aspx#{0}", position));
    }
}