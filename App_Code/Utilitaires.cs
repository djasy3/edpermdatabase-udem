﻿using System;
using System.Net;
using System.Net.Mail;
/// <summary>
/// Description résumée de Utilitaires
/// </summary>
public static class Utilitaires
{
    static Utilitaires()
    {
        //constructeur
    }
    //méthode générique pour envoyer un e-mail en cas d'erreur quelconques
    public static void sendMail(string from, string to, string subject, string body)
    {
        //configuration de mail du client
        SmtpClient mailClient = new SmtpClient(EdpermConfig.MailServer);
        //configuration du serveur smtp qui requiert une authentification
        mailClient.Credentials = new NetworkCredential(EdpermConfig.MailUserName, EdpermConfig.MailPassword);
        //création le message du mail
        MailMessage mailMessage = new MailMessage(from, to, subject, body);
        //envoi du message
        mailClient.Send(mailMessage);
    }
    //méthode pour envoyer une description d'erreur
    public static void LogError(Exception ex)
    {
        //obtention de la date actuelle
        string dateTime = DateTime.Now.ToLongDateString() + ", à " + DateTime.Now.ToShortTimeString();
        //sauvegarde du message d'erreur !
        string errorMessage = "L'exception générée en date du " + dateTime;
        //obtention de la page qui a générée l'erreur
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        errorMessage += "\n\n Page localisée à : " + context.Request.RawUrl;
        //construction du message d'erreur !
        errorMessage += "\n\n Message: " + ex.Message;
        errorMessage += "\n\n Source:  " + ex.Source;
        errorMessage += "\n\n Methode: " + ex.TargetSite;
        errorMessage += "\n\n Stack Trace: " + ex.StackTrace;
        //on envoie le mail si l'option est activé dans la web config
        if (EdpermConfig.EnableErrorLogEmail)
        {
            string from = EdpermConfig.MailFrom;
            string to = EdpermConfig.ErrorLogEmail;
            string subject = "Rapport d'erreur - Reservation d'examen";
            string body = errorMessage;
            sendMail(from, to, subject, body);
        }
    }
    //méthode pour envoyer l'e-mail de confirmation du lieux d'examen!
    public static void confirmationMail(string ni, string email, string body)
    {

        //on commence par récupérer le lien unique pour générer l'url de modification
        string lien = SessionAccess.getEtudiantLien(ni);
        string from = EdpermConfig.MailFrom;
        string to = email;
        string subject = "Education Permanente - confirmation lieux examen";
        sendMail(from, to, subject, body);
    }
}