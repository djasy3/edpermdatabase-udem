﻿using System;
using System.Data;
using System.Data.Common;

/// <summary>
/// Description résumée de GenericDataAccess
/// </summary>
public static class GenericDataAccess
{
    //constructeur static
    static GenericDataAccess()
    {
        //TODO: constructeur
    }
    //execute la commande and retourne le resultat sous forme de l'objet DataTable
    public static DataTable ExecuteSelectCommand(DbCommand cmd)
    {
        //l'objet DataTable à être retournée
        DataTable table;
        //Execution de la commande en étant sur par après que celle-ci soit terminée
        try
        {
            //ouverture de la connection des données
            cmd.Connection.Open();
            //Execution de la commande et sauvegarde du résultat dans le DataTable
            DbDataReader reader = cmd.ExecuteReader();
            table = new DataTable();
            table.Load(reader);
            //fermeture du reader
            reader.Close();
        }
        catch (Exception ex)
        {
            Utilitaires.LogError(ex);
            throw;
        }
        finally
        {
            //fermeture de la conexion
            cmd.Connection.Close();
        }
        return table;
    }
    //on crée et on prépare une nouvelle instance de DbCommand à utiliser sur la nouvelle connexion
    public static DbCommand CreateCommand()
    {
        //on obtient le nom du fournisseur du pilote!
        string dataProviderName = EdpermConfig.DbProviderName;
        //on obtient la chaine de connexion
        string connectionString = EdpermConfig.DbConnectionString;
        //on crée un data factory provider
        DbProviderFactory factory = DbProviderFactories.GetFactory(dataProviderName);
        //on obtient la chaine de connexion spécifique au pilote
        DbConnection cnx = factory.CreateConnection();
        //on définit la connexion string
        cnx.ConnectionString = connectionString;
        //on crée l'objet command spécifique à la bd(mysql);
        DbCommand cmd = cnx.CreateCommand();
        //on définit le type de commande pour la manipulation des données à utiliser(procédures)
        cmd.CommandType = CommandType.StoredProcedure;
        //on retourne l'objet commande initialisé
        return cmd;
    }
    //
    public static int ExecuteNonQuery(DbCommand cmd)
    {
        //le nombre de lignes affectées dans la base de données
        int ligneAffectees = -1;
        //exécution de la commande et s'assurer la connexion à la base est close
        try
        {
            //ouverture de la connexion
            cmd.Connection.Open();
            //on execute la commande et on obtient le nombre de lignes affectées
            ligneAffectees = cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            //on génère l'erreur, si rencontréé
            Utilitaires.LogError(ex);
            throw;// new Exception(ex.Message);
        }
        finally
        {
            //on ferme la connexion
            cmd.Connection.Close();
        }
        //on retourne le nombre de lignes affectées
        return ligneAffectees;
    }
    //on execute une commande de selection et on renvoi le résultat sous forme de chaine de charactère
    public static string ExecuteScalar(DbCommand cmd)
    {
        //la valeur à être retournée
        string valeur = "";
        //execution de la commande et s'assurer la connexion à la base est close
        try
        {
            //ouverture de la connexion
            cmd.Connection.Open();
            //on execute la commande et on obtient la première ligne affectée
            valeur = cmd.ExecuteScalar().ToString();
        }
        catch (Exception ex)
        {
            //on génère l'erreur, si rencontréé
            Utilitaires.LogError(ex);
            throw;// new Exception(ex.Message);
        }
        finally
        {
            //on ferme la connexion
            cmd.Connection.Close();
        }
        //on retourne la première ligne affectée
        return valeur;
    }
}