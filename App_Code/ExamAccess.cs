﻿using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Description résumée de ExamAccess
/// contiendra toutes les méthodes relatives aux superviseurs, aux examens et aux réservations...
/// </summary>

public struct Superviseur
{
    public string id;
    public string nom;
    public string prenom;
    public string titrePoste;
    public string nomEts;
    public string couriel;
    public string tel;
    public string adrsCiv;
    public string tlcpy;
    public string langue;
    public string comment;
    public bool blocked;
    public string color;
    public string lien;
}
//
public struct Etudiant
{
    public string id;
    public string nom;
    public string prenom;
    public string adresse;
    public string couriel;
    public string telJour;
    public string telSoir;
    public string notes;
}

public static class ExamAccess
{
	static ExamAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    //on génère l'id pour la réservation
    private static string id = UserId;     //on récupère l'id
    //génération d'un user Id
    //à travers la propriéte
    private static string UserId
    {
        get { return Guid.NewGuid().ToString(); }
    }
    //méthode pour avoir l'id du superviseur qu'on venait juste d'enregistrer
    public static string getSupId(string couriel)
    {
        string idSup = "";
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getSup_id";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@couriel";
        param.Value = couriel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        DataTable dtTable = GenericDataAccess.ExecuteSelectCommand(cmd);
        //
        if (dtTable.Rows.Count > 0)
        {
            DataRow ligne = dtTable.Rows[0];
            idSup = ligne["sup_id"].ToString();
        }
        return idSup;
    }
    //méthode pour obtenir l'id d'un superviseur auxquel on a eu recour dans le passé
    public static string getSupId(int session, int nrc, string ni)
    {
        string idSup = "";
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getSupId";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        DataTable dtTable = GenericDataAccess.ExecuteSelectCommand(cmd);
        //
        if(dtTable.Rows.Count > 0)
        {
            DataRow ligne = dtTable.Rows[0];
            idSup = ligne["ce_sup"].ToString();
        }
        return idSup;
    }
    //méthode pour obtenir l'id du dernier superviseur
    public static string getSupId()
    {
        string prefix = "S";
        string id = "";
        int i = 1;
        //00000001
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getLastSupId";
        //on charge le retour des résultat dans une datatable
        DataTable dtTable = GenericDataAccess.ExecuteSelectCommand(cmd);
        //si la table à plusqu'une ligne donc il y'a déjà un superviseur dans la table.
        if (dtTable.Rows.Count > 0)
        {
            //on génère pour la toute première fois
            string temp = "";
            DataRow ligne = dtTable.Rows[0];//on récupère l'id déjà présente
            temp = ligne["sup_id"].ToString();
            //vérifions l'année dans laquelle nous somme
            if (DateTime.Now.ToString("yy") != temp.Substring(3, 2))
            {
                //si l'année est différente, l'index représentant l'année dans la chaine prend l'année actuelle
                //si l'année passé on avait S00158681, la nouvelle année on aura S00160001
                id = DateTime.Now.ToString("yy")+ i.ToString("D4");
                id = prefix + (int.Parse(id)).ToString("D8");
            }
            else
            {
                i = int.Parse(temp.Substring(3));
                i++;//on incrémente sur base de l'ancien id
                id = prefix + i.ToString("D8");
            }
        }
        else
        {
            //première partie  : 15 0001
            id = DateTime.Now.ToString("yy")+ i.ToString("D4");
            //on aura une chaine de format S 00150001
            id = prefix + (int.Parse(id)).ToString("D8");
        }
        //on retourne l'id au format S00XX000I
        return id;
    }
    //méthode pour obtenir le lien spécifique d'un superviseur
    public static string getLienSup(string couriel)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getSupLien";

        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@couriel";
        param.Value = couriel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        //
        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];
            return ligne["sup_lien"].ToString();
        }
        else
            throw new Exception("Impossible de trouvé le lien demandé");//le lien spécifié avec le sigle d'entrée n'existe pas
    }
    //méthode pour afficher les examens assignés à un superviseur donné
    public static DataTable getSupExamAss(string id, int sess)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //
        cmd.CommandText = "supExamAss";

        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = sess;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //méthode pour obtenir la liste des superviseurs
    public static DataTable getSupList()
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //
        cmd.CommandText = "supList";
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }

    //méthode pour obtenir les détails d'un superviseur
    public static Superviseur getSupDetails(string lien)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //
        cmd.CommandText = "getSupDetails";

        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@lien";
        param.Value = lien;
        param.DbType = DbType.String;
        param.Size = 36;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        Superviseur sup = new Superviseur();
        //
        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];

            sup.id = ligne["sup_id"].ToString();
            sup.nom = ligne["sup_nom"].ToString();
            sup.prenom = ligne["sup_prenom"].ToString();
            sup.titrePoste = ligne["sup_titrePoste"].ToString();
            sup.nomEts = ligne["sup_nomEts"].ToString();
            sup.couriel = ligne["sup_couriel"].ToString();
            sup.tel = ligne["sup_tel"].ToString();
            sup.adrsCiv = ligne["sup_adrsCiv"].ToString();
            sup.tlcpy = ligne["sup_telecop"].ToString();
            sup.langue = ligne["sup_langue"].ToString();
            sup.comment = ligne["sup_comment"].ToString();
            sup.blocked = bool.Parse(ligne["sup_blocked"].ToString());
            sup.color = ligne["sup_color"].ToString();
            //on retourne la structure contenant les informations du superviseur
            return sup;
        }
        else
            throw new Exception("L'id que vous avez spécifié ne correspond à aucun superviseur");
    }
    //méthode pour obtenir les détails sur l'étudiant
    public static Etudiant getEtudiantDetails(string lien)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //
        cmd.CommandText = "getEtudDetails";

        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@lien";
        param.Value = lien;
        param.DbType = DbType.String;
        param.Size = 36;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        //
        Etudiant etud = new Etudiant();

        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];

            etud.id = ligne["et_id"].ToString();
            etud.nom = ligne["et_nom"].ToString();
            etud.prenom = ligne["et_prenom"].ToString();
            etud.adresse = ligne["et_adresse"].ToString();
            etud.couriel = ligne["et_couriel"].ToString();
            etud.telJour = ligne["et_telJour"].ToString();
            etud.telJour = ligne["et_telSoir"].ToString();
            etud.notes = ligne["et_notes"].ToString();
            //on retourne la structure contenant les informations de l'étudiant
            return etud;
        }
        else
            throw new Exception("L'id que vous avez spécifié ne correspond à aucun étudiant");
    }
    //méthode pour avoir un nrc à partir d'un sigle de cours
    public static int getNrc(string sigle)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getNrc";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sigle";
        param.Value = sigle;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        //
        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];
            return int.Parse(ligne["c_id"].ToString());
        }
        else
            throw new Exception("le nrc associé à ce sigle n'existe pas");

    }
    //méthode pour retourner le nrc ou en créer un autre
    public static int getNewNrc()
    {
        int newNrc = 0;
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getLastNrc";
         //on charge le retour des résultat dans une datatable
        DataTable dtTable = GenericDataAccess.ExecuteSelectCommand(cmd);
        //si la table à plusqu'une ligne donc il y'a déjà un cours dans la table.
        if (dtTable.Rows.Count > 0)
        {
            DataRow ligne = dtTable.Rows[0];//on récupère l'id déjà présente
            newNrc = int.Parse(ligne["c_id"].ToString());
            newNrc++;//on incrémente le dernier nrc
        }
        else
        {//on génère pour la première fois un new nrc;
            newNrc += 1;
        }
        //on retourne le nrc du cours
        return newNrc;
    }
    //méthode pour générer un nouveau id du centre d'examen
    private static int getIdCentre()
    {
        int newIdCentre = 0;
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getLastIdCentre";
        //on charge le retour des résultat dans une datatable
        DataTable dtTable = GenericDataAccess.ExecuteSelectCommand(cmd);
        //si la table à plusqu'une ligne donc il y'a déjà un cours dans la table.
        if (dtTable.Rows.Count > 0)
        {
            DataRow ligne = dtTable.Rows[0];//on récupère l'id déjà présente
            newIdCentre = int.Parse(ligne["ce_id"].ToString());
            newIdCentre++;//on incrémente le dernier nrc
        }
        else
        {//on génère pour la première fois un new nrc;
            newIdCentre += 1;
        }
        //on retourne le nrc du cours
        return newIdCentre;
    }
    //méthode pour obtenir un Id du centre d'exam en fonction du cours et du type d'exam
    public static int getIdCentre(int nrc, string ni, string typeExam, int idSession)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "getIdCentre";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@idSession";
        param.Value = idSession;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];
            return int.Parse(ligne["ce_id"].ToString());
        }
        else
            return 0;
    }
    //
    public static bool setSuperviseur(string nom, string prenom, string poste, string Ets, string couriel, string tel, string adrs, string langue, string coment, bool app)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "setSup";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = getSupId();
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nom";
        param.Value = nom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@prenom";
        param.Value = prenom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@poste";
        param.Value = poste;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ets";
        param.Value = Ets;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@couriel";
        param.Value = couriel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@tel";
        param.Value = tel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@adrs";
        param.Value = adrs;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@langue";
        param.Value = langue;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@cmt";
        param.Value = coment;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@app";
        param.Value = app;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //ajout du lien guid !
        param = cmd.CreateParameter();
        param.ParameterName = "@lien";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 36;
        cmd.Parameters.Add(param);
        //on retourne true si tout se passe bien 
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    public static bool setSuperviseur(Superviseur sup)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "setSup2";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = getSupId();
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nom";
        param.Value = sup.nom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@prenom";
        param.Value = sup.prenom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@poste";
        param.Value = sup.titrePoste;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ets";
        param.Value = sup.nomEts;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@couriel";
        param.Value = sup.couriel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@tel";
        param.Value = sup.tel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@tlcpy";
        param.Value = sup.tlcpy;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@adrs";
        param.Value = sup.adrsCiv;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@langue";
        param.Value = sup.langue;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@cmt";
        param.Value = sup.comment;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@blck";
        param.Value = sup.blocked;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //ajout du lien guid !
        param = cmd.CreateParameter();
        param.ParameterName = "@color";
        param.Value = sup.color;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@lien";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 36;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //
    public static bool updateSuperviseur(Superviseur sup)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "updtSup";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = sup.id;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nom";
        param.Value = sup.nom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@prenom";
        param.Value = sup.prenom;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@poste";
        param.Value = sup.titrePoste;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ets";
        param.Value = sup.nomEts;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@couriel";
        param.Value = sup.couriel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@tel";
        param.Value = sup.tel;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@tlcpy";
        param.Value = sup.tlcpy;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@adrs";
        param.Value = sup.adrsCiv;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@langue";
        param.Value = sup.langue;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@cmt";
        param.Value = sup.comment ;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@blck";
        param.Value = sup.blocked;
        param.DbType = DbType.Boolean;
        cmd.Parameters.Add(param);
        //ajout du lien guid !
        param = cmd.CreateParameter();
        param.ParameterName = "@color";
        param.Value = sup.color;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthode pour enregistre le centre d'examen sans superviseur
    public static bool setExam(int exam, string typeExam, string ni, int lieu, int session)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "SetExam";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = getIdCentre();
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@exam";
        param.Value = exam;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@lieu";
        param.Value = lieu;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    ////méthode pour enregistre le centre d'examen avec superviseur
    public static bool setExam(int exam, string typeExam, string ni, string sup, int lieu, int session)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "setExamWtSup";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@id";
        param.Value = getIdCentre();
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@exam";
        param.Value = exam;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@lieu";
        param.Value = lieu;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sup";
        param.Value = sup;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //
    public static bool aDejaConfirmeExamen(string ni, int nrc, int session)
    {
        DbCommand cmd = GenericDataAccess.CreateCommand();
        cmd.CommandText = "aDejaConfirmer";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ni";
        param.Value = ni;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //si le résultat renvoi 2 lignes dans la table donc, une confirmation à déjà été faite par cet étudiant
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);

        return table.Rows.Count == 2;
    }
    //méthode pour set al reservation
    public static bool setReservation(int centreId)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "SetReservation";
        //on définit les paramètres
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@centreId";
        param.Value = centreId;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    //méthodes pour définir le lieux d'examen
    public static bool setLieuxExamens(string adresse, string ville, string province, string pays)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "setLieuxExamen";

        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@adrs";
        param.Value = adresse;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@province";
        param.Value = province;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@pays";
        param.Value = pays;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteNonQuery(cmd) != -1;
    }
    public static DataTable getVillesNb()//lieux au nouveau brunswick
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getVilleNb";
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //lieux au canada mais non au nb
    public static DataTable getVillesCan()
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getVilleHorsNb";
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //lieux hors du canada
    public static DataTable getVillesHorsCan()
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getVilleHorsCan";
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    public static bool lieuExiste(string ville, string province, string pays)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getIdLieuExamen";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@province";
        param.Value = province;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@pays";
        param.Value = pays;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        DataTable dtTable = GenericDataAccess.ExecuteSelectCommand(cmd);
        return dtTable.Rows.Count > 0;
    }
    //méthode pour avoir l'id du lieu de l'examen
    public static int idLieuxExam(string ville, string province, string pays)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getIdLieuExamen";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@ville";
        param.Value = ville;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@province";
        param.Value = province;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@pays";
        param.Value = pays;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        DataTable table = GenericDataAccess.ExecuteSelectCommand(cmd);
        //
        if (table.Rows.Count > 0)
        {
            DataRow ligne = table.Rows[0];
            return int.Parse(ligne["le_id"].ToString());
        }
        else
        {
            return 0;
        }

    }
    //****méthode pour avoir les nombres de réservation déjà faites
    public static int nbreRes()
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getNbreRes";
        return int.Parse(GenericDataAccess.ExecuteScalar(cmd));
    }
    //méthode pour différentes reservations
    public static DataTable getReservations(int session)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getAllReservations";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //reservation par cours
    public static DataTable getReservations(int nrc, int session)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getResCours";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //reservation par cours par lieu
    public static DataTable getReservations(int nrc, int session, string lieu)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        if (lieu == "hnb")
        {
            cmd.CommandText = "getResCoursLieuHnb";
            lieu = "nb";
        }
        else
            cmd.CommandText = "getResCoursLieuNb";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@lieu";
        param.Value = lieu;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //reservation cours par type d'examen
    public static DataTable getReservationsType(int nrc, int session, string typeExam)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getResCoursExam";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //
    public static DataTable getReservationsType(int session, string typeExam)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        cmd.CommandText = "getResSessExam";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //
    public static DataTable getReservationsTypeLieu(int session,string typeExam, string lieu)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        if (lieu == "hnb")
        {
            cmd.CommandText = "getResTypeLieuExamHnb";
            lieu = "nb";
        }
        else
            cmd.CommandText = "getResTypeLieuExamNb";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@lieu";
        param.Value = lieu;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //
    public static DataTable getReservationsLieu(int session, string lieu)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        if (lieu == "hnb")
        {
            cmd.CommandText = "getResLieuExamHnb";
            lieu = "nb";
        }
        else
            cmd.CommandText = "getResLieuExamNb";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@lieu";
        param.Value = lieu;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
    //reservation cours, lieu et type
    public static DataTable getReservations(int nrc, int session, string lieu, string typeExam)
    {
        //on obtient la configuration de l'objet DbCommand
        DbCommand cmd = GenericDataAccess.CreateCommand();
        //on définit la procédure stockée
        if (lieu == "hnb")
        {
            cmd.CommandText = "getResCoursExamLieuHnb";
            lieu = "nb";
        }
        else
            cmd.CommandText = "getResCoursExamLieuNb";
        //
        DbParameter param = cmd.CreateParameter();
        param.ParameterName = "@nrc";
        param.Value = nrc;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@sess";
        param.Value = session;
        param.DbType = DbType.Int32;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@typeExam";
        param.Value = typeExam;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        param = cmd.CreateParameter();
        param.ParameterName = "@lieu";
        param.Value = lieu;
        param.DbType = DbType.String;
        cmd.Parameters.Add(param);
        //
        return GenericDataAccess.ExecuteSelectCommand(cmd);
    }
}