﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
/// <summary>
/// Description résumée de ExcelAccess
/// </summary>
public static class ExcelAccess
{
	static ExcelAccess()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    //
    public static DataTable ImportToDataTable(string filePath, string ext, string isHdr)
    {
        string cnxString = "";
        bool hdr;

        if (isHdr == "Oui")
            hdr = true;
        else 
            hdr = false;
        //
        switch (ext)
        {
            case ".xls": //Excel 97 - 03
                cnxString = ConfigurationManager.ConnectionStrings["Excel03CnxString"].ConnectionString;
                break;
            case ".xlsx"://Excel 2007 et plus 
                cnxString = ConfigurationManager.ConnectionStrings["Excel07CnxString"].ConnectionString;
                break;
        }
        //on consolide la connection string
        cnxString = String.Format(cnxString, filePath, hdr);
        //on établie les connexions afin d'extraire les données
        OleDbConnection cnx = new OleDbConnection(cnxString);
        DataTable table = new DataTable();
        OleDbCommand cmd = new OleDbCommand();
        OleDbDataAdapter adapter = new OleDbDataAdapter();
        cmd.Connection = cnx;
        try
        {
            //on obtient le nom de la première feuille de données
            cnx.Open();
            DataTable tableExcelSchema = cnx.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string sheetName = tableExcelSchema.Rows[0]["TABLE_NAME"].ToString();

            //on lit les données de la première feuille
            cmd.CommandText = "SELECT * From [" + sheetName + "]";
            adapter.SelectCommand = cmd;
            adapter.Fill(table);
        }
        catch (Exception ex)
        {
            //on génère l'erreur, si rencontréé
            Utilitaires.LogError(ex);
            throw;// new Exception(ex.Message);
        }
        finally
        {
            cnx.Close();
        }
        //on retourne la datatable
        return table;
    }
    //méthode pour enregistrer les données dans différentes tables
    public static bool ImportCours(DataTable dt)
    {
        int count = 0;
        for (int i = 0; i < dt.Rows.Count; ++i)
        {
            //ici nous allons aligner les colonnes de la feuilles excelle en fonction des celles qui se trouvent dans notre base de données :sigle(1), nom(2)
            DetailsCours cours = new DetailsCours();
            //cours.nrc = ExamAccess.getNewNrc();//si on ne nous fournis pas le nrc !
            cours.nom = dt.Rows[i][1].ToString();
            cours.sigle = dt.Rows[i][0].ToString();
            cours.dateIntra = DateTime.Now;
            cours.dateFinal = DateTime.Now;
            try
            {//nrc, sigle, nom
                //si on n'a pas de nrc, on génère un automatiquement !
                SessionAccess.setCours(cours.sigle, cours.nom, cours.dateIntra, cours.dateFinal);
                count++;
            }
            catch (Exception ex)
            {
                //on génère l'erreur, si rencontréé
                Utilitaires.LogError(ex);
                throw;// new Exception(ex.Message);
            }
        }
        return count == dt.Rows.Count;
    }
    //méthode pour importer les étudiants et les inscrire directement
    public static bool ImportEtudiant(DataTable dt, int cours, int session)
    {
        int count = 0;
        Etudiant etud = new Etudiant();
        for (int i = 0; i < dt.Rows.Count; ++i)
        {
            //on remplit les champs de la structure selon les rangées dans la feuille excel
            etud.id = dt.Rows[i][0].ToString();
            etud.nom = dt.Rows[i][1].ToString();
            etud.prenom = dt.Rows[i][2].ToString();
            etud.couriel = dt.Rows[i][3].ToString();
            try
            {//ni, nom, prenom, e-mail. ici on commence par enregistrer l'étudiant ensuite on l'inscrit
                SessionAccess.SetEtudiants(etud);
                SessionAccess.SetInscription(session, cours, etud.id);
                count++;
            }
            catch (Exception ex)
            {
                //on génère l'erreur, si rencontréé
                Utilitaires.LogError(ex);
                throw;// new Exception(ex.Message);
            }
        }
        return count == dt.Rows.Count;
    }
}