﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermTestingMaster.master" AutoEventWireup="true" CodeFile="Etud.aspx.cs" Inherits="Etud2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="erreur" runat="server"/>
    <br />
    <asp:Panel ID="ajoutEtudiant" runat="server">
        <h3>Importer une liste des étudiants dans la base de données</h3>
        <table style="width:70%; text-align:left; height:50%">
            <tr>
                <td>Session</td>
                <td><asp:DropDownList ID="lstSession" runat="server" /></td>
                <td>Importer à partir d'un classeur</td>
                <td><asp:FileUpload ID="importFichier" runat="server" /></td>
            </tr>
            <tr>
                <td>Cours</td>
                <td><asp:DropDownList ID="lstCours" runat="server" /></td>
                <td></td>
                <td><asp:CheckBox ID="header" runat="server" Text="As une En-tête" /></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><asp:Button ID="importer" runat="server" Text="Importer le classeur" OnClick="importer_Click"  /></td>
            </tr>
        </table>
        <hr />
    </asp:Panel>
    <asp:Panel ID="formEtudiant" runat="server">
        <h3>Détails sur l'étudiant</h3>
        <hr />
        <table style="width:50%; text-align:left;">
            <tr>
                <td>NI</td>
                <td><asp:TextBox ID="et_ni" runat="server" /></td>
            </tr>
            <tr>
                <td>Nom</td>
                <td><asp:TextBox ID="et_nom" runat="server" /></td>
            </tr>
            <tr>
                <td>Prénom</td>
                <td><asp:TextBox ID="et_prenom" runat="server" /></td>
            </tr>
            <tr>
                <td>Adresse</td>
                <td><asp:TextBox ID="et_adrs" runat="server" TextMode="MultiLine" /></td>
            </tr>
            <tr>
                <td>Couriel</td>
                <td><asp:TextBox ID="et_couriel" runat="server" type="email" /></td>
            </tr>
            <tr>
                <td>Téléphone du jour</td>
                <td><asp:TextBox ID="et_phoneJr" runat="server" /></td>
            </tr>
            <tr>
                <td>Téléphone du soir</td>
                <td><asp:TextBox ID="et_phoneNt" runat="server" /></td>
            </tr>
            <tr>
                <td>Note</td>
                <td><asp:TextBox ID="et_note" runat="server" TextMode="MultiLine" /></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:Button ID="updtEt" runat="server" OnClick="updtEt_Click" Text="Mettre à jour ses infos" />
                    <asp:Button ID="setEt" runat="server" OnClick="setEt_Click" Text="Ajouter un étudiant" />
                </td>
                <td><asp:Label ID="notice" runat="server" ForeColor="Red" /></td>
            </tr>
        </table>
        <hr />
    </asp:Panel>
    <asp:Panel ID="détailsEtudiant" runat="server">
        <h3>Détails académiques</h3>
        <br />
        <span>*&nbsp;Liste des cours auxquels il est inscrit</span>
        <asp:BulletedList ID="lstCoursInscrits" runat="server">
            
        </asp:BulletedList>
        <br />
        <span>*&nbsp;Cours confirmés pour les examens</span>
        <!-- liste view -->
        <asp:ListView ID="lstCoursConfirmE" runat="server" GroupPlaceholderID="gpePlcHolder2" ItemPlaceholderID="itmPlcHolder2" OnItemDataBound="lstCoursConfirmE_ItemDataBound">
            <LayoutTemplate>
                <table style="width:50%; text-align:left;">
                    <tr>
                        <th>Examen</th>
                        <th>Type d'examen</th>
                        <th>Ville</th>
                        <th>Superviseur</th>
                    </tr>
                    <asp:PlaceHolder ID="gpePlcHolder2" runat="server"></asp:PlaceHolder>
                </table>
            </LayoutTemplate>
            <GroupTemplate>
                <asp:PlaceHolder ID="itmPlcHolder2" runat="server"></asp:PlaceHolder>
            </GroupTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:HyperLink ID="sigle_lien" runat="server" NavigateUrl='<%# Liens.detailsCours(Eval("c_sigle").ToString()) %>' ToolTip='Details du cours' Text='<%# Eval("c_sigle") %>' /></td>
                    <td><asp:Label ID="typeExam" runat="server" Text='<%# Eval("ce_typeExam") %>' /></td>
                    <td><asp:Label ID="ville" runat="server" Text='<%# Eval("le_ville") %>' /></td>
                    <td>
                        <asp:HyperLink ID="et_lien" runat="server" NavigateUrl='<%# Liens.detailsSup(Eval("sup_couriel").ToString()) %>' ToolTip='Details sur le superviseur' Text='<%# Eval("sup_nom") %>' />
                        <asp:Label ID="nosup" runat="server" Text="Sans Sup" />
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <span><em>L'étudiant n'a confirmé aucun examen !</em></span>
            </EmptyDataTemplate>
        </asp:ListView>
        <br />
        <asp:CheckBox ID="rappel" runat="server" Text="Lui envoyer un rappel pour les cours non confirmé" OnCheckedChanged="rappel_CheckedChanged" />
    </asp:Panel>
</asp:Content>

