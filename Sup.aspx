﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermTestingMaster.master" AutoEventWireup="true" CodeFile="Sup.aspx.cs" Inherits="Sup2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Détails superviseur - EdPerm</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ajax" runat="server" />
        <edPerm:Sup ID="sup_details" runat="server"/>
        <hr />
        <asp:Panel ID="assExam" runat="server" >
            <h3>Assigner un cours à un superviseur</h3>
            <table>
                <tr>
                    <td>ID</td>
                    <td><asp:TextBox ID="sup_id" runat="server" /></td>
                </tr>
                <tr><!-- Disponible que pour les cours qui ont déjà reservés -->
                    <td>Cours</td>
                    <td><asp:DropDownList ID="lst_cours" runat="server" /></td>
                </tr>
                <tr>
                    <td>Ville</td>
                    <td><asp:DropDownList ID="lst_ville" runat="server" /></td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td>
                        <asp:DropDownList ID="lst_type" runat="server">
                            <asp:ListItem Value="intra">Intra</asp:ListItem>
                            <asp:ListItem Value="final">Final</asp:ListItem>
                            <asp:ListItem Value="both">Les deux</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td><asp:Button ID="btn" runat="server" Text="Assigner" /></td>
                </tr>
            </table>
        </asp:Panel>
</asp:Content>

