﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermTestingMaster.master" AutoEventWireup="true" CodeFile="Cours.aspx.cs" Inherits="Cours" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="App_Themes/css/calendar-blue.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="uploadCours" runat="server">
        <h3>Importer une liste des cours </h3>
        <table style="width:50%; text-align:left; height:50%">
            <tr>
                <td>Importer à partir d'un classeur</td>
                <td><asp:FileUpload ID="importFichier" runat="server" /></td>
            </tr>
            <tr>
                <td></td>
                <td><asp:CheckBox ID="header" runat="server" Text="As une En-tête" /></td>
            </tr>
            <tr>
                <td><asp:Button ID="importer" runat="server" Text="Importer le classeur" OnClick="importer_Click"  /></td>
            </tr>
        </table>
        <hr />
    </asp:Panel>
    <h3>Détails sur les cours</h3>&nbsp;&nbsp;<asp:Label ID="erreur" runat="server" />
    <hr />
    <asp:Panel ID="srch" runat="server">
        <span>Liste des cours:&nbsp;</span>
        <asp:DropDownList ID="lstCours" runat="server" OnSelectedIndexChanged="lstCours_SelectedIndexChanged" />
        <hr />
    </asp:Panel>
    <table style="width:50%; text-align:justify">
        <tr>
            <td>Nom</td>
            <td><asp:TextBox ID="nomCours" runat="server" /></td>
        </tr>
        <tr>
            <td>Sigle</td>
            <td><asp:TextBox ID="sigleCours" runat="server" required="required"/></td>
        </tr>
        <tr>
            <td>Date Intra</td>
            <td><asp:Textbox ID="dateIntra" runat="server" type="date"></asp:Textbox>
            <img src="images/calender.png" />
            </td>
        </tr>
        <tr>
            <td>Date Final</td>
            <td><asp:TextBox ID="dateFinale" runat="server" type="date"></asp:TextBox>
            <img src="images/calender.png" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="insrtCours" runat="server" Text="Ajouter un cours" OnClick="insrtCours_Click"/>
                <asp:Button ID="updtCours" runat="server" Text="Mettre à jours un cours" OnClick="updtCours_Click" />
            </td>
        </tr>
    </table>
    <hr />
    <span>Nombre d'étudiants inscrits (&nbsp;<asp:Label ID="nbreInscrit" runat="server" />&nbsp;)</span><br />
    <span>Nombre d'étudiants ayant déjà confirmé pour ce cours:&nbsp;</span>
    <ul>
        <li>Intra&nbsp;(&nbsp;<asp:Label ID="nbreIntra" runat="server" /> &nbsp;) -</li>
        <li>Final&nbsp;(&nbsp;<asp:Label ID="nbreFinal" runat="server" /> &nbsp;) -</li>
    </ul>
    <!-- script side -->
    <%--<script src="App_Themes/js/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="App_Themes/js/jquery.dynDateTime.min.js" type="text/javascript"></script>
    <script src="App_Themes/js/calendar-en.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=dateIntra.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });
        //deuxième boite
        $(document).ready(function () {
            $("#<%=dateFinale.ClientID %>").dynDateTime({
                showsTime: true,
                ifFormat: "%Y/%m/%d %H:%M",
                daFormat: "%l;%M %p, %e %m,  %Y",
                align: "BR",
                electric: false,
                singleClick: false,
                displayArea: ".siblings('.dtcDisplayArea')",
                button: ".next()"
            });
        });
</script>--%>
</asp:Content>

