﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermTestingMaster.master" AutoEventWireup="true" CodeFile="Confirmations.aspx.cs" Inherits="Confirmations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Vérification des confirmations - EdPerm</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ajax" runat="server" />
    <asp:UpdatePanel ID="upPanel" runat="server">
        <ContentTemplate>
            <h4>Confirmer les reservations</h4><br />
            <asp:Label ID="erreur" runat="server" ForeColor="Red"></asp:Label>
            <hr />
            <asp:Table ID="tab1" runat="server" style="width:50%; text-align:left">
                <asp:TableRow>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell>Session:</asp:TableCell>
                    <asp:TableCell><asp:DropDownList ID="sessionActuelle" runat="server" AutoPostBack="true" OnSelectedIndexChanged="sessionActuelle_SelectedIndexChanged"></asp:DropDownList></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Cours</asp:TableCell>
                    <asp:TableCell><asp:DropDownList ID="lstCours" runat="server" OnSelectedIndexChanged="lstCours_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Date de l'examen:</asp:TableCell>
                    <asp:TableCell><asp:Label ID="dateExam" runat="server" /></asp:TableCell><asp:TableCell>Lieux Examen:</asp:TableCell><asp:TableCell>Partout
                    </asp:TableCell></asp:TableRow><asp:TableRow>
                    <asp:TableCell>Type d'Examen:</asp:TableCell><asp:TableCell>
                        <asp:DropDownList ID="typExam" runat="server" AutoPostBack="true" OnSelectedIndexChanged="typExam_SelectedIndexChanged">
                            <asp:ListItem Value="intra" Selected="True">Intra</asp:ListItem>
                            <asp:ListItem Value="final">Final</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell></asp:TableRow></asp:Table><hr /><asp:Panel ID="lowPanel" runat="server">
                <asp:ListView ID="lstCfrm" runat="server" GroupPlaceholderID="gpePlcHolder2" ItemPlaceholderID="itmPlcHolder2" OnItemDataBound="lstCfrm_ItemDataBound">
                    <LayoutTemplate>
                        <table style=" text-align:center">
                            <tr>
                                <td colspan="3">Détails sur les examens</td><td colspan="2">Copies</td><td colspan="7">Coordonnées du superviseur</td><td colspan="3">Commentaires</td></tr><tr>
                                <td>Confirmation du lieu</td><td>Ville</td><td>Cours</td><td>Scannée</td><td>Papier</td><td>Nom et prénom</td><td>Adresse</td><td>Couriel</td><td>Langue</td><td>Approbation</td><td>Trousse envoyée</td><td>Entente reçue et signée</td><td>notes</td><td>A facturer</td><td>Ont déjà confirmé</td></tr><asp:PlaceHolder ID="gpePlcHolder2" runat="server"></asp:PlaceHolder>
                        </table>
                    </LayoutTemplate>
                    <GroupTemplate>
                        <asp:PlaceHolder ID="itmPlcHolder2" runat="server"></asp:PlaceHolder>
                    </GroupTemplate>
                    <ItemTemplate>
                        <tr>
                        <td rowspan="2"><asp:Checkbox ID="chkLieu" runat="server" CssClass="css-checkbox" Checked='<%# Eval("res_lieu") %>' AutoPostBack="true" OnCheckedChanged="chkLieu_CheckedChanged" /></td>
                        <td rowspan="2"><asp:Label ID="ville" runat="server" Text='<%# Eval("le_ville") %>' /></td>
                        <td rowspan="2"><asp:HyperLink ID="sigle_lien" runat="server" NavigateUrl='<%# Liens.detailsCours(Eval("sigle").ToString()) %>' ToolTip='Details sur le superviseur' Text='<%# Eval("sigle") %>' /></td>
                        <td rowspan="2"><asp:Checkbox ID="cpPapier" runat="server" CssClass="css-checkbox" Checked='<%# Eval("res_cpPapier") %>' AutoPostBack="true" OnCheckedChanged="cpPapier_CheckedChanged" /></td>
                        <td rowspan="2"><asp:Checkbox ID="cpElctrk" runat="server" Checked='<%# Eval("res_cpElectr") %>' AutoPostBack="true" OnCheckedChanged="cpElctrk_CheckedChanged" /></td>
                        <td>
                            <asp:HyperLink ID="sup_lien" runat="server" NavigateUrl='<%# Liens.detailsSup(Eval("sup_couriel").ToString()) %>' ToolTip='Details sur le superviseur' Text='<%# Eval("sup_nom") %>' />
                        </td>
                        <td rowspan="2"><asp:Label ID="sup_adr" runat="server" Text='<%# Eval("sup_adrsCiv") %>' /></td>
                        <td rowspan="2"><asp:Label ID="sup_couriel" runat="server" Text='<%# Eval("sup_couriel") %>' /></td>
                        <td rowspan="2"><asp:Label ID="sup_lan" runat="server" Text='<%# Eval("sup_langue") %>' /></td>
                        <td rowspan="2"><asp:Checkbox ID="sup_lcked" runat="server" Checked='<%# Eval("res_sup") %>' AutoPostBack="true" OnCheckedChanged="sup_lcked_CheckedChanged" /></td>
                        <td rowspan="2"><asp:Checkbox ID="trousseSent" runat="server" Checked='<%# Eval("res_trousse") %>' AutoPostBack="true" OnCheckedChanged="trousseSent_CheckedChanged" /></td>
                        <td rowspan="2"><asp:Checkbox ID="ententeSent" runat="server" Checked='<%# Eval("res_entente") %>' AutoPostBack="true" OnCheckedChanged="ententeSent_CheckedChanged" /></td>
                        <td rowspan="2"><asp:TextBox ID="notes" runat="server" Text='<%# Eval("res_note") %>' TextMode="MultiLine" OnTextChanged="notes_TextChanged" AutoPostBack="true" /></td><td rowspan="2"><asp:TextBox ID="facture" runat="server" Text='<%# Eval("res_facture") %>' TextMode="MultiLine" OnTextChanged="facture_TextChanged" AutoPostBack="true" /></td><td rowspan="2"><%# SessionAccess.nbreConf(SId, TypeExam, Nrc,Eval("le_ville").ToString()) %></td></tr>
                        <tr>
                            <td><asp:Label ID="sup_prenom" runat="server" Text='<%# Eval("sup_prenom") %>' /></td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>Aucune vérification à faire pour le moment</EmptyDataTemplate></asp:ListView></asp:Panel></ContentTemplate></asp:UpdatePanel>
</asp:Content>

