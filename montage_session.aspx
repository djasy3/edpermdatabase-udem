﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="montage_session.aspx.cs" Inherits="montage_session" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title> Montage session </title>

        <!-- style sheets -->
        <link rel="stylesheet" type="text/css" href="App_Themes/css/frame.css">
        <link rel="stylesheet" type="text/css" href="App_Themes/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="App_Themes/css/skeleton.css"/>
        <link rel="stylesheet" type="text/css" href="App_Themes/css/form_admin.css"/>

        <!-- script files -->
        <script type="text/javascript" src="App_Themes/js/jquery.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <meta charset="utf-8"/>
    </head>
<body>
    <form id="form1" runat="server">
 <div id="wrapper">
          <header class="blue_bar">
            <div class="container">
                <div class="three columns logo">
                    <p class="">
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">

               <div class="ten columns alerts_admin">
                 <span> Creation du cours, reussie !! </span>
               </div><!-- end alerts admin -->

               <div class="ten columns mount_session"> <!-- This replaces the form tag -->
                <div id="Tabs" role="tabpanel">
                   <!-- nav tabs to list the steps -->
                <ul class="nav nav-tabs twelve columns" role="tablist" >
                 <li role="presentation" class="active">
                    <a href="#createSession" aria-controls="createSession" role="tab" data-toggle="tab">Session</a>
                 </li>
                 <li role="presentation">
                   <a href="#createCourse" aria-controls="createCourse" role="tab" data-toggle="tab">Cours</a>
                 </li>
                 <li role="presentation">
                    <a href="#createStudent" aria-controls="createStudent" role="tab" data-toggle="tab">Étudiants</a>
                 </li>
                 <li role="presentation">
                    <a href="#createMessages" aria-controls="createMessages" role="tab" data-toggle="tab">Messages</a>
                 </li>
                 <li role="presentation">
                    <a href="#createCities" aria-controls="createCities" role="tab" data-toggle="tab">
                        Villes d'examen
                    </a>
                 </li>
                </ul> <!-- end of tabs ul -->

                <div class="tab-content"> <!-- purely for the sake of bootstrap -->
                  <div role="tabpanel" class="eight columns tab-pane active first_step" id="createSession">
                    <p class="twelve columns"> Informations sur la session </p>
                     <asp:TextBox ID="a" runat="server" CssClass="twelve columns" name="" placeholder="nom de session"/>
                    <asp:TextBox ID="b" runat="server" CssClass="twelve columns" name="" placeholder="ann&eacute;e"/>
                    <asp:Button ID="c" runat="server" type="button" value="infos cours >"/>
                  </div> <!-- end first setp -->

                  <div role="tabpanel" class="twelve columns tab-pane second_step" id="createCourse">
                     <div class="twelve columns import_file">
                       <p class="six columns">
                          <span>Importer à partir d'un fichier excel</span>
                       </p>
                       <asp:TextBox ID="importListCourses" runat="server" class="four columns" type="file" placeholder="importer fichier"/>
                     </div> <!-- end import file -->
                     <div class="five columns insert_manually">
                      <p> Ajouter manuellement le cours </p>
                      <asp:TextBox ID="d" runat="server" CssClass="twelve columns" name="" placeholder="sigle du cours"/>
                      <asp:TextBox ID="e" runat="server" CssClass="twelve columns" name="" placeholder="titre du cours"/>
                      <asp:TextBox ID="f" runat="server" CssClass="twelve columns" name="" placeholder="date intra"/>
                      <asp:TextBox ID="g" runat="server" CssClass="twelve columns" name="" placeholder="date final"/>
                      <asp:TextBox ID="h" runat="server" type="button" value="Ajouter un cours" />
                     </div><!-- end insert manually -->
                     <div class="six columns courseInstant">
                      <p> Liste des cours que vous venez d'ajouter </p>
                       <ul class="twelve columns">
                           <li><a href="#"> Math 2000 </a></li>
                           <li><a href="#"> Math 4000 </a></li>
                       </ul> <!-- end DB populated list -->
                     </div><!-- end course instant -->
                  </div> <!-- end second setp -->

                  <div class="twelve columns tab-pane third_step" id="createStudent">
                    <p class="twelve columns step_title">
                        Ajouter les étudiant(e)s à partir d'une liste
                    </p>
                    <div class="twelve columns">
                    <p class="six columns">
                     <span>Choisir un cours pour ajouter les étudiants </span>
                    </p>
                    <select class="five columns availableCourses">
                      <option> Course 1</option>
                      <option> Course 2</option>
                    </select>
                    </div><!-- end div1 -->
                    <div class="twelve columns">
                    <p class="six columns">
                     <span> Veuillez importer la liste des étudiants s.v.p</span>
                    </p>
                    <asp:TextBox ID="importListStudent" runat="server" CssClass="five columns" type="file"/>
                    </div><!-- end div2 -->
                    <%--<asp:TextBox ID="j" runat="server" type="button" value="Ajouter"/>--%>
                      <asp:Button ID="k" runat="server" Text="ajouter" />
                    <div class="twelve columns imported_students">
                        <!-- placeholder to show imported students -->
                    </div><!-- imported students -->
                  </div> <!-- end of third step -->
                  <div class="twelve columns tab-pane fourth_step" id="createMessages">
                      <!-- create messages here -->
                      <asp:Button ID="msg" runat="server" Text="blabla" />
                  </div>
                  <div class="twelve columns tab-pane fifth_step" id="createCities">
                      <!-- create cities here -->
                  </div>
                </div><!-- end of tab-content -->
</div>
                   <asp:HiddenField ID="TabName" runat="server" />
               </div>

</div>

          
            </div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
                      <footer> <!-- release the display to see the footer -->
               <div class="container">
                <div class="one column logo">
                        <a href="#">
                            <img src="images/logo_udem_foot.png" />
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="four columns copyright">
                    <span class="twelve columns"> &copy;copyright 2015 - tous droits reservés </span>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div> <!-- wrapper -->
    </form>
    <script type="text/javascript">
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "createSession";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });
</script>
</body>
</html>
