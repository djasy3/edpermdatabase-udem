﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermTestingMaster.master" AutoEventWireup="true" CodeFile="FormReservation.aspx.cs" Inherits="FormReservation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>Confirmer votre présence à l'examen</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ajax" runat="server" />
        <h3>Examen supervisé - Confirmation du lieu</h3>
        <asp:Panel ID="etapeCommune" runat="server">
            <asp:Label ID="Label1" runat="server" Text="Sigle du Cours:" /><asp:Label ID="Label2" runat="server" ForeColor="Red">*</asp:Label><br />
            <asp:TextBox ID="sigle_cours" runat="server" required="required" />&nbsp;<asp:Label ID="errorText" runat="server" ForeColor="Red" />
            <br /><br />
            <fieldset>
                <legend>Coordonnées de l'étudiant</legend>
                <asp:TextBox ID="ni" runat="server" required="required" placeholder="NI"  /><br />
                <asp:TextBox ID="nom" runat="server" placeholder="Nom"  /><br />
                <asp:TextBox ID="prenom" runat="server" placeholder="Prenom"  /><br />
                <asp:TextBox ID="email" type="text" required="required" runat="server" placeholder="E-mail" /><br />
                <asp:TextBox ID="adresse" runat="server"  placeholder="Adresse" /><br />
                <asp:TextBox ID="phone_jour" type="text" runat="server"  placeholder="Téléphone jour" /><br />
                <asp:TextBox ID="phone_nuit" runat="server"  placeholder="Téléphone nuit" /><br />
            </fieldset>
            <p>
L'équipe des services d'encadrement de l'Éducation permanente de l'Université de Moncton invite l'étudiant d'un cours en ligne à confirmer le lieu où il souhaite effectuer son examen supervisé.</p>
            <span> Je ferai mon examen intra et final:</span>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:RadioButtonList ID="chckVille" runat="server" RepeatColumns="2" OnSelectedIndexChanged="chckVille_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Au meme endroit</asp:ListItem>
                        <asp:ListItem Value="1">Aux endroits différents</asp:ListItem>
                    </asp:RadioButtonList>
                    <hr />
                    <asp:Label ID="msg" runat="server" ForeColor="Red" Text="Veuillez spécifiez un endroit" />
                    <asp:Panel ID="mmEndrtPanel" runat="server">
                        Je serai:&nbsp;
                        <edPerm:Lieux ID="mmPlc" runat="server" />
                        <br />
                        <edPerm:HorsNb ID="mmPlcHnb" runat="server"/>
                        <asp:CheckBox ID="signalMm" runat="server" Text="Si un changement devait survenir à ma situation, je communiquerai avec les Services d'encadrement dans les plus brefs délais"/>
                        <hr />
                    </asp:Panel>
                    <asp:Wizard ID="dfrtEndrtWizard" runat="server" DisplaySideBar="False" ActiveStepIndex="0" OnActiveStepChanged="dfrtEndrtWizard_ActiveStepChanged">
                        <FinishNavigationTemplate>
                            <asp:Button ID="FinishPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious" Text="Intra" />
                            <asp:Button ID="FinishButton" runat="server" CommandName="MoveComplete" Text="Confirmer" />
                        </FinishNavigationTemplate>
                        <StartNavigationTemplate>
                            <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" Text="Final" />
                        </StartNavigationTemplate>
                        <WizardSteps>
                            <asp:WizardStep ID="WizardStep1" runat="server" Title="Intra">
                                Pour mon examen intra je serai:&nbsp;
                                <edPerm:Lieux ID="intraPlc" runat="server" /><br />
                                <edPerm:HorsNb ID="intraHnb" runat="server" /><br />
                                <asp:CheckBox ID="SignalChgmntIntra" runat="server" Text="Si un changement devait survenir à ma situation, je communiquerai avec les Services d'encadrements dans les plus brefs délais"/>
                                <hr />
                            </asp:WizardStep>
                            <asp:WizardStep ID="WizardStep2" runat="server" Title="Final">
                                Pour mon examen final je serai:&nbsp;
                                <edPerm:Lieux ID="finalPlc" runat="server" /><br />
                                <edPerm:HorsNb ID="finalHnb" runat="server" /><br />
                                <asp:CheckBox ID="SignalChgmntFinal" runat="server" Text="Si un changement devait survenir à ma situation, je communiquerai avec les Services d'encadrements dans les plus brefs délais"/>
                                <hr />
                            </asp:WizardStep>
                        </WizardSteps>
                    </asp:Wizard>
                    <asp:Panel ID="final" runat="server">
                        <asp:Button ID="finalBtn" runat="server" Text="Confirmer" OnClick="finalBtn_Click"/>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="finalBtn" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
        <br />
        <br />
</asp:Content>

