﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Cours : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string lien;
        erreur.Visible = false;
        try
        {
            CurrentSession sess = SessionAccess.getCurrentSession();
            if (!IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    lien = Request.QueryString["lq"];
                    //
                    if (lien == "noId")
                    {
                        srch.Visible = false;
                        insrtCours.Visible = true;
                        updtCours.Visible = false;
                    }
                    else if (!string.IsNullOrEmpty(lien))
                    {
                        lstCours.DataSource = SessionAccess.getListeCours();
                        lstCours.DataTextField = "c_sigle";
                        lstCours.DataValueField = "c_id";
                        lstCours.DataBind();

                        ListItem item = new ListItem();
                        item.Text = "<Choisir un cours>";
                        item.Value = "0";
                        lstCours.Items.Insert(0, item);

                        srch.Visible = true;
                        insrtCours.Visible = false;
                        updtCours.Visible = true;
                        //on charge les détails sur le cour
                        DetailsCours detailsCours = SessionAccess.getDetailsCours(int.Parse(lien));
                        //
                        if (detailsCours.nrc > 0)
                        {
                            loadDetails(detailsCours);
                            //
                        }
                    }
                    else
                        Response.Redirect(Liens.detailsCours(""));
                }
                else
                    Response.Redirect(Liens.detailsCours(""));
            }
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
    }
    //méthode pour afficher les détails d'un cours
    protected void loadDetails(DetailsCours det)
    {
        nomCours.Text = det.nom;
        sigleCours.Text = det.sigle;
        dateIntra.Text = det.dateIntra.ToLongDateString();
        dateFinale.Text = det.dateFinal.ToString();
    }
    //méthode qui permet d'enregistrer un nouveau cours
    protected void insrtCours_Click(object sender, EventArgs e)
    {
        try
        {
            int nrc = ExamAccess.getNewNrc();
            if (SessionAccess.setCours(sigleCours.Text, nomCours.Text, DateTime.Parse(dateIntra.Text), DateTime.Parse(dateFinale.Text)))
            {
                erreur.Text = "Cours enregistré avec succès !";
                erreur.ForeColor = System.Drawing.Color.Green;
                erreur.Visible = true;
            }
            else
                throw new Exception("Enregistrement echoué !");
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
    }
    //méthode qui permet de mettre à jours un cours
    protected void updtCours_Click(object sender, EventArgs e)
    {
        try
        {
            int nrc = ExamAccess.getNrc(sigleCours.Text);
            if (SessionAccess.updtCours(nrc, sigleCours.Text, nomCours.Text, DateTime.Parse(dateIntra.Text), DateTime.Parse(dateFinale.Text)))
            {
                erreur.Text = "Cours modifié avec succès !";
                erreur.ForeColor = System.Drawing.Color.Green;
                erreur.Visible = true;
            }
            else
                throw new Exception("Echec de modification !");
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
    }
    protected void importer_Click(object sender, EventArgs e)
    {
        try
        {
            if (importFichier.HasFile)
            {
                //on récupère le nom du fichier excel à importer
                string fileName = Path.GetFileName(importFichier.PostedFile.FileName);
                //l'extension du fichier
                string extension = Path.GetExtension(importFichier.PostedFile.FileName);
                //le chemin du dossier qui contient le fichier
                string folderName = ConfigurationManager.AppSettings["FolderPath"];
                //le chemin du fichier
                string filePath = Server.MapPath(folderName + fileName);
                importFichier.SaveAs(filePath);//on sauvegarde le fichier uploadé
                //on fait appel à la méthode qui permet de copier les données dans un dataset !
                DataTable dt;
                if (header.Checked)
                    dt = ExcelAccess.ImportToDataTable(filePath, extension, "Oui");
                else
                    dt = ExcelAccess.ImportToDataTable(filePath, extension, "Non");
                //
                if (ExcelAccess.ImportCours(dt))
                {
                    erreur.Text = "Importation reussie";
                    erreur.ForeColor = System.Drawing.Color.Green;
                    erreur.Visible = true;
                }
                else
                    throw new Exception("L'importation n'a pas réussi!");
            }
            else
                throw new Exception("Vous devez choisir un fichier");
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
    }
    protected void lstCours_SelectedIndexChanged(object sender, EventArgs e)
    {
        //on charge les détails sur le cour
        if (lstCours.SelectedValue != "0")
        {
            DetailsCours detailsCours = SessionAccess.getDetailsCours(int.Parse(lstCours.SelectedValue));
            //
            if (detailsCours.nrc > 0)
            {
                loadDetails(detailsCours);
                //
            }
        }
        else
        {
            string lien = Request.QueryString["lq"];
            //
            DetailsCours detailsCours = SessionAccess.getDetailsCours(int.Parse(lien));
            //
            if (detailsCours.nrc > 0)
            {
                loadDetails(detailsCours);
                //
            }
        }
    }
}