﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FormConfirmationExam : System.Web.UI.Page
{
    CurrentSession sessionCourante = SessionAccess.getCurrentSession();
    //on renvoi l'id de la session
    public int Sid
    {
        get { return sessionCourante.Id; }
    }
    //on renvoi le nrc du cours
    public int Nrc
    {
        get { return int.Parse(lstCours.SelectedValue); }
    }
    //on renvoi les infos sur l'étudiant
    public string Nom
    {
        get 
        {
            if (!string.IsNullOrEmpty(nom.Text))
            {
                return nom.Text;
            }
            else throw new Exception("Le Nom ne doit pas être vide");
        }
    }
    //
    public string Prenom
    {
        get
        {
            if (!string.IsNullOrEmpty(prenom.Text))
            {
                return prenom.Text;
            }
            else throw new Exception("Le Prenom ne doit pas être vide");
        }
    }
    //
    public string Ni
    {
        get
        {
            if (!string.IsNullOrEmpty(ni.Text))
            {
                return ni.Text;
            }
            else throw new Exception("Le NI ne doit pas être vide");
        }
    }
    //
    public string Email
    {
        get
        {
            if (!string.IsNullOrEmpty(email.Text))
            {
                return email.Text;
            }
            else throw new Exception("L'adresse de couriel ne doit pas être vide");
        }
    }
    //
    public string Adresse
    {
        get
        {
            //if (string.IsNullOrEmpty(adresse.Text))
            //{
                return adresse.Text;
            //}
            //else throw new Exception("L'adresse ne doit pas être vide");
        }
    }
    //
    public string PhoneJour
    {
        get
        {
            if (!string.IsNullOrEmpty(phone_jour.Text))
            {
                return phone_jour.Text;
            }
            else throw new Exception("Veuillez entrer votre numéro de téléphone");
        }
    }
    //
    public string PhoneNuit
    {
        get
        {
            return phone_nuit.Text;
        }
    }
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lstCours.DataSource = SessionAccess.getListeCours();
            lstCours.DataTextField = "c_sigle";
            lstCours.DataValueField = "c_id";
            lstCours.DataBind();
            //Examens même place liste ville nb
            this.chargerVilles(lstVilleNb, true);
            //Examens même place liste ville hors du nouveau brunswick
            this.chargerVilles(lstVilleHnb, false);
            //Examen intra place différente liste ville au nb
            this.chargerVilles(intraLstVilleNbDifferentPlace, true);
            //Examen intra place différente liste ville hors
            this.chargerVilles(intraLstVilleHnbDifferentPlace, false);
            //Examen final place différente liste ville au nb
            this.chargerVilles(finalLstVilleNbDifferentPlace, true);
            //Examen final place différente liste ville hors nb
            this.chargerVilles(finalLstVilleHnbDifferentPlace, false);
            test.Visible = false;
            debug.Visible = false;
        }
    }

    protected void chargerVilles(DropDownList list, bool inOut)
    {
        if (inOut)//true si c'est à l'intérieur du nouveau brunswick
            list.DataSource = ExamAccess.getVillesNb();
        else
            list.DataSource = ExamAccess.getVillesCan();
        //ensuite le reste du boulot
        list.DataTextField = "le_ville";
        list.DataValueField = "le_id";
        list.DataBind();
    }
    //méthode pour confirmer la reservation sans superviseur
    protected void save(string typeExam, int idLieuExamen)
    {
        if (!ExamAccess.aDejaConfirmeExamen(Ni, Nrc, Sid))
        {
            //si l'étudiant est inscrit, on met à jour ses infos dans la base;
            SessionAccess.updateEtudiant(Ni, Nom, Prenom, Email, Adresse, PhoneJour, PhoneNuit, "");
            //ensuite on enregistre l'examen à présenter
            ExamAccess.setExam(Nrc, typeExam, Ni, idLieuExamen, Sid);
            //ensuite on fait la reservation
            ExamAccess.setReservation(ExamAccess.getIdCentre(Nrc, Ni, typeExam, Sid));
        }
        else
            throw new Exception("Une confirmation a déjà été faite pour ce cours et pour cet étudiant\n veuillez consulter votre e-mail pour avoir le lien de modification");

    }
    //méthode pour confirmer la reservation avec superviseur
    protected void save(string typeExam, int idLieuExamen, string superviseur)
    {
        if (!ExamAccess.aDejaConfirmeExamen(Ni, Nrc, Sid))
        {
            //si l'étudiant est inscrit, on met à jour ses infos dans la base;
            SessionAccess.updateEtudiant(Ni, Nom, Prenom, Email, Adresse, PhoneJour, PhoneNuit, "");
            //ensuite on enregistre l'examen à présenter
            ExamAccess.setExam(Nrc, typeExam, Ni, superviseur, idLieuExamen, Sid);
            //ensuite on fait la reservation
            ExamAccess.setReservation(ExamAccess.getIdCentre(Nrc, ni.Text, typeExam, Sid));
        }
        else
            throw new Exception("Une confirmation a déjà été faite pour ce cours et pour cet étudiant\n veuillez consulter votre e-mail pour avoir le lien de modification");
    }
    //méthode pour enregistrer un superviseur
    public string setNewSuperviseur(Controls_SupDetail sup)
    {
        //méthode pour enregistrer un nouveau !
        if (ExamAccess.setSuperviseur(sup.SupNom, sup.SupPrenom, sup.SupPoste, sup.SupEts, sup.SupCouriel, sup.SupPhone, sup.SupAdrsCiv, sup.SupLangue, "Non Aprv.", false))
            return ExamAccess.getSupId(sup.SupCouriel);//ensuite on prend son id
        else
            throw new Exception("Enregistrement du superviseur échoué, veuillez contacter l'administrateur !");
    }
    //méthode pour enregistrer les informations venant du formulaire
    public void saveForm()
    {
        //debug.Text = "..."+chckYBtn.Checked.ToString();
        if (chckYBtn.Checked)//si les deux examens se passent au même endroit
        {
            //debug.Text += "2mmplace";
            if(chckNb.Checked)//au nb
            {
                //debug.Text += " nb chosen";
                //méthode pour enregistrer au nb
                //on aura besoin des infos de l'étudiants, cours, ville au nbnt type d'examen
                save("intra", int.Parse(lstVilleNb.SelectedValue));
                save("final", int.Parse(lstVilleNb.SelectedValue));
            }
            else if (chckHnb.Checked)//hors du nb
            {
                string supIdIntra = setNewSuperviseur(supIntra);
                //on vérifie également si pour les deux examens il aura un seul superviseur ou deux
                if (sameSup.Checked)
                {
                    //enfin on confirme l'enregistrement               //méthode pour enregistre le superviseur
                    save("intra", int.Parse(lstVilleHnb.SelectedValue), supIdIntra );
                    save("final", int.Parse(lstVilleHnb.SelectedValue), supIdIntra);
                }
                else//cas ou chaque examen aura son propre superviseur
                {
                    string supIdFinal = setNewSuperviseur(supFinal);
                    save("intra", int.Parse(lstVilleHnb.SelectedValue), supIdIntra);
                    save("final", int.Parse(lstVilleHnb.SelectedValue), supIdFinal);
                }
            }
        }
        else if (chckNBtn.Checked)//les deux examens ne se passent pas au même endroit
        {
            //gerons pourl'intra !
            if (YintraNb.Checked)//oui intra au nb
            {
                save("intra", int.Parse(intraLstVilleNbDifferentPlace.SelectedValue));
            }
            else if (NintraNb.Checked)//non intra au nb
            {
                //trouvons le superviseur et ensuite on enregistre
                save("intra", int.Parse(intraLstVilleHnbDifferentPlace.SelectedValue), setNewSuperviseur(supIntra));
            }
            else //ca veut dire pas besoin de superviseur !on insinue que c'est au nouveau brunswick !
            {
                save("intra", int.Parse(intraLstVilleNbDifferentPlace.SelectedValue));
            }
            //gérons le final
            if (YfinalHnb.Checked)//oui final au nb
            {
                //on reserve pour l'examen, 
                save("final", int.Parse(finalLstVilleNbDifferentPlace.SelectedValue));
            }
            else if (NfinalHnb.Checked)//non final au nb
            {
                //trouvons le superviseur et ensuite on enregistre
                save("final", int.Parse(finalLstVilleHnbDifferentPlace.SelectedValue), setNewSuperviseur(supFinal));
            }
            else
            {
                save("final", int.Parse(finalLstVilleNbDifferentPlace.SelectedValue));
            }
        }
        else
        {
            test.Text = "Rien choisit";
            test.Visible = true;
        }
    }
    //
    protected void btn_Click(object sender, EventArgs e)
    {
        try
        {
            //avant d'enregistrer quoi que ce soit, on vérifie d'abords si l'étudiant est inscrit !
            if (SessionAccess.estInscrit(Ni, Sid, Nrc))
            {
                //on sauvegarde les info dans la base de donées
                saveForm();
                //on envoi l'email à l'étudiant

                //on met un flag sur son inscription
                SessionAccess.setAck(Sid, Nrc, Ni, true);
                //rédirection vers la page merci
                Response.Redirect("merci.aspx");
            }
            else
                throw new Exception("Vous n'êtes pas inscrit à ce cours pour cette session?");
        }
        catch (Exception ex)
        {
            test.Text = ex.Message;
            test.ForeColor = System.Drawing.Color.Red;
            test.Visible = true;
        }
    }
}