﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class prod_insc_etudiant : System.Web.UI.Page
{
    bool yBtn = false;
    bool nBtn = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lstCours.DataSource = SessionAccess.getListeCours();
            lstCours.DataTextField = "c_sigle";
            lstCours.DataValueField = "c_id";
            lstCours.DataBind();
            //Examens même place liste ville nb
            this.chargerVilles(lstVilleNb, true);
            //Examens même place liste ville hors du nouveau brunswick
            this.chargerVilles(lstVilleHnb, false);
            //Examen intra place différente liste ville au nb
            this.chargerVilles(intraLstVilleNbDifferentPlace, true);
            //Examen intra place différente liste ville hors
            this.chargerVilles(intraLstVilleHnbDifferentPlace, false);
            //Examen final place différente liste ville au nb
            this.chargerVilles(finalLstVilleNbDifferentPlace, true);
            //Examen final place différente liste ville hors nb
            this.chargerVilles(finalLstVilleHnbDifferentPlace, false);
            saveForm();
        }
    }
            //
    protected void chargerVilles(DropDownList list, bool inOut)
    {
        if (inOut)//true si c'est à l'intérieur du nouveau brunswick
            list.DataSource = ExamAccess.getVillesNb();
        else
            list.DataSource = ExamAccess.getVillesCan();
        //ensuite le reste du boulot
        list.DataTextField = "le_ville";
        list.DataValueField = "le_id";
        list.DataBind();
    }
    //méthode pour enregistrer les informations venant du formulaire
    public void saveForm()
    {
        if(yBtn)
        {
            //ca veut dire que l'étudiant souhaite composer ses deux examens au même endroit !
            test.Text = "oui selectioné";
            //test.Visible = true;

        }
        else if(nBtn)//
        {
            test.Text = "non selectioné";
            //test.Visible = true;
        }
    }
    protected void YesBtn_Click(object sender, EventArgs e)
    {
        yBtn = true;
        nBtn = false;
    }
    protected void NoBtn_Click(object sender, EventArgs e)
    {
        yBtn = false;
        nBtn = true;
    }
    protected void btn_Click(object sender, EventArgs e)
    {
        if (yBtn == true)
        {
            //ca veut dire que l'étudiant souhaite composer ses deux examens au même endroit !
            test.Text = "oui selectioné";
            //test.Visible = true;

        }
        else if (nBtn)//
        {
            test.Text = "non selectioné";
            //test.Visible = true;
        }
    }


}