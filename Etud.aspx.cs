﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class Etud2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string lien;
        erreur.Visible = false;
        CurrentSession sess = SessionAccess.getCurrentSession();
        try
        {
            if(!IsPostBack)
            {
                if (Request.QueryString.Count > 0)
                {
                    lien = Request.QueryString["lq"];
                    //
                    if (lien == "noId")
                    {
                        lstCours.DataSource = SessionAccess.getListeCours();
                        lstCours.DataTextField = "c_sigle";
                        lstCours.DataValueField = "c_id";
                        lstCours.DataBind();
                        //on obtient les données de la bd en passant par la couche 2(sessionAccess)
                        lstSession.DataSource = SessionAccess.getSessions();
                        lstSession.DataTextField = "s_nom";
                        lstSession.DataValueField = "s_id";
                        lstSession.DataBind();
                        lstSession.SelectedValue = sess.Id.ToString();
                        //
                        rappel.Visible = false;
                        ajoutEtudiant.Visible = true;
                        et_ni.ReadOnly = false;
                        détailsEtudiant.Visible = false;
                        notice.Visible = true;
                        notice.Text = "N'oubliez de préciser le cours dans lequel vous inscrivez l'étudiant";
                        updtEt.Visible = false;
                        setEt.Visible = true;
                    }
                    else if (!string.IsNullOrEmpty(lien))
                    {
                        //on charge les détails sur l'étudiant
                        Etudiant etud = ExamAccess.getEtudiantDetails(lien);
                        //
                        if (etud.id != null)
                        {
                            loadDetails(etud);
                            //on charge le nombre de cours aux l'étudiant est inscrit !
                            lstCoursInscrits.DataSource = SessionAccess.getListeCours(etud.id, sess.Id);
                            lstCoursInscrits.DataTextField = "c_sigle";
                            lstCoursInscrits.DataValueField = "c_id";
                            lstCoursInscrits.DataBind();
                            //on charge également les cours déjà confirmés par l'étudiant !
                            lstCoursConfirmE.DataSource = SessionAccess.getCoursConfrimE(etud.id, sess.Id);
                            lstCoursConfirmE.DataBind();
                            //
                            rappel.Visible = true;
                            ajoutEtudiant.Visible = false;
                            et_ni.ReadOnly = true;
                            détailsEtudiant.Visible = true;
                            notice.Visible = false;
                            updtEt.Visible = true;
                            setEt.Visible = false;
                        }
                    }
                    else
                        Response.Redirect(Liens.detailsEtud(""));
                }
                else
                    Response.Redirect(Liens.detailsEtud(""));
            }
            
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
    }
    //méthode pour mettre à  jour les infos sur l'étudiant
    protected void updtEt_Click(object sender, EventArgs e)
    {
        try
        {
            if (SessionAccess.updateEtudiant(et_ni.Text, et_nom.Text, et_prenom.Text, et_couriel.Text, et_adrs.Text, et_phoneJr.Text, et_phoneNt.Text, et_note.Text))
            {
                //
                erreur.Text = "Mise à jour réussi !";
                erreur.ForeColor = System.Drawing.Color.Green;
                erreur.Visible = true;
            }
            else
                throw new Exception("Mise à jour échouée !");
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
    }
    //
    protected void loadDetails(Etudiant etud)
    {
        et_ni.Text = etud.id;
        et_nom.Text = etud.nom;
        et_prenom.Text = etud.prenom;
        et_adrs.Text = etud.adresse;
        et_couriel.Text = etud.couriel;
        et_phoneJr.Text = etud.telJour;
        et_phoneNt.Text = etud.telSoir;
        et_note.Text = etud.notes;
    }
    protected void lstCoursConfirmE_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        Label nosup;
        HyperLink et_lien;
        if(e.Item.ItemType == ListViewItemType.DataItem)
        {
            nosup = (Label)e.Item.FindControl("nosup");
            et_lien = (HyperLink)e.Item.FindControl("et_lien");
            //et_lien.Visible = false;
            //nosup.Visible = false;
            //
            System.Data.DataRowView rowView = e.Item.DataItem as System.Data.DataRowView;
            string lien = rowView["sup_nom"].ToString();
            if (string.IsNullOrEmpty(lien))
            {
                nosup.Visible = true;
                et_lien.Visible = false;
            }
            else
            {
                et_lien.Visible = true;
                nosup.Visible = false;
            }
            
        }
    }
    protected void rappel_CheckedChanged(object sender, EventArgs e)
    {
        if (rappel.Checked == true)
        {
            //on envoi un e-mail pour rappeller au user à propos des examens non confirmé
        }
    }
    protected void importer_Click(object sender, EventArgs e)
    {
        try
        {
            if (importFichier.HasFile)
            {
                //on récupère le nom du fichier excel à importer
                string fileName = Path.GetFileName(importFichier.PostedFile.FileName);
                //l'extension du fichier
                string extension = Path.GetExtension(importFichier.PostedFile.FileName);
                //le chemin du dossier qui contient le fichier
                string folderName = ConfigurationManager.AppSettings["FolderPath"];
                //le chemin du fichier
                string filePath = Server.MapPath(folderName + fileName);
                importFichier.SaveAs(filePath);//on sauvegarde le fichier uploadé
                //on fait appel à la méthode qui permet de copier les données dans un dataset !
                DataTable dt;
                if (header.Checked)
                    dt = ExcelAccess.ImportToDataTable(filePath, extension, "Oui");
                else
                    dt = ExcelAccess.ImportToDataTable(filePath, extension, "Non");
                //
                if (ExcelAccess.ImportEtudiant(dt, int.Parse(lstCours.SelectedValue), int.Parse(lstSession.SelectedValue)))
                {
                    erreur.Text = "Importation reussie";
                    erreur.ForeColor = System.Drawing.Color.Green;
                    erreur.Visible = true;
                }
                else
                    throw new Exception("L'importation n'a pas réussi!");
            }
            else
                throw new Exception("Vous devez choisir un fichier");
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message + ex.StackTrace;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
        
    }
    //méthode pour mettre à jour les info de l'étudiant dans la base de données
    protected void setEt_Click(object sender, EventArgs e)
    {
        Etudiant etud = new Etudiant();
        etud.id = et_ni.Text;
        etud.nom = et_nom.Text;
        etud.prenom = et_prenom.Text;
        etud.adresse = et_adrs.Text;
        etud.couriel = et_couriel.Text;
        etud.telJour = et_phoneJr.Text;
        etud.telSoir = et_phoneNt.Text;
        etud.notes = et_note.Text;
        //
        try
        {
            //on enregistre l'étudiant
            SessionAccess.SetEtudiants(etud);
            //on l'inscrit à meme temps
            SessionAccess.SetInscription(int.Parse(lstSession.SelectedValue), int.Parse(lstCours.SelectedValue), etud.id);
            //
            erreur.Text = "Enregistrement reussie";
            erreur.ForeColor = System.Drawing.Color.Green;
            erreur.Visible = true;

        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
    }
}