﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="inscription_etudiant.aspx.cs" Inherits="inscription_etudiant" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" class="no-js">
<head runat="server">
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
        <meta charset="utf-8"/>
        <title> Renseignements etudiant(e)s et superviseur(e)s upload </title>
    <link href="App_Themes/css/bootstrap.css" rel="stylesheet" />
    <link href="App_Themes/css/skeleton.css" rel="stylesheet" />
    <link href="App_Themes/css/flaticon.css" rel="stylesheet" />
     <link href="App_Themes/css/form_etudiant.css" rel="stylesheet" />
        <link href="App_Themes/css/frame.css" rel="stylesheet" />
        <script type="text/javascript" src="App_Themes/js/jquery.js"></script>
        <script type="text/javascript" src="App_Themes/js/bootstrap.js"></script>
        <script type="text/javascript" src="App_Themes/js/custom_validate.js"></script>
        <script type="text/javascript" src="App_Themes/js/form_etudiant.js"></script>
        <script type="text/javascript">
            // you really want me to close this sheett !! 
        </script>        
</head>

<body>
     <div id="wrapper">
          <header class="blue_bar">
            <div class="container">
                <div class="three columns logo">
                    <p class="">
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">
                <form id="info_etudiant" runat="server" class="seven columns studentBooking">
 <!-- *********************************** coordonnées de l'etudiant **************************************** -->
                    <div id="accordion">
                        <h3> Coordonn&eacute;es de l'étudiant</h3>
                    <div class="twelve columns tabContent">
                        <asp:TextBox ID="nom" runat="server" placeholder="Nom" CssClass="twelve columns" />
                        <asp:TextBox ID="prenom" runat="server" placeholder="Prenom" CssClass="twelve columns" />
                        <asp:TextBox ID="ni" runat="server" placeholder="Ni" CssClass="twelve columns" />
                        <asp:TextBox ID="email" required="required" runat="server" CssClass="twelve columns" placeholder="E-mail" />
                        <asp:TextBox ID="adresse" runat="server" CssClass="twelve columns" placeholder="Adresse" />
                        <asp:TextBox ID="phone_jour" runat="server" CssClass="twelve columns" placeholder="Téléphone jour" />
                        <asp:TextBox ID="phone_nuit" runat="server" CssClass="twelve columns" placeholder="Téléphone nuit" />
                    </div> <!-- tab content 1 -->
<!-- *********************************** choix des exams et des villes **************************************** -->
                    <h3> Concernant l'examen supervisé </h3>
                    <div class="twelve columns tabContent">
                     <p> Choisir le ou les examens possibles </p>
                     <p>Dérouler liste des cours offerts:</p>
                     <asp:DropDownList ID="liste_cours" runat="server" CssClass="twelve columns cours_offerts" />
                     <br />
                     <p class="twelve columns"> Je désire composer mon examen mi-semestre(intra) au:</p>

                    <!-- inside of nb -->
                     <p>
                         Intérieur du N.-B.:&nbsp;
                         <asp:DropDownList ID="ville_intra" runat="server" CssClass="offset-by-one five columns button_InNB" />
                     </p>
                    <!-- out of nb -->
                     <p>
                         A l'exterieur du N.-B.:&nbsp;
                         <asp:DropDownList ID="ville_final" runat="server" CssClass="five columns button_OutNB" />  
                     </p>

                    <!--2 bouton checkliste-->
                     <p class="twelve columns"> Je désire composer mon examen final au:</p>

                    <!-- inside of nb -->
                     <p>
                         Intérieur du N.-B.&nbsp;
                         <asp:DropDownList ID="ville_intra1" runat="server" CssClass="offset-by-one five columns button_InNB" />
                     </p>
                     <br />
                    <!-- out of nb -->
                    <asp:DropDownList ID="ville_final1" runat="server" CssClass="five columns button_OutNB" />
                      <!-- Choix de ville pour les intra -->


                      <!-- Choix de ville fini -->
                    </div><!-- end tab content 2 -->

 <!-- *********************************** coordonnées du superviseur **************************************** -->

                     <h3 class=""> Coordonn&eacutes du superviseur</h3>
                     <div class="twelve columns tabContent">
                     <p class="eight columns"> Coordonn&eacute;es du superviseur </p>
                        <div class="twelve columns chooseOption_Sup">
                            <a class="twelve columns getExistingSup" href="#">
                                <p class="twelve columns">Composer avec un ancien superviseur</p>
                            </a>
                            <a class="twelve columns getNewSup" href="#">
                                <p class="twelve columns">Proposer un nouveau superviseur </p>
                            </a>
                        </div> <!-- choose option sup -->
                            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#yesNewSuepervisor" aria-expanded="false" aria-controls="yesNewSupervisor">
  yes i want
                            </button> 
                        <div class="twelve columns yesNewSupervisor collapse" id="yesNewSupervisor"> <!-- shows up if student wants new sup -->
                            <asp:TextBox ID="sup_nom" runat="server" CssClass="twelve columns" placeholder="Nom" />
                            <asp:TextBox ID="sup_prenom" runat="server" CssClass="twelve columns" placeholder="Prénom" />
                            <asp:TextBox ID="sup_titre" runat="server" CssClass="twelve columns" placeholder="Titre du Poste" />
                            <asp:TextBox ID="sup_adrs" runat="server" CssClass="twelve columns" placeholder="Adresse civique" />
                            <asp:TextBox ID="sup_ville" runat="server" CssClass="twelve columns" placeholder="Ville" />
                            <asp:TextBox ID="sup_pays" runat="server" CssClass="twelve columns" placeholder="Pays" />
                            <asp:TextBox ID="sup_email" runat="server" CssClass="twelve columns" placeholder="E-mail" />
                            <asp:TextBox ID="sup_telJ" runat="server" CssClass="twelve columns" placeholder="Téléphone jour" />
                            <asp:TextBox ID="sup_telN" runat="server" CssClass="twelve columns" placeholder="Téléphone nuit" />
                         <span class="twelve columns"> Communiquer avec le superviseur en ?</span>
                            <asp:CheckBoxList ID="sup_lang" runat="server">
                                <asp:ListItem Value="francais" Text="Français" />
                                <asp:ListItem Value="anglais" Text="Anglais" />
                            </asp:CheckBoxList>

                        </div> <!-- yes new supervisor -->
                    </div> <!-- end tab content 3 -->
                    </div> <!-- close accordion -->
                    <input class="eight columns" name="" type="submit" />
                </form> <!-- global student and supervisor info -->
                 
              </div><!-- container -->
            </div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
            <footer>
               <div class="container">
                <div class="three columns logo">
                        <a href="#">
                            <img src="#" />
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="two columns copyright">
                    <p class="">
                        <a href="#"> copyright 2015 - tous droits reserved  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div> <!-- wrapper -->
    </body>
</html>
