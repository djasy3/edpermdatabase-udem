﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class VueCentresExamens : System.Web.UI.Page
{
    CurrentSession sessionCourante = SessionAccess.getCurrentSession();

    public int Nrc 
    {
        get { return int.Parse(lstCours.SelectedValue); }
    }
    //
    public int SId
    {
        get { return int.Parse(lstSession.SelectedValue); }
    }
    //
    public string TypeExam
    {
        get { return typeExamen.SelectedValue; }
    }
    //
    public static int IdVille { get; set; }//pour avoir le id de la ville 
    //
    public static string NomVille { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            sessionAct();
            //
            listeCours();
            //
            listeCentreExams();
            //
            centreExams();
        }
        //
        erreur.Visible = false;
    }
    //
    protected void sessionAct()
    {
        lstSession.DataSource = SessionAccess.getSessions();
        lstSession.DataTextField = "s_nom";
        lstSession.DataValueField = "s_id";
        lstSession.DataBind();
        //
        lstSession.SelectedValue = sessionCourante.Id.ToString();
    }
    //
    protected void listeCours()
    {
        lstCours.DataSource = SessionAccess.getListeCours();
        lstCours.DataTextField = "c_sigle";
        lstCours.DataValueField = "c_id";
        lstCours.DataBind();
        //
        ListItem item = new ListItem();
        item = new ListItem();
        item.Text = "<Tous les cours>";
        item.Value = "0";
        //lstCours.Items.Add(item);
        lstCours.Items.Insert(0, item);
    }
    //
    protected void listeCentreExams()
    {
        lstCentreExams.DataSource = SessionAccess.lstVilleRsrve();
        lstCentreExams.DataBind();
    }
    //
    protected void centreExams()
    {
        if(Nrc != 0)
            affichageCours.DataSource = SessionAccess.getConfirmationT(SId, TypeExam, Nrc, IdVille);
        else
            affichageCours.DataSource = SessionAccess.getConfirmationS(SId, TypeExam, IdVille);

        affichageCours.DataBind();
    }
    protected void lstSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        centreExams();
    }
    protected void lstCours_SelectedIndexChanged(object sender, EventArgs e)
    {
        centreExams();
    }
    protected void typeExamen_SelectedIndexChanged(object sender, EventArgs e)
    {
        centreExams();
    }
    protected void villeCentreExam_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        IdVille = int.Parse(btn.CommandArgument);
        NomVille = btn.Text;
        //on revient à la page !
        centreExams();
    }
    protected void cpElctrk_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox cpElctrk = (CheckBox)sender;
            ListViewItem item = (ListViewItem)cpElctrk.NamingContainer;

            Label m_sigle = (Label)affichageCours.Items[item.DataItemIndex].FindControl("nomCours");
            //
            int nrc = ExamAccess.getNrc(m_sigle.Text);
            //
            SessionAccess.cnfrmCpscan(nrc, SId, TypeExam, NomVille, cpElctrk.Checked);
            //
            erreur.Text = "confirmé(copie scanée) pour : " + NomVille + "Pour le cours de " + m_sigle.Text;
            erreur.ForeColor = System.Drawing.Color.Green;
            erreur.Visible = true;
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.Visible = true;
        }
    }
    protected void cpPapier_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox cpPapier = (CheckBox)sender;
            ListViewItem item = (ListViewItem)cpPapier.NamingContainer;

            Label m_sigle = (Label)affichageCours.Items[item.DataItemIndex].FindControl("nomCours");
            //
            int nrc = ExamAccess.getNrc(m_sigle.Text);
            //
            SessionAccess.cnfrmCpPapier(nrc, SId, TypeExam, NomVille, cpPapier.Checked);
            //
            erreur.Text = "confirmé(copie papier) pour : " + NomVille + "Pour le cours de " + m_sigle.Text;
            erreur.ForeColor = System.Drawing.Color.Green;
            erreur.Visible = true;
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.Visible = true;
        }
    }
    protected void trousseSent_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox trousseSent = (CheckBox)sender;
            ListViewItem item = (ListViewItem)trousseSent.NamingContainer;

            Label m_sigle = (Label)affichageCours.Items[item.DataItemIndex].FindControl("nomCours");
            //
            int nrc = ExamAccess.getNrc(m_sigle.Text);
            //
            SessionAccess.trousseSent(nrc, SId, TypeExam, NomVille, trousseSent.Checked);
            //
            erreur.Text = "Trousse envoyé pour : " + NomVille + "Pour le cours de " + m_sigle.Text;
            erreur.ForeColor = System.Drawing.Color.Green;
            erreur.Visible = true;
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.Visible = true;
        }
    }
    protected void lieuConfrm_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox lieuConfrm = (CheckBox)sender;
            ListViewItem item = (ListViewItem)lieuConfrm.NamingContainer;

            Label m_sigle = (Label)affichageCours.Items[item.DataItemIndex].FindControl("nomCours");
            //
            int nrc = ExamAccess.getNrc(m_sigle.Text);
            //
            SessionAccess.cnfrmLieu(nrc, SId, TypeExam, NomVille, lieuConfrm.Checked);
            //
            erreur.Text = "Lieu confirmé pour : " + NomVille + "Pour le cours de " + m_sigle.Text;
            erreur.ForeColor = System.Drawing.Color.Green;
            erreur.Visible = true;
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.Visible = true;
        }
    }
    protected void notes_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox notes = (TextBox)sender;
            ListViewItem item = (ListViewItem)notes.NamingContainer;

            Label m_sigle = (Label)affichageCours.Items[item.DataItemIndex].FindControl("nomCours");
            //
            int nrc = ExamAccess.getNrc(m_sigle.Text);
            //
            SessionAccess.setNote(nrc, SId, TypeExam, NomVille, notes.Text);
            //
            erreur.Text = "Bien noté : " + NomVille + "Pour le cours de " + m_sigle.Text;
            erreur.ForeColor = System.Drawing.Color.Green;
            erreur.Visible = true;
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.Visible = true;
        }
    }
}