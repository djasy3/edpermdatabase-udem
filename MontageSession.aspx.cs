﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;


public partial class MontageSession : System.Web.UI.Page
{
    CurrentSession sess = SessionAccess.getCurrentSession();

    //propriété pour donner le nom et l'id de la session
    public string Nom
    {
        get { return Server.HtmlEncode(nom_session.Text); }
    }
    //
    public int Id
    {
        get { return int.Parse(Server.HtmlEncode(annee.Text)); }
    }
    //
    public static int Nrc { get; set; }
    //
    public static int IdMsg { get; set; }//pour avoir la clé primaire du message !
    //
    public string Sigle
    {
        get {
            if (!string.IsNullOrEmpty(sigleCours.Text) || !string.IsNullOrWhiteSpace(sigleCours.Text))
                return Server.HtmlEncode(sigleCours.Text);
            else
                throw new Exception("Le champs du sigle du cours ne doit pas être vide.");
        }
        set { sigleCours.Text = Server.HtmlDecode(value); }
    }
    //
    public string TitreDuCours
    {
        get { return Server.HtmlEncode(nomCours.Text); }
        set { nomCours.Text = Server.HtmlDecode(value); }
    }
    //
    public DateTime DateIntra
    {
        get { return DateTime.Parse(dateIntra.Text); }
        set { dateIntra.Text = String.Format("{0:yyyy-MM-dd  hh:mm:ss}",value); }
    }
    //
    public DateTime DateFinal
    {
        get { return DateTime.Parse(dateFinal.Text); }
        set { dateFinal.Text = String.Format("{0:yyyy-MM-dd  hh:mm:ss}", value); }
    }
    //méthodes concernant l'enregistrement des villes
    public string Adresse
    {
        get { return Server.HtmlEncode(m_adres.Text); }
        set { m_adres.Text = Server.HtmlDecode(value); }
    }
    public string Ville
    {
        get {
            if (!string.IsNullOrEmpty(m_ville.Text) || !string.IsNullOrWhiteSpace(m_ville.Text))
                return Server.HtmlEncode(m_ville.Text);
            else
                throw new Exception("le champs de la ville ne doit pas être vide");
        }
        set { m_ville.Text = Server.HtmlDecode(value); }
    }
    public string Etat
    {
        get
        {
            if (!string.IsNullOrEmpty(m_etat.Text) || !string.IsNullOrWhiteSpace(m_etat.Text))
                return Server.HtmlEncode(m_etat.Text);
            else
                throw new Exception("le champs de la province/etat ne doit pas être vide");
        }
        set { m_etat.Text = Server.HtmlDecode(value); }
    }
    public string Pays
    {
        get { return m_pays.Text; }
        set { m_pays.Text = Server.HtmlDecode(value); }
    }
    //
    public string ObjetMessage
    {
        get {
            if (!string.IsNullOrEmpty(objetMessage.Text) || !string.IsNullOrWhiteSpace(objetMessage.Text))
                return Server.HtmlEncode(objetMessage.Text);
            else
                throw new Exception("le champs de l'objet du message ne doit pas être vide");
        }
        set { objetMessage.Text = Server.HtmlDecode(value); }
    }
    //
    public string CorpsMessage
    {
        get { return Server.HtmlEncode(corpsMessage.Text); }
        set { corpsMessage.Text = Server.HtmlDecode(value); }
    }
    //
    public static int IdVille { set; get; }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            
            if (!IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];
                listeCoursDpDlst();
                //
                listeSessions();
                //
                listeCoursLstV();//méthode qui appelle la deuxième liste des cours liée à la  liste view
                //
                lstMessages();

                listeCentreExams();

                erreur.Visible = false;
                alertSession.Visible = false;
                alertCours.Visible = false;
                alertStudent.Visible = false;
                alertVilleExam.Visible = false;
                alertMsg.Visible = false;
                //afficherDetailsCours();
                btnUpdtCours.Visible = false;
                btnAddCours.Visible = true;
                btnDelCours.Visible = false;
                //concernant les  messages
                btnSent.Visible = true;
                btnUpdtMsg.Visible = false;
                btnClear.Visible = false;
                btnDltMsg.Visible = false;
                //concernant les villes
                addnewtown.Visible = true;
                updtTown.Visible = false;
                delTown.Visible = false;
                clrTown.Visible = false;
//                create procedure coursInscrit(in nrc int(4))
//select c_id from t_inscription where c_id = nrc

//create procedure delCours(in nrc int(4))
//delete from t_cours where c_id = nrc
            }
            TabName.Value = Request.Form[TabName.UniqueID];
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
    }
    //méthode pour renvoyer différente session
    public void listeSessions()
    {
        lstSession.DataSource = SessionAccess.getSessions();
        lstSession.DataTextField = "s_nom";
        lstSession.DataValueField = "s_id";
        lstSession.DataBind();
        lstSession.SelectedValue = sess.Id.ToString();
    }
    //méthode qui renvoi les cours par la dropdownliste
    public void listeCoursDpDlst()
    {
        lstCours.DataSource = SessionAccess.getListeCours();
        lstCours.DataTextField = "c_sigle";
        lstCours.DataValueField = "c_id";
        lstCours.DataBind();

        ListItem item = new ListItem();
        item = new ListItem();
        item.Text = "<Choisissir un cours>";
        item.Value = "0";
        //lstCours.Items.Add(item);
        lstCours.Items.Insert(0, item);
    }
    //méthode qui renvoi le cours par la liste vieux
    public void listeCoursLstV()
    {
        lstCours2.DataSource = SessionAccess.getListeCours();
        lstCours2.DataBind();
    }
    protected void btnSession_Click(object sender, EventArgs e)
    {
        //fonction pour ajouter les infos dans la session
        try
        {
            //commencons par vérifier si la session n'existe ou pas !
            if (SessionAccess.setSession(Nom, Id))
            {
                alertSession.Text = "Session enregistrée avec succès";
                alertSession.ForeColor = System.Drawing.Color.Green;
                alertSession.Visible = true;
                listeSessions();
            }
            else
                throw new Exception("Une erreur est survenue, vérifiez que vous avez bien entré les données");
        }
        catch (Exception ex)
        {
            alertSession.Text = ex.Message;
            alertSession.ForeColor = System.Drawing.Color.Red;
            alertSession.Visible = true;
        }
    }
    //
    protected void btnAddCours_Click(object sender, EventArgs e)
    {
        try
        {
            if (DateIntra > DateFinal)
                throw new Exception("la date de l'examen intra doit venir avant celle de l'examen final");

            if (SessionAccess.setCours(Sigle, TitreDuCours, DateIntra, DateFinal))
            {
                cleanCours();
                alertCours.Text = "Cours enregistré avec succès !";
                alertCours.ForeColor = System.Drawing.Color.Green;
                alertCours.Visible = true;
                listeCoursLstV();
                listeCoursDpDlst();

            }
            else
                throw new Exception("Enregistrement echoué !");
        }
        catch (Exception ex)
        {
            alertCours.Text = ex.Message;
            alertCours.ForeColor = System.Drawing.Color.Red;
            alertCours.Visible = true;
        }
    }
    protected void afficherDetailsCours()
    {
        DetailsCours detailsCours = SessionAccess.getDetailsCours(Nrc);
        if (detailsCours.nrc > 0)
        {
            Sigle = detailsCours.sigle;
            TitreDuCours = detailsCours.nom;
            DateIntra = detailsCours.dateIntra;
            DateFinal = detailsCours.dateFinal;
            //
        }
    }
    protected void btnUpdtCours_Click(object sender, EventArgs e)
    {
        try
        {
            if (DateIntra > DateFinal)
                throw new Exception("la date de l'examen intra doit venir avant celle de l'examen final");

            if (SessionAccess.updtCours(Nrc, Sigle, TitreDuCours, DateIntra, DateFinal))
            {
                cleanCours();
                alertCours.Text = "Cours modifié avec succès !";
                alertCours.ForeColor = System.Drawing.Color.Green;
                alertCours.Visible = true;
                listeCoursLstV();
            }
            else
                throw new Exception("Echec de modification !");
        }
        catch (Exception ex)
        {
            alertCours.Text = ex.Message;
            alertCours.ForeColor = System.Drawing.Color.Red;
            alertCours.Visible = true;
        }
    }
    //méthode pour effacer les cours
    protected void btnDelCours_Click(object sender, EventArgs e)
    {
        try
        {
            if (!SessionAccess.coursInscrit(Nrc))
            {
                if (SessionAccess.delCours(Nrc))
                {
                    cleanCours();
                    alertCours.Text = "Cours supprimé avec succès !";
                    alertCours.ForeColor = System.Drawing.Color.Green;
                    alertCours.Visible = true;
                    listeCoursLstV();
                    listeCoursDpDlst();
                }
            }
            else
                throw new Exception("Certains étudiant(e)s sont inscrit(e)s au cours que vous voulez supprimer !");
        }
        catch (Exception ex)
        {
            alertCours.Text = ex.Message;
            alertCours.ForeColor = System.Drawing.Color.Red;
            alertCours.Visible = true;
        }
    }
    protected void btnAddStudents_Click(object sender, EventArgs e)
    {
        try
        {
            if (importListEtudiant.HasFile)
            {
                //on récupère le nom du fichier excel à importer
                string fileName = Path.GetFileName(importListEtudiant.PostedFile.FileName);
                //l'extension du fichier
                string extension = Path.GetExtension(importListEtudiant.PostedFile.FileName);
                //le chemin du dossier qui contient le fichier
                string folderName = ConfigurationManager.AppSettings["FolderPath"];
                //le chemin du fichier
                string filePath = Server.MapPath(folderName + fileName);
                importListEtudiant.SaveAs(filePath);//on sauvegarde le fichier uploadé
                //on fait appel à la méthode qui permet de copier les données dans un dataset !
                DataTable dt;
                //if (header.Checked)
                //    dt = ExcelAccess.ImportToDataTable(filePath, extension, "Oui");
                //else
                dt = ExcelAccess.ImportToDataTable(filePath, extension, "Non");
                //
                if (ExcelAccess.ImportEtudiant(dt, int.Parse(lstCours.SelectedValue), int.Parse(lstSession.SelectedValue)))
                {
                    alertStudent.Text = "Importation reussie";
                    alertStudent.ForeColor = System.Drawing.Color.Green;
                    alertStudent.Visible = true;
                }
                else
                    throw new Exception("L'importation n'a pas réussi!");
            }
            else
                throw new Exception("Veuillez chosir un fichier");
        }
        catch (Exception ex)
        {
            alertStudent.Text = ex.Message;
            alertStudent.ForeColor = System.Drawing.Color.Red;
            alertStudent.Visible = true;
        }
    }
    protected void detailsCours_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        Nrc = int.Parse(btn.CommandArgument);
        //on revient à la page !
        cleanCours();
        afficherDetailsCours();
        //
        btnUpdtCours.Visible = true;
        btnAddCours.Visible = false;
        btnDelCours.Visible = true;
    }
    protected void btnClean_Click(object sender, EventArgs e)
    {
        cleanCours();
    }
    //méthode pour cleaner les cours
    protected void cleanCours()
    {
        Sigle = "";
        TitreDuCours = "";
        DateIntra = DateTime.Now;
        DateFinal = DateTime.Now;
        alertCours.Text = "";
        //
        btnUpdtCours.Visible = false;
        btnAddCours.Visible = true;
        btnDelCours.Visible = false;
    }
    //
    protected void importerCours_Click(object sender, EventArgs e)
    {
        try
        {
            if (importFichier.HasFile)
            {
                //on récupère le nom du fichier excel à importer
                string fileName = Path.GetFileName(importFichier.PostedFile.FileName);
                //l'extension du fichier
                string extension = Path.GetExtension(importFichier.PostedFile.FileName);
                //le chemin du dossier qui contient le fichier
                string folderName = ConfigurationManager.AppSettings["FolderPath"];
                //le chemin du fichier
                string filePath = Server.MapPath(folderName + fileName);
                importFichier.SaveAs(filePath);//on sauvegarde le fichier uploadé
                //on fait appel à la méthode qui permet de copier les données dans un dataset !
                DataTable dt;
                //if (header.Checked)
                //    dt = ExcelAccess.ImportToDataTable(filePath, extension, "Oui");
                //else
                dt = ExcelAccess.ImportToDataTable(filePath, extension, "Non");
                //
                if (ExcelAccess.ImportCours(dt))
                {
                    alertCours.Text = "Importation reussie";
                    alertCours.ForeColor = System.Drawing.Color.Green;
                    alertCours.Visible = true;
                }
                else
                    throw new Exception("L'importation n'a pas réussi!");
            }
            else
                throw new Exception("Vous devez choisir un fichier");
        }
        catch (Exception ex)
        {
            alertCours.Text = ex.Message;
            alertCours.ForeColor = System.Drawing.Color.Red;
            alertCours.Visible = true;
        }
    }
    //méthode pour villes
    protected void addnewtown_Click(object sender, EventArgs e)
    {
        try
        {
            if (!ExamAccess.lieuExiste(Ville, Etat, Pays))
            {
                ExamAccess.setLieuxExamens(Adresse, Ville, Etat, Pays);
                listeCentreExams();
                alertVilleExam.Text = "La ville à été ajouté avec succes";
                alertVilleExam.ForeColor = System.Drawing.Color.Green;
                alertVilleExam.Visible = true;
            }
            else
                throw new Exception("La ville que vous essayez d'enregistrer existe déjà !");
        }
        catch (Exception ex)
        {
            alertVilleExam.Text = ex.Message;
            alertVilleExam.ForeColor = System.Drawing.Color.Red;
            alertVilleExam.Visible = true;
        }
    }
    //
    protected void listeCentreExams()
    {
        lstVilleDispo.DataSource = SessionAccess.lstVilleRsrve();
        lstVilleDispo.DataBind();
    }

    //méthode qui renvoi la liste des objets de message
    protected void lstMessages()
    {
        lstMsgs.DataSource = SessionAccess.getLstMsg();
        lstMsgs.DataBind();
    }
    //méthode pour afficher les messages
    protected void afficherMessages()
    {
        MessagePreEnregistre msg = SessionAccess.getLstMsg(IdMsg);
        ObjetMessage = msg.objet;
        CorpsMessage = msg.corps;
    }
    protected void btnSent_Click(object sender, EventArgs e)
    {
        //méthode pour enregistrer les messages !
        try
        {
            if (SessionAccess.setMsg(ObjetMessage, CorpsMessage))
            {
                alertMsg.Text = "Le message à été enregistré avec succès";
                alertMsg.ForeColor = System.Drawing.Color.Green;
                alertMsg.Visible = true;
                cleanMessage();
            }
            else
                throw new Exception("votre message n'a pas pu être envoyé");
        }
        catch (Exception ex)
        {
            alertMsg.Text = ex.Message;
            alertMsg.ForeColor = System.Drawing.Color.Red;
            alertMsg.Visible = true;
        }

    }
    protected void btnUpdtMsg_Click(object sender, EventArgs e)
    {
        //méthode pour mettre à jour les msgs
        //SessionAccess.updtMsg(int id, string obj, string corps);
        try
        {
            if (SessionAccess.updtMsg(IdMsg, ObjetMessage, CorpsMessage))
            {
                alertMsg.Text = "Le message à été modifié avec succès";
                alertMsg.ForeColor = System.Drawing.Color.Green;
                alertMsg.Visible = true;
            }
            else
                throw new Exception("votre message n'a pas pu être modifié");
        }
        catch (Exception ex)
        {
            alertMsg.Text = ex.Message;
            alertMsg.ForeColor = System.Drawing.Color.Red;
            alertMsg.Visible = true;
        }
        
    }
    protected void btnDltMsg_Click(object sender, EventArgs e)
    {
        //on efface le message qu'on ne veux plus !
        try
        {
            if (SessionAccess.dltMsg(IdMsg))
            {
                alertMsg.Text = "Le message à été effacé avec succès";
                alertMsg.ForeColor = System.Drawing.Color.Green;
                alertMsg.Visible = true;
                cleanMessage();
            }
            else
                throw new Exception("votre message n'a pas pu être effacé");
        }
        catch (Exception ex)
        {
            alertMsg.Text = ex.Message;
            alertMsg.ForeColor = System.Drawing.Color.Red;
            alertMsg.Visible = true;
        }
    }
    //méthode pour clear les champs de messagerie
    protected void cleanMessage()
    {
        corpsMessage.Text = "";
        objetMessage.Text = "";

        btnSent.Visible = true;
        btnUpdtMsg.Visible = false;
        btnClear.Visible = false;
        btnDltMsg.Visible = false;
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        cleanMessage();
    }
    protected void lnkbtnMsgs_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        IdMsg = int.Parse(btn.CommandArgument);
        //on fait appel à la méthode pour afficher les différents messages
        afficherMessages();
        //
        btnSent.Visible = false;
        btnUpdtMsg.Visible = true;
        btnClear.Visible = true;
        btnDltMsg.Visible = true;
    }
    protected void detailsVilles_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        IdVille = int.Parse(btn.CommandArgument);
        //on fait appel à la méthode qui affiche les infos sur les villes
        afficherVille();
        //ensuite on affiche les bouttons concernés
        addnewtown.Visible = false;
        updtTown.Visible = true;
        delTown.Visible = true;
        clrTown.Visible = true;
    }
    //méthode pour afficher les villes
    protected void afficherVille()
    {
        DetailsVille dv = SessionAccess.lstVille(IdVille);
        Adresse = dv.adresse;
        Etat = dv.etat;
        Ville = dv.ville;
        Pays = dv.pays;
    }
    protected void updtTown_Click(object sender, EventArgs e)
    {
        //méthode pour mettre à jour
        try
        {
            //méthode pour mettre à jour !
            SessionAccess.updtVille(IdVille, Adresse, Ville, Etat, Pays);
            alertVilleExam.Text = "Ville mise à jour effectuée avec success !";
            alertVilleExam.ForeColor = System.Drawing.Color.Red;
            alertVilleExam.Visible = true;
            listeCentreExams();
        }
        catch (Exception ex)
        {
            alertVilleExam.Text = ex.Message;
            alertVilleExam.ForeColor = System.Drawing.Color.Red;
            alertVilleExam.Visible = true;
        }
    }
    protected void delTown_Click(object sender, EventArgs e)
    {
        try
        {
            //méthode pour effacer
            SessionAccess.delVille(IdVille);
            clearTown();
            alertVilleExam.Text = "Ville effacée avec success !";
            alertVilleExam.ForeColor = System.Drawing.Color.Red;
            alertVilleExam.Visible = true;
            listeCentreExams();
        }
        catch (Exception ex)
        {
            alertVilleExam.Text = ex.Message;
            alertVilleExam.ForeColor = System.Drawing.Color.Red;
            alertVilleExam.Visible = true;
        }
    }
    protected void clrTown_Click(object sender, EventArgs e)
    {
        clearTown();
    }
    //
    protected void clearTown()
    {
        Adresse = "";
        Ville = "";
        Etat = "";
        Pays = "";
        //
        alertVilleExam.Text = "";
        alertVilleExam.Visible = false;
        addnewtown.Visible = true;
        updtTown.Visible = false;
        delTown.Visible = false;
        clrTown.Visible = false;
    }
}