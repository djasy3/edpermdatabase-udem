﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="InscriptionEtudiant" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Bienvenue au centre de gestion des reservations de l'edperm</title>
    <link rel="stylesheet" type="text/css" href="./App_Themes/css/skeleton.css" media="all" />
    <link rel="stylesheet" type="text/css" href="./App_Themes/css/login.css" media="all" />
    <script type="text/javascript" src="App_Themes/js/jquery.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="wrapper">
       <div class="container">
             f<!-- brand slogan starts -->
            <div class="offset-by-three ten columns brand_slogan">
                <p class="twelve columns brand">
                    &Eacute;ducation permanente
                </p> <!-- brand or institution name -->
                <p class="offset-by-three ten columns slogan">
                    L'universit&eacute; en tout temps.
                </p> <!-- slogan -->
            </div> <!-- brand_slogan -->
             <!-- authentication starts -->
            <div class="offset-by-four four columns authentication">
            <div class="login_form">
            <asp:LoginView ID="cnx" runat="server" >
                <AnonymousTemplate>
                    <asp:Login ID="login" runat="server" DestinationPageUrl="DBoard.aspx" VisibleWhenLoggedIn="false">
                <LayoutTemplate>
                <asp:TextBox ID="UserName" runat="server" placeholder="identifiant*" CssClass="twelve columns username" />
                <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="twelve columns password" placeholder="mot de passe*" />
                <asp:RequiredFieldValidator CssClass="twelve columns checkUserName" ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="Un nom d'utilisateur est requis." ToolTip="Un nom d'utilisateur est requis." ValidationGroup="login"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator CssClass="twelve columns checkUserPass" ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Un mot de passe est requis." ToolTip="Un mot de passe est requis." ValidationGroup="login"></asp:RequiredFieldValidator>
                <asp:CheckBox ID="RememberMe" runat="server" Text="Mémoriser le mot de passe." CssClass="twelve columns rememberCheck"/>
                <asp:Label CssClass="twelve columns connexionFailed" ID="FailureText" runat="server" EnableViewState="False"></asp:Label>
            <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Se connecter" ValidationGroup="login" CssClass="four columns submit" />
                </LayoutTemplate>
            </asp:Login> 
                </AnonymousTemplate>
            </asp:LoginView>
            </div> 
            </div> <!-- authentication -->
             <!-- error starts -->
            <div class="four columns errors">
                <a class="twelve columns" href="PwdRecovery.aspx"> mot de passe oubli&eacute; ?</a>
            </div> <!-- errors -->
             <!-- footer starts -->
               <div class="twelve columns footer">
                <hr />
                <a class="one column" href="http://www.umoncton.ca"><img src="./images/logo_udem.png" /></a>
                <p class="five columns"> &copy; 2015, Universit&eacute; de Moncton.
                 Tous droits r&eacute;serv&eacute;s</p>
            </div> <!-- footer -->
            
         </div> <!-- container -->
     </div> <!-- end wrapper -->
    </form>
</body>
</html>
