﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DBoard : System.Web.UI.Page
{
    CurrentSession sess = SessionAccess.getCurrentSession();
    public string Sid
    {
        get { return listOfSessions.SelectedValue; }
    }
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lstSessions();

            afficherInfos();
        }
    }
    //méthode pour charger les sessions
    protected void lstSessions()
    {
        listOfSessions.DataSource = SessionAccess.getSessions();
        listOfSessions.DataTextField = "s_nom";
        listOfSessions.DataValueField = "s_id";
        listOfSessions.DataBind();
        listOfSessions.SelectedValue = sess.Id.ToString();
    }
    //méthode pour charger les info
    protected void afficherInfos()
    {
        //fonction pour afficher le nombre des cours présents
        nbreEtudInscrits.Text = SessionAccess.nbreEtudiant(int.Parse(Sid)).ToString();
        //afficher les cours dispo par sessions aux quels les étudiants sont inscrits
        nbreCours.Text = SessionAccess.nbreCours(int.Parse(Sid)).ToString();
        //afficher le nombre des centres d'examens
        centres.Text = SessionAccess.nbreCentreExams(int.Parse(Sid)).ToString() ;
    }
    //
    protected void listOfSessions_SelectedIndexChanged(object sender, EventArgs e)
    {
        afficherInfos();
    }
}