﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class VueDetailCours : System.Web.UI.Page
{
    CurrentSession sessionCourante = SessionAccess.getCurrentSession();

    public static int Nrc{ get; set; }

    public int SId
    {
        get { return int.Parse(lstSession.SelectedValue); }
        set { lstSession.SelectedValue = value.ToString(); }
    }
    //
    public string TypeExam
    {
        get { return typeExam.SelectedValue; }
    }
    //
    public string Confirmer
    {
        get { return students.SelectedValue; }
    }
    //
    public string Etat
    {
        get { return lieuExam.SelectedValue; }
    }
    public string Ack { get; set; }
    public static int IdMsg { get; set; }
    public static string CourielEtudiant { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        //string lien;
        try
        {
            if (PreviousPage != null)//on prend le nrc de la page si l'on vient de la page vuecours !
            {
                VueCours prevPage = PreviousPage as VueCours;
                if (prevPage != null)
                {
                    Nrc = prevPage.Nrc;
                    SId = prevPage.SessionId;
                }
            }
            //else
                //SId = sessionCourante.Id;
            //
            if (!this.IsPostBack)
            {
                lstSess();

                listeCours();

                //dExams();

                afficherDetails();

                erreur.Visible = false;

                lstEtudiantTous.Visible = false;
                //
            }
            //afficherDetails();
            RegisterAsyncPostBackControl();
            
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message + ex.StackTrace;
            erreur.ForeColor = System.Drawing.Color.Red;
            erreur.Visible = true;
        }
    }
    //
    public void lstSess()
    {
        lstSession.DataSource = SessionAccess.getSessions();
        lstSession.DataTextField = "s_nom";
        lstSession.DataValueField = "s_id";
        lstSession.DataBind();
        lstSession.SelectedValue = SId.ToString();
    }
    //
    public void listeCours()
    {
        lstCours.DataSource = SessionAccess.getListeCours();
        lstCours.DataBind();
    }
    //
    protected void detailsCours_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        Nrc = int.Parse(btn.CommandArgument);
        ListViewItem item = (ListViewItem)btn.NamingContainer;
        HtmlGenericControl li = (HtmlGenericControl)lstCours.Items[item.DataItemIndex].FindControl("lcours");
        //li.Attributes.Add("class", "five columns");
        //li.Attributes.Add("class", "is_active");
  
        //on revient à la page !
        afficherDetails();
    }
    //méthode pour prendre les post back control
    private void RegisterAsyncPostBackControl()
    {
        foreach (ListViewItem item in lstCours.Items)
        {
            LinkButton lien = item.FindControl("detailsCours") as LinkButton;
            ajax.RegisterAsyncPostBackControl(lien);
        }
    }
    //méthode pour loader les informations
    protected void afficherDetails()
    {

        try
        {
            if (Confirmer == "conf")
            {
                //on affiche la liste des étudiants ayant déjà confirmé
                typeExam.Enabled = true;
                lieuExam.Enabled = true;
                lstEtudiantTous.Visible = false;
                lstReservation.Visible = true;

                if ((Etat == "all") && (TypeExam == "both"))
                {
                    lstReservation.DataSource = ExamAccess.getReservations(Nrc, SId);
                    lstReservation.DataBind();
                }
                else if ((Etat != "all") && (TypeExam == "both"))
                {
                    lstReservation.DataSource = ExamAccess.getReservations(Nrc, SId, Etat);
                    lstReservation.DataBind();
                }
                else if ((Etat == "all") && (TypeExam != "both"))
                {
                    lstReservation.DataSource = ExamAccess.getReservationsType(Nrc, SId, TypeExam);
                    lstReservation.DataBind();
                }
                else if ((Etat != "all") && (TypeExam != "both"))
                {
                    lstReservation.DataSource = ExamAccess.getReservations(Nrc, SId, Etat, TypeExam);
                    lstReservation.DataBind();
                }
            }
            else if (Confirmer == "tconf")
            {
                //on affiche une simple liste de tous les étudiant inscrit pour la session actuelle
                typeExam.Enabled = false;
                lieuExam.Enabled = false;
                lstReservation.Visible = false;
                //méthode appelant à un simple affichage
                lstEtudiantTous.DataSource = SessionAccess.listeEtudiantInscrit(SId, Nrc);
                lstEtudiantTous.DataBind();
                lstEtudiantTous.Visible = true;
            }
            else if (Confirmer == "nconf")
            {
                //ceux qui n'ont pas encore confirmé
                typeExam.Enabled = false;
                lieuExam.Enabled = false;
                lstEtudiantTous.Visible = false;
                //méthode appelant à un simple affichage
            }
        }
        catch (Exception ex)
        {
            erreur.Text = ex.Message;
            erreur.Visible = true;
        }

    }

    protected void lstSession_SelectedIndexChanged(object sender, EventArgs e)
    { 
        afficherDetails(); 
    }
    protected void lieuExam_SelectedIndexChanged(object sender, EventArgs e)
    {
        afficherDetails();
    }
    protected void typeExam_SelectedIndexChanged(object sender, EventArgs e)
    {
        afficherDetails();
    }
    protected void students_SelectedIndexChanged(object sender, EventArgs e)
    {
        afficherDetails();
    }
    protected void cd_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox notes = (TextBox)sender;
            ListViewItem item = (ListViewItem)notes.NamingContainer;
            Label m_couriel = (Label)lstReservation.Items[item.DataItemIndex].FindControl("couriel");
            Label m_typeExam = (Label)lstReservation.Items[item.DataItemIndex].FindControl("typeExam");

            string ni = SessionAccess.getEtudiantNi(m_couriel.Text);
            int cid = ExamAccess.getIdCentre(Nrc, ni, m_typeExam.Text, SId);
            //on note une note particulière chez l'étudiant
            SessionAccess.setNote(cid, notes.Text);
            //
            erreur.Text = "noté " + m_couriel.Text;
            erreur.Visible = true;
        }
        catch (Exception ex)
        {
            erreur.Text = "Erreur: " + ex.Message + ex.StackTrace;
            erreur.Visible = true;
        }
    }
    //
    protected void facture_TextChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox facture = (TextBox)sender;
            ListViewItem item = (ListViewItem)facture.NamingContainer;
            Label m_couriel = (Label)lstReservation.Items[item.DataItemIndex].FindControl("couriel");
            Label m_typeExam = (Label)lstReservation.Items[item.DataItemIndex].FindControl("typeExam");
            //obtenons le nrc
            string ni = SessionAccess.getEtudiantNi(m_couriel.Text);
            //on a le nrc !
            SessionAccess.detailsFacture(Nrc, SId, m_typeExam.Text, ni, facture.Text);
            erreur.ForeColor = System.Drawing.Color.Green;
            erreur.Visible = true;
        }
        catch (Exception ex)
        {
            erreur.Text = "Erreur: " + ex.Message;
            erreur.Visible = true;
        }
    }
    //méthode pour afficher la liste des tous les étudiants importés
    protected void lstEtudiantTous_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Label ack = (Label)e.Item.FindControl("ack");
            Label couriel = (Label)e.Item.FindControl("couriel");
            CourielEtudiant = couriel.Text;
            HtmlGenericControl li = (HtmlGenericControl)e.Item.FindControl("liste");
            //DropDownList chooseMsg = (DropDownList)e.Item.FindControl("chooseMsg");
            //chooseMsg.DataSource = SessionAccess.getLstMsg();
            //chooseMsg.DataTextField = "obj";
            //chooseMsg.DataValueField = "id";
            //chooseMsg.DataBind();

            //ListItem item = new ListItem();
            //item = new ListItem();
            //item.Text = "<Choisissir un message>";
            //item.Value = "0";
            ////lstCours.Items.Add(item);
            //chooseMsg.Items.Insert(0, item);

            //Button sentMsg = (Button)e.Item.FindControl("sentMsg");

            System.Data.DataRowView rowView = e.Item.DataItem as System.Data.DataRowView;
            bool confrm = bool.Parse(rowView["ack"].ToString());
            if (confrm)
            {
                ack.Text = "Oui";
                Ack = ack.Text;
                li.Style.Add("background-color", "#DFF2FF");
                //li.Attributes.Add("class", "confirmed");
                //chooseMsg.Visible = false;
                //sentMsg.Visible = false;
            }
            else
            {
                ack.Text = "Pas encore";
                Ack = ack.Text;
                li.Style.Add("background-color", "#FDE9E0");
                //li.Attributes.Add("class", "confirmed");
                //chooseMsg.Visible = true;
                //sentMsg.Visible = true;
            }
        }
    }
    protected void lstCours_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            HtmlGenericControl li = (HtmlGenericControl)e.Item.FindControl("lcours");
            li.Style.Clear();
        }
    }
    protected void delAll_Click(object sender, EventArgs e)
    {
        //méthode pour effacer toute la liste des étudiants d'un certain cours et le cours ensuite !
        try
        {
            CheckBox chkDellAll = (CheckBox)lstEtudiantTous.FindControl("checkDelAll");
            if (chkDellAll.Checked)
            {
                //on efface toute la liste, on vérife si il n'a pas encore confirmé son examen
                //ensuite le cours
                for (int i = 0; i < lstEtudiantTous.Items.Count; ++i)
                {
                    Label ack = (Label)lstEtudiantTous.Items[i].FindControl("ack");
                    Label couriel = (Label)lstEtudiantTous.Items[i].FindControl("couriel");
                    string ni = SessionAccess.getEtudiantNi(couriel.Text);
                    //si il a déjà confirmé, on commence par efface dans la table centreexamen, reservation ensuite inscription
                    if (ack.Text == "Oui")
                    {
                        //on get le id du centreexamen
                        int idCentreIntra = ExamAccess.getIdCentre(Nrc,ni , "intra", SId);
                        int idCentreFinal = ExamAccess.getIdCentre(Nrc, ni, "final", SId);
                        //reservations
                        SessionAccess.delStud(idCentreIntra, 1);
                        SessionAccess.delStud(idCentreFinal, 1);
                        //centreexamens
                        SessionAccess.delStud(idCentreIntra, 2);
                        SessionAccess.delStud(idCentreFinal, 2);
                        //
                        SessionAccess.delStud(Nrc, SId, ni);
                    }
                    else//si pas encore confirmé, on désinscrit carrément !
                    {
                        SessionAccess.delStud(Nrc,SId, ni);
                    }
                }
                erreur.Text = "La liste a été supprimée avec succès !";
                erreur.Visible = true;

                afficherDetails();
            }
            else
                throw new Exception("Veuillez cocher la case pour confirmer la suppression");

        }
        catch(Exception ex)
        {
            erreur.Text = "Erreur: " + ex.Message + ex.StackTrace;
            erreur.Visible = true;
        }
    }
    protected void del_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btn = (LinkButton)sender;
            ListViewItem item = (ListViewItem)btn.NamingContainer;
            Label couriel = (Label)lstEtudiantTous.Items[item.DataItemIndex].FindControl("couriel");
            Label ack = (Label)lstEtudiantTous.Items[item.DataItemIndex].FindControl("ack");
            string ni = SessionAccess.getEtudiantNi(couriel.Text);
            //
            if (ack.Text == "Oui")
            {
                //on get le id du centreexamen
                int idCentreIntra = ExamAccess.getIdCentre(Nrc, ni, "intra", SId);
                int idCentreFinal = ExamAccess.getIdCentre(Nrc, ni, "final", SId);
                //reservations
                SessionAccess.delStud(idCentreIntra, 1);
                SessionAccess.delStud(idCentreFinal, 1);
                //centreexamens
                SessionAccess.delStud(idCentreIntra, 2);
                SessionAccess.delStud(idCentreFinal, 2);
                //
                SessionAccess.delStud(Nrc, SId, ni);
            }
            else
            {
                SessionAccess.delStud(Nrc, SId, ni);
                erreur.Text = "L'étudiant a été supprimé avec succès !";
                erreur.Visible = true;
            }
            afficherDetails();
        }
        catch (Exception ex)
        {
            erreur.Text = "Erreur: " + ex.Message + ex.StackTrace;
            erreur.Visible = true;
        }
    }
    protected void sentMsg_Click(object sender, EventArgs e)
    {
        try
        {
            //on récupère le message et on envoi le message !
            MessagePreEnregistre msg = SessionAccess.getLstMsg(IdMsg);
            //Utilitaires.sendMail(EdpermConfig.MailFrom, CourielEtudiant, msg.objet, msg.corps);
            //
            erreur.Text = "Le rappel a été envoyé avec succès !";
            erreur.Visible = true;
        }
        catch (Exception ex)
        {
            erreur.Text = "Erreur: " + ex.Message + ex.StackTrace;
            erreur.Visible = true;
        }
    }
    protected void chooseMsg_SelectedIndexChanged(object sender, EventArgs e)
    {
        //
        DropDownList msg = (DropDownList)sender;
        IdMsg = int.Parse(msg.SelectedValue);
    }
}