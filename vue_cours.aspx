﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="vue_cours.aspx.cs" Inherits="vue_cours" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title> Vue des cours </title>
        <!-- CSS -->
    <link href="App_Themes/css/bootstrap.css" rel="stylesheet" />
    <link href="App_Themes/css/skeleton.css" rel="stylesheet" />
    <link href="App_Themes/css/flaticon.css" rel="stylesheet" />
    <link href="App_Themes/css/scrollpane.css" rel="stylesheet" />
    <link href="App_Themes/css/frame.css" rel="stylesheet" />
    <link href="App_Themes/css/course_view.css" rel="stylesheet"/>
        <!-- JS -->
    <script src="App_Themes/js/jquery.js" type="text/javascript"></script>
    <script src="App_Themes/js/jquery.mousewheel.js"></script>
    <script src="App_Themes/js/jquery.jscrollpane.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="App_Themes/js/course_view.js" type="text/javascript"></script>

        <!-- <script type="text/javascript" src="./js/html5shiv.js"></script> -->
        <!-- <script type="text/javascript" src="./js/modernizr-latest.js"></script> -->
</head>
<body>
    <form id="form1" runat="server">
<div id="wrapper">
          <header class="blue_bar">
            <div class="container">
                <div class="three columns logo">
                    <p class="">
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">
               <div class="ten columns alerts_admin">
                 <span> Creation du cours, reussie !! </span>
               </div><!-- end alerts admin -->
               <p class="twelve columns view_title">
                   <span> Prière de cliquer sur un cours pour accéder aux étudiants inscrits de ce cours </span>
               </p> <!-- end view title -->
                <div class="seven columns course_list">
                    <ul class="twelve columns">
                        <li>
                            <a class="five columns" href="detailsCours.aspx"> info 1000 </a>
                            <div class="offset-by-three four columns summary">
                                <span style=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                            </div>
                        </li>
                        <li>
                            <a class="five columns" href="#"> math 2000 </a>
                            <div class="offset-by-three four columns summary">
                                <span style=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                                <span class=""> 3/4 </span>
                            </div>
                        </li>
                        <li> <a href="#"> math 3400 </a></li>
                        <li> <a href="#"> gmec 2200 </a></li>
                        <li> <a href="#"> adrh 2100 </a></li>
                        <li> <a href="#"> piza 1700 </a></li>
                        <li> <a href="#"> piza 1700 </a></li>
                        <li> <a href="#"> piza 1700 </a></li>
                    </ul>
                </div><!-- end course list -->
                <div class="three columns legend">
                    <span class="twelve columns"> ceci</span>
                    <span class="twelve columns"> cela </span>
                    <span class="twelve columns"> celui </span><%--<span> labas </span>--%>
                    <span class="twelve columns"> voici </span>
                </div>
              </div><!-- container -->
            </div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
            <footer> <!-- release the display to see the footer -->
               <div class="container">
                <div class="one column logo">
                        <a href="#">
                            <img src="images/logo_udem_foot.png"/>
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="four columns copyright">
                    <span class="twelve columns"> &copy;copyright 2015 - tous droits reservés </span>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div> <!-- wrapper -->
    </form>
</body>
</html>
