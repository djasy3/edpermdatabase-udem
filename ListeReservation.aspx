﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermTestingMaster.master" AutoEventWireup="true" CodeFile="ListeReservation.aspx.cs" Inherits="ListeReservation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ajax" runat="server" />
    <asp:UpdatePanel ID="upPanel" runat="server">
        <ContentTemplate>
            <h3>Liste des reservations</h3><br />
            <asp:Label ID="erreur" runat="server" ForeColor="Red"></asp:Label>
            <hr />
            <asp:Table ID="tab1" runat="server" style="width:40%; text-align:left">
                <asp:TableRow>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell style="text-align:right">Session:</asp:TableCell>
                    <asp:TableCell><asp:DropDownList ID="sessionActuelle" runat="server" AutoPostBack="true" OnSelectedIndexChanged="sessionActuelle_SelectedIndexChanged"></asp:DropDownList></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Cours</asp:TableCell>
                    <asp:TableCell><asp:DropDownList ID="lstCours" runat="server" OnSelectedIndexChanged="lstCours_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                    <asp:TableCell></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>Date Intra:</asp:TableCell>
                    <asp:TableCell><asp:Label ID="dateIntra" runat="server" /></asp:TableCell><asp:TableCell>Lieux Examen:</asp:TableCell><asp:TableCell>
                        <asp:DropDownList ID="lieuxExams" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lieuxExams_SelectedIndexChanged">
                            <asp:ListItem Value="nb">Au N.B.</asp:ListItem>
                            <asp:ListItem Value="hnb">Hors du N.B.</asp:ListItem>
                            <asp:ListItem Value="all" Selected="True">Partout</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell></asp:TableRow><asp:TableRow>
                    <asp:TableCell>Date Finale:</asp:TableCell><asp:TableCell><asp:Label ID="dateFin" runat="server" /></asp:TableCell><asp:TableCell>Type d'Examen:</asp:TableCell><asp:TableCell>
                        <asp:DropDownList ID="typExam" runat="server" AutoPostBack="true" OnSelectedIndexChanged="typExam_SelectedIndexChanged">
                            <asp:ListItem Value="intra">Intra</asp:ListItem>
                            <asp:ListItem Value="final">Final</asp:ListItem>
                            <asp:ListItem Value="both" Selected="True">Les deux</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell></asp:TableRow></asp:Table><hr /><asp:Panel ID="downPanel" runat="server">
                <asp:ListView ID="lstReservation" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder" >
                    <LayoutTemplate>
                        <table id="tab2" style="width:100%; text-align:justify">
                            <tr>
                                <td>Cours</td><td>Type<br />d'examen</td><td>Nom</td><td>Prenom</td><td>Couriel</td><td>Téléphone jour</td><td>Ville</td><td>Notes</td><td>ACK</td></tr><asp:PlaceHolder ID="gpePlcHolder" runat="server"></asp:PlaceHolder>
                       </table>
                    </LayoutTemplate>
                    <GroupTemplate>
                        <tr>
                            <asp:PlaceHolder ID="itmPlcHolder" runat="server"></asp:PlaceHolder>
                        </tr>
                    </GroupTemplate>
                    <ItemTemplate>                       
                            <td><asp:HyperLink ID="sigle" runat="server" NavigateUrl='<%# Liens.detailsCours(Eval("c_sigle").ToString()) %>' ToolTip='Details du cours' Text='<%# Eval("c_sigle") %>' /></td>
                            <td><asp:Label ID="typeExam" runat="server" Text='<%#Eval("ce_typeExam") %>' /></td>
                            <td>
                                <asp:HyperLink ID="et_lien1" runat="server" NavigateUrl='<%# Liens.detailsEtud(Eval("et_couriel").ToString()) %>' ToolTip='Info Etudiant' Text='<%# Eval("et_prenom") %>' />
                            </td>
                            <td>
                                <asp:HyperLink ID="et_lien2" runat="server" NavigateUrl='<%# Liens.detailsEtud(Eval("et_couriel").ToString()) %>' ToolTip='Info Etudiant' Text='<%# Eval("et_nom") %>' />
                            </td>
                            <td><asp:Label ID="couriel" runat="server" Text='<%#Eval("et_couriel") %>' /></td>
                            <td><asp:Label ID="phoneJr" runat="server" Text='<%#Eval("et_telJour") %>' /></td>
                            <td><asp:Label ID="ville" runat="server" Text='<%#Eval("le_ville") %>' /></td>
                            <td><asp:Textbox ID="cd" runat="server" Text='<%# Eval("ce_comment") %>' TextMode="MultiLine" OnTextChanged="cd_TextChanged" AutoPostBack="true"/></td>
                            <td><asp:CheckBox ID="ack" runat="server" Checked="false" AutoPostBack="true" OnCheckedChanged="ack_CheckedChanged" /></td>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <p> Pas de reservation encore disponible </p></EmptyDataTemplate></asp:ListView></asp:Panel>
        </ContentTemplate></asp:UpdatePanel>
</asp:Content>

