// custom validation for inscription_etudiant form
$(function () {

    function swapClass(a) {
        if (a.hasClass("tip")) {
            a.removeClass("tip");
            a.addClass("error");
            a.css({ display: "block" });
        }
    } // end of swapClass



    function removeError(a) {
        if (a.hasClass("error")) {
            a.removeClass("error");
            a.css({ display: "none" });
            a.addClass("tip");
        } else { console.log("smoothies"); }

    } // end of removeError

    function curator(reg, value, talk) { // this function checks if the entries of the users are correct
        if (reg.test(value)) {
            var bingo = value.match(reg);
            if (bingo) {
                var submitBtn = $("input.bottomForm");
                var submitBtnDisabled = submitBtn.is(':disabled');
                removeError(talk); // we remove error message
                if (submitBtnDisabled) {
                    submitBtn.prop('disabled', false);
                }// end 
            }
        } else {
            var submitBtn = $("input.bottomForm");
            swapClass(talk); // we display error message
            submitBtn.prop('disabled', true);
        }
    }

    // Student textfield below 
    $("div.inputHolder input.last_n").keyup(function () {
        // last_name field
        var last_name_reg = /^[a-zA-Z ]{3,16}$/;
        var last_name_value = $(".inputHolder input.last_n").val();
        var last_name_talk = $("div.inputHolder span.last_n");

        curator(last_name_reg, last_name_value, last_name_talk);

    }); //end

    $("div.inputHolder input.first_n").keyup(function () {
        // first_name field
        var first_name_reg = /^[a-zA-Z ]{3,16}$/;
        var first_name_value = $(".inputHolder input.first_n").val();
        var first_name_talk = $("div.inputHolder span.first_n");

        curator(first_name_reg, first_name_value, first_name_talk);

    }); //end

    $("div.inputHolder input.ni").keyup(function () {
        // NI field
        var ni_reg = /^A00+[0-9]{6,6}$/;
        var ni_value = $(".inputHolder input.ni").val();
        var ni_talk = $("div.inputHolder span.ni");

        curator(ni_reg, ni_value, ni_talk);

    }); //end    

    $("div.inputHolder input.email").keyup(function () {
        // email field
        var email_reg = /^[A-Za-z0-9._-]+@[A-Za-z0-9-]+\.[A-Za-z]{2,4}$/;
        var email_value = $(".inputHolder input.email").val();
        var email_talk = $("div.inputHolder span.email");

        curator(email_reg, email_value, email_talk);

    }); //end  

    $("div.inputHolder input.tel_j").keyup(function () {
        // email field
        var tel_reg = /^[0-9-+() ]{9,15}$/;
        var tel_value = $(".inputHolder input.tel_j").val();
        var tel_talk = $("div.inputHolder span.tel_j");

        curator(tel_reg, tel_value, tel_talk);

    }); //end  

    // supervisor textFields below
    $("div.inputHolder input.sup_last_n").keyup(function () {
        // last_name field
        var sup_last_name_reg = /^[a-zA-Z ]{3,16}$/;
        var sup_last_name_value = $(".inputHolder input.sup_last_n").val();
        var sup_last_name_talk = $("div.inputHolder span.sup_last_n");

        curator(sup_last_name_reg, sup_last_name_value, sup_last_name_talk);

    }); //end

    $("div.inputHolder input.sup_first_n").keyup(function () {
        // first_name field
        var sup_first_name_reg = /^[a-zA-Z ]{3,16}$/;
        var sup_first_name_value = $(".inputHolder input.sup_first_n").val();
        var sup_first_name_talk = $("div.inputHolder span.sup_first_n");

        curator(sup_first_name_reg, sup_first_name_value, sup_first_name_talk);

    }); //end


    $("div.inputHolder input.sup_email").keyup(function () {
        // email field
        var sup_email_reg = /^[A-Za-z0-9._-]+@[A-Za-z0-9-]+\.[A-Za-z]{2,4}$/;
        var sup_email_value = $(".inputHolder input.sup_email").val();
        var sup_email_talk = $("div.inputHolder span.sup_email");

        curator(sup_email_reg, sup_email_value, sup_email_talk);

    }); //end  

    $("div.inputHolder input.sup_tel_j").keyup(function () {
        // email field
        var sup_tel_reg = /^[0-9-+() ]{9,15}$/;
        var sup_tel_value = $(".inputHolder input.sup_tel_j").val();
        var sup_tel_talk = $("div.inputHolder span.sup_tel_j");

        curator(sup_tel_reg, sup_tel_value, sup_tel_talk);

    }); //end 


    /*     function noEmptyField() { // this function checks if all the required fields are filled
             var a = $("input.last_n").val();
             if (a.text=="") {
                 var submitBtn = $("input.bottomForm");
                 submitBtn.prop('disabled', true);
             }
         } */ // i will remake the function tomorrow, it doesn't work now.




});



/* if(name_reg.test(name_value)) {
     var bingo = name_value.match(name_reg);
     if(bingo) {
        removeError(name_talk);
     } 
 } else {
     swapClass(name_talk);
 } */