// some jQuery goodies

// installing the customized scrollbars 
/*$(function () {
    $(".studentBooking").jScrollPane();
    $('.scroll-pane-arrows').jScrollPane(
		{
		    showArrows: true,
		    verticalGutter: 2
		}
	);
})*/

/*intialize the states of the buttons and links that need initialization */
$(function () {
    var superVisor_Option_Links = $("div.chooseOption_Sup a");
    /* we deactivate the supervisor options when page loads
       we'll activate them back when question 2 gets answered */
    superVisor_Option_Links.prop('disabled', true);
    $("div.chooseOption_Sup").css({'display':'none'});

});

// custom functions start. The codes that follow cover the following features:
/* Activation and deactivation of (Yes - No) buttons for the choice of exam location */
/* Activation and deactivation of choosing supervisor options for the 3rd question */
/* Intelligent hiding of the supervisor options if while the option was enable, the user
   pressed on of the question 2 buttons, making the question 2 unanswered */
/* auto scroll of form if user sarts typing into an area that's not completly visible to him */


$(function(){
    // Custom functions for the inscription_etudiant page
    // Activating and deactivating buttons

    $("input.yesBoth").click(function(){ // if the user replies yes to the second question.

        var yesBoth = $("input.yesBoth");
        var noBoth = $("input.noBoth");
        var noBothDisabled = noBoth.is(':disabled');
        var yesBothDisabled = yesBoth.is(':disabled');
        var superVisor_Option_Links = $("div.chooseOption_Sup a");
        if (noBothDisabled){  // if the the 'non' button was disabled when user presses 'yes'
                 noBoth.prop('disabled', false); // then we activate the 'non' button again
                 noBoth.attr("title", "Cliquer sur Oui pour activer ce boutton");
                 /* if the other button was disabled when we clicked this button, it means the supervisor
                    buttons were already activated, so since the user is clicking this button back, thus leaving
                 the second question unsanswered we have to deactivate the supervisor button again*/
                 superVisor_Option_Links.prop('disabled', true);
                 $(".chooseOption_Sup").css({ backgroundColor: "silver" });
                 $(".chooseOption_Sup").css({ display: "none" });
            

                /* if the user has opened the choose sup option and accidentaly clicks back on the
                   yes or no of the question 2 to close it, then the choose sup panels also disappears*/
                 noBoth.attr("title", "");
                 $("#suggestNewSup_intra").collapse('hide');
                 $("#suggestNewSup_final").collapse('hide');
                 $(".chooseOption_Sup").css({borderTop:"none"}); // we deactivate the borderTop of the succedding div because the div we are in here already has a border bottom, and the stroke line of the border will be overcharged if both borders were active. In the else statement since the next div will slideDown then it should have a borderTop to respect the design, so in the next else statement we will set a borderTop.
// color of the yes button when set back to inactive
$("input.yesBoth").css({backgroundColor:"#7aa3c7"});
               } else {  // color of the yes button when active
                         $("input.yesBoth").css({backgroundColor:"#f7b325"});
                         noBoth.prop('disabled', true);
                         noBoth.attr("title", "Cliquer sur Oui pour activer ce boutton");
                         $("select.intra_locations").prop('selectedIndex', -1); // clean up the selections for intra
                         $("select.finals_locations").prop('selectedIndex', -1); // clean up the selections for finals
                         // handling chooseOption sup styles
                         $(".chooseOption_Sup").css({ backgroundColor: "#da9e3c" });
                         superVisor_Option_Links.css({color:"#fff"});
                         superVisor_Option_Links.prop('disabled', false);
                         $(".chooseOption_Sup").css({borderTop:"1px solid #7aa3c7"});

                          }

                      }

    ); // close click event

    $("input.noBoth").click(function() {
                var yesBoth = $("input.yesBoth");
                var noBoth = $("input.noBoth");
                var noBothDisabled = noBoth.is(':disabled');
                var yesBothDisabled = yesBoth.is(':disabled');
          /* if the other button was disabled when we clicked this button, it means the supervisor
          buttons where activated, so since the user is clicking this button back, thus leaving
          the second question unsanswered we have to deactivate the supervisor button again*/
                var superVisor_Option_Links = $("div.chooseOption_Sup a");
         if(yesBothDisabled) {
             yesBoth.prop('disabled', false);
             // first, handling supervisor options
             superVisor_Option_Links.prop('disabled', true);
             $(".chooseOption_Sup").css({ backgroundColor: "silver" });
                /* if the user has opened the choose sup option and accidentaly clicks back on the
                   yes or no of the question 2 to close it, then the choose sup panels also disappears*/
             $("#suggestNewSup_intra").collapse('hide');
             $("#suggestNewSup_final").collapse('hide');
              yesBoth.attr("title", "");
$("input.noBoth").css({backgroundColor:"#7aa3c7"}); // color of button when inactive
         } else { yesBoth.prop('disabled', true);
                  yesBoth.attr("title", "Cliquer sur Non pour activer ce boutton");
                  $("select.singleLocation").prop('selectedIndex', -1); // clean up the selection
             // handling chooseOption sup styles
                  $(".chooseOption_Sup").css({ backgroundColor: "#da9e3c" });
                  superVisor_Option_Links.css({ color: "#fff" });
                  $(".chooseOption_Sup").css({ display: "block" });
                  superVisor_Option_Links.prop('disabled', false);
                  // handling button style
                  $("input.noBoth").css({backgroundColor:"#f7b325"}); // color of button when active
                }
    // If the student wants to suggest a new city:

    }); // end buttons activation


    // Panel display. on click of a.next second panel shows and on click of a.prev first panel shows up
    $("div.personal_infos a.next").click(function() {
        var first_panel = $("div.personal_infos"); // this panel is open by default
        var second_panel = $("div.second_panel"); // this panel is closed by default

        first_panel.css({display:'none'});
         second_panel.css({display:'block'});
    });

    $("div.second_panel a.prev").click(function() {
        var first_panel = $("div.personal_infos"); // this panel is open by default
        var second_panel = $("div.second_panel"); // this panel is closed by default
        var incomingDiv = $("div.second_panel");

        second_panel.css({display:'none'});
        first_panel.css({display:'block'});

    });



});



/* The auto scroll function */
 $(function() {
     $("#suggestNewSup input:eq(2)").focusin(function(){
         if(this.value!=" "){
            $("form.studentBooking").animate({ scrollTop: $(".studentBooking").height() }, "slow");
         }
     });
 });

/*  When a student wants to confirm new city */
$(function() {
    var intra_city_index = $("select.intra_locations");
    var final_city_index =  $("select.finals_locations");
    
});
// BOTH EXAMS WILL TAKE PLACE AT THE SAME LOCATION
$(function () {
    // BOTH EXAMS WILL TAKE PLACE AT THE SAME LOCATION
    // This function displays the relevant cities according to the answer of the student. 
    // If the student clicks that he will be inside nb for both examns we show him the cities
    // that are inside New-Brunswick, and we remove the supervisor choice option, since supervisors
    // will be automatically assigned by administration. 
    // if the student clicks " outside of New-Brunswick " then we show him cities that are outside of nb
    // and we allow the student to suggest a new supervisor.

    // student chooses if he will do both exams in nb or outsite nb 
    var examInNb = $(".geo_choose p.geo_in");
    var examOutNb = $(".geo_choose p.geo_out");
    var backToOption = $(".back_to_option");
    var inNb_check = $("input.geo_in_check");
    var outNb_check = $("input.geo_out_check");

    
    // exams are in nb 
    examInNb.click(function in_nb_same() {
        inNb_check.prop('checked', true);
        $(".geo_choose").css({display:"none"});
        $(".geo_choose_city").css({ display: "block" });
        $(".geo_choose_city select.in_nb_select").css({ display: "block" }); // load the list of cities that are within nb
        $("select.out_nb_select").prop('selectedIndex', -1); // clean up the selections for outisde of nb cities
        /*we substitute the supervisor options by another sentence */
        $(".chooseOption_Sup").css({ display: "none" });
        //$(".chooseOption_Sup a.getExistingSup").css({ 'display': 'none' });
        outNb_check.prop('checked', false);
        if (inNb_check.is(':checked')) {
            console.log("ghost operation demo success !!");
        }

    }); // end in nb

    examOutNb.click(function () {
        outNb_check.prop('checked',true);
        $(".geo_choose").css({ display: "none" });
        $(".geo_choose_city").css({ display: "block" });
        $(".geo_choose_city select.out_nb_select").css({ display: "block" });
        $(".chooseOption_Sup").css({ display: "block" });
        $("select.in_nb_select").prop('selectedIndex', -1); // clean up the selection for inside of nb cities
        inNb_check.prop('checked', false);
        if (outNb_check.is(':checked')) {
            console.log("ghost operation 2 demo success !!");
        }
    }); // end out nb 

    backToOption.click(function () {
        $(".geo_choose").css({ display: "block" });
        $(".geo_choose_city").css({ display: "none" });
        $(".geo_choose_city select.in_nb_select").css({display:"none"});
        $(".geo_choose_city select.out_nb_select").css({ display: "none" });
        //$(".chooseOption_Sup a").css({ display: "block" });
        
        
    });

  

});
// BOTH EXAMS WILL NOT TAKE PLACE AT THE SAME LOCATION 
$(function () {
    // BOTH EXAMS WILL NOT TAKE PLACE AT THE SAME LOCATION 

    var intraYes = $(".intra_yes");
    var intraNo = $(".intra_no");
    var finalYes = $(".final_yes");
    var finalNo = $(".final_no");

    var intra_question = $(".intra_question");
    var final_question = $(".final_question");
    var intra_choose = $(".differentPlace_intra_choose");
    var final_choose = $(".differentPlace_final_choose");

    var intra_select_in_nb = $(".intra_in_nb");
    var intra_select_out_nb = $(".intra_out_nb");
    var final_select_in_nb = $(".final_in_nb");
    var final_select_out_nb = $(".final_out_nb");

    var radio_intra_no = $(".radio_intra_no");
    var radio_intra_yes = $(".radio_intra_yes");
    var radio_final_no = $("radio_final_no");
    var radio_final_yes = $("radio_final_yes");

    var backIntra = $(".back_to_intra_diff");
    var backFinal = $(".back_to_final_diff");

    intraYes.click(function () {
        intra_question.css({ display: 'none' });
        intra_choose.css({ display: 'block' });
        intra_select_out_nb.css({ display: 'block' }); 
        intra_select_in_nb.prop('selectedIndex', -1); // we cancel any other choice for the same question
        // send form traversing state to serve-side file 
        radio_intra_yes.prop('checked', true);
        console.log("intraYes clicked");
        radio_intra_no.prop('checked', false)
    }); // end intraYes click

    intraNo.click(function () {
        intra_question.css({ display: 'none' });
        intra_choose.css({ display: 'block' });
        intra_select_in_nb.css({ display: 'block' });
        intra_select_out_nb.prop('selectedIndex', -1);// we cancel any other choice for the same question
        // send form traversing state to serve-side file 
        radio_intra_no.prop('checked', true);
        console.log("intraNo clicked");
        radio_intra_yes.prop('checked', false)
        
    }); // end intraNo click

    finalYes.click(function () {
        final_question.css({ display: 'none' });
        final_choose.css({ display: 'block' });
        final_select_out_nb.css({ display: 'block' });
        final_select_in_nb.prop('selectedIndex', -1);
        // send form traversing state to serve-side file 
        radio_final_yes.prop('checked', true);
        console.log("finalYes clicked");
        radio_final_no.prop('checked', false)
        
    }); // end finalYes click

    finalNo.click(function () {
        final_question.css({ display: 'none' });
        final_choose.css({ display: 'block' });
        final_select_in_nb.css({ display: 'block' });
        final_select_out_nb.prop('selectedIndex', -1);
        radio_final_no.prop('checked', true);
        console.log("finalNo clicked");
        radio_final_yes.prop('checked', false)
        
    }); // end finalNo click

    backIntra.click(function () {
        intra_choose.css({ display: 'none' });
        intra_question.css({ display: 'block' });
        intra_select_in_nb.css({ display: 'none' }); // we close the other listBox if it was previously opened
        intra_select_out_nb.css({ display: 'none' });// we close the other listBox if it was previously opened
        $("#suggest_intra_city").collapse('hide'); // in case new city suggestion was active, the closure of the question answer will also close the suggested city inputs
    }); // end backIntra click

    backFinal.click(function () {
        final_choose.css({ display: 'none' });
        final_question.css({ display: 'block' });
        final_select_in_nb.css({ display: 'none' });
        final_select_out_nb.css({ display: 'none' });
        $("#suggest_final_city").collapse('hide');
    }); // end backIntra click 

});

//function to send event listener to behind file for the yes or no button for the question coming after 
// the course list question 

$(function () {
    var yesBtn = $(".yesBoth");
    var noBtn = $(".noBoth");
    var yesChecked = $(".yes_clicked");
    var noChecked = $(".no_clicked");

    yesBtn.click(function () {
        yesChecked.prop('checked', true);
        noChecked.prop('checked', false);
        if (yesChecked.is(':checked')) { console.log("the Yes button has been clicked"); }
        
    });

    noBtn.click(function () {
        noChecked.prop('checked', true);
        yesChecked.prop('checked', false);
        console.log("the No button has been clicked");
    });
});

// function to handle the fact that student chooses only one supervisor
$(function () {
    var link_sup_intra = $(".intra_new_sup_link");
    var link_sup_final = $(".final_new_sup_link");
    var check_same_sup = $(".checkSameSup");
    var text_unik_sup = "Choisir un superviseur unique pour mes deux examens";
    
    check_same_sup.bind('click', function() {
        if (check_same_sup.is(':checked')) {
            link_sup_final.css({ 'display': 'none' });
            link_sup_intra.html('<span>'+text_unik_sup+'</span>');
            link_sup_intra.css({ 'width': '70%' });
        } else {
            link_sup_intra.css({ 'width': '35%' });
            link_sup_final.css({ 'display': 'block' });
            link_sup_intra.html("<span>Superviseur ville intra</span>");
            //remember to add code to clean up the input text when students deselect the same_sup_check
        } // end if
         
     
    });
 
}); //end

//function to allow the student to know the supervisor for which he is filling the form 
$(function () {
    var parentDiv = $(".chooseOption_Sup");
    var allchild = $(".chooseOption_Sup a");
    var clickedChildDiv = parentDiv.find("a.is_clicked");
    
    allchild.click(function () {
        clickedChildDiv.removeClass("is_clicked");
        clickedChildDiv.collapse('hide');
        $(this).addClass("is_clicked");
    });
});

// Other:
/* function for supervisor links individual activation and deactivation
 to consult the code that enables the chooseOption_Sup div when a user clicks
 on the yes or no button, go to the code of they yes and no button activation
 it's near the end of the if statements */





