﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermProd.master" AutoEventWireup="true" CodeFile="DBoard.aspx.cs" Inherits="DBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title> Admin Dashboard </title>
    <link href="App_Themes/css/dashboard.css" rel="stylesheet" />
    <script src="App_Themes/js/dashboard.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ajax" runat="server" />
    <asp:UpdatePanel ID="updtPanel" runat="server">
        <ContentTemplate>
            <div class="twelve columns insights" style="visibility:hidden;">
                <span class="read_insight"> Ce button sert &agrave;: </span>
            </div><!-- end insights -->
            <asp:DropDownList runat="server" ID="listOfSessions" CssClass="five columns sessionList" OnSelectedIndexChanged="listOfSessions_SelectedIndexChanged" AutoPostBack="true" /> <!-- end sessionList -->
            <div class="ten columns currentSessionOverview">
                
                <div class="three columns courseOverview">
                    <span class="twelve columns view_title">
                        <asp:LinkButton ID="lienVersVueCours" runat="server" ToolTip="Allez vers la vue des cours" PostBackUrl="~/VueCours.aspx" >Cours</asp:LinkButton>
                        <%--<asp:HyperLink ID="lienVueCours" runat="server" ToolTip="Allez vers la vue des cours" NavigateUrl="~/VueCours.aspx">Cours</asp:HyperLink>--%>  </span>
                    <div class="eight columns totalEntries">
                        <span> <asp:Label ID="nbreCours" runat="server" /> </span>
                    </div>
                    <div class="three columns add">
                        <asp:HyperLink ID="ajoutCours" runat="server" ToolTip="Ajouter un cours" NavigateUrl="~/MontageSession.aspx" ><span> + </span></asp:HyperLink>
                    </div>
                    <div class="twelve columns tags">
                        <span role="button" class="three columns tag_singleton cours_one" title="Nombre total d'étudiants inscrits pour tous les cours">
                            34
                        </span>
                        <span class=" three columns tag_singleton cours_two" title="Nombre total d'étudiants ayant déjà confirmé leur présence à un examen">
                            28
                        </span>
                        <span class=" three columns tag_singleton cours_three" title="Nombre des centres d'examens disponible">
                            21
                        </span>
                        <span class=" three columns tag_singleton cours_four" title="Toute les tâches déjà completées">
                            7
                        </span>
                    </div>
                </div><!-- end course overview -->
                <div class="three columns studentOverview">
                    <span class="twelve columns view_title"><asp:HyperLink ID="lienDetailsEtudiant" runat="server" ToolTip="Allez vers la vue détaillée des étudiants" NavigateUrl="~/VueDetailCours.aspx" Text="&Eacute;tudiants" /></span>
                    <div class="eight columns totalEntries">
                        <span><asp:Label ID="nbreEtudInscrits" runat="server" /></span>
                    </div>
                    <div class="three columns add">
                        <asp:HyperLink ID="AjoutEtudiant" runat="server" ToolTip="Ajouter un etudiant" NavigateUrl="~/MontageSession.aspx" ><span> + </span></asp:HyperLink>
                    </div>
                    <div class="twelve columns tags">
                        <span class=" three columns tag_singleton count_one" title="">
                            34
                        </span>
                        <span class=" three columns tag_singleton count_two" title="">
                            28
                        </span>
                        <span class=" three columns tag_singleton count_three" title="">
                            21
                        </span>
                        <span class=" three columns tag_singleton count_four" title="">
                            7
                        </span>
                    </div>
                </div><!-- end student overview -->
                </a>
                <div class="three columns examCenterOverview">
                    <span class="twelve columns view_title"> 
                        <asp:HyperLink ID="lienCentreExamen" runat="server" ToolTip="Allez vers la vue détaillée des centres d'examens" NavigateUrl="~/VueCentresExamens.aspx" Text="Centres" /></span>
                    <div class="eight columns totalEntries">
                        <span> <asp:Label ID="centres" runat="server" /> </span>
                    </div>
                    <div class="three columns add">
                        <asp:HyperLink ID="ajoutCentre" runat="server" ToolTip="Ajouter centres examens" NavigateUrl="~/MontageSession.aspx" ><span> + </span></asp:HyperLink>
                    </div>
                    <div class="twelve columns tags">
                        <span class=" three columns tag_singleton count_one" title="">
                            34
                        </span>
                        <span class=" three columns tag_singleton count_two" title="">
                            28
                        </span>
                        <span class=" three columns tag_singleton count_three" title="">
                            21
                        </span>
                        <span class=" three columns tag_singleton count_four" title="">
                            7
                        </span>
                    </div>
                </div><!-- end centers overview -->
            </div><!-- end currentSessionOverview -->
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="five columns createSession">
        <asp:HyperLink ID="lienVersMontageSession" runat="server" NavigateUrl="~/MontageSession.aspx" Text="Enregistrer une nouvelle session" CssClass="createSessionBtn" />
    </div><!-- end createSession -->
</asp:Content>

