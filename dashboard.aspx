﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title> Admin Dashboard </title>
 
        <link href="App_Themes/css/skeleton.css" rel="stylesheet" />
    <link href="App_Themes/css/frame.css" rel="stylesheet" />
    <link href="App_Themes/css/dashboard.css" rel="stylesheet" />
    <!-- script -->
    <script src="App_Themes/js/jquery.js"></script>
    <script src="App_Themes/js/bootstrap.js"></script>
    <script src="App_Themes/js/dashboard.js"></script>
    <script type="text/javascript">

    </script>
</head>
<body>
    <form id="form1" runat="server">
<div id="wrapper">
          <header class="blue_bar">
            <div class="container">
                <div class="border_bottom"><!-- Cette div est ici pour encadrer juste le les elements de la barre blue et faire une ligne blanche en dessous d'eux-->
                <div class="three columns logo">
                    <p class="">
                        <a href="#"> &Eacute;ducation permanente </a>
                    </p>
                </div> <!-- logo -->
                   <!-- +++ -->
                <div class="four columns current_section">
                    <p class="">
                        <a href="#"> path </a>
                    </p>
                </div> <!-- current section -->
                   <!-- +++ -->
                <div class="two columns documentation">
                    <p class=" ">
                        <a href="#"> Documentation </a>
                    </p>
                </div> <!-- documentation -->
                  <!-- +++ -->
                <div class="two columns user_name">
                    <p class="">
                        <a href="#"> John  </a>
                    </p>
                </div> <!-- user menu dropDown -->
                </div><!-- end border bottom -->
              </div> <!-- end container -->
            </header> <!-- blue_bar -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
             <!-- main content (Ajax loads and all) -->
            <div class="main">
              <div class="container">
                  <div class="twelve columns insights" style="visibility:hidden;">
                      <span class="read_insight"> Ce button sert &agrave;: </span>
                  </div><!-- end insights -->

                  <asp:DropDownList runat="server" ID="listOfSessions" CssClass="five columns sessionList">   
                  </asp:DropDownList><!-- end sessionList -->
                  <div class="ten columns currentSessionOverview">
                      <a href="#">
                      <div class="three columns courseOverview">
                          <span class="twelve columns view_title"> Cours </span>
                          <div class="eight columns totalEntries">
                              <span> 07 </span>
                          </div>
                          <div class="three columns add">
                              <a href="#"><span> + </span></a>
                          </div>
                          <div class="twelve columns tags">
                              <span role="button" class="three columns tag_singleton cours_one" title="desc 1">
                                  34
                              </span>
                              <span class=" three columns tag_singleton cours_two" title="desc 2">
                                  28
                              </span>
                              <span class=" three columns tag_singleton cours_three" title="desc 3">
                                  21
                              </span>
                              <span class=" three columns tag_singleton cours_four" title="desc 4">
                                  7
                              </span>
                          </div>
                      </div><!-- end course overview -->
                      <div class="three columns studentOverview">
                          <span class="twelve columns view_title"> &Eacute;tudiants </span>
                          <div class="eight columns totalEntries">
                              <span> 88 </span>
                          </div>
                          <div class="three columns add">
                              <a href="#"><span> + </span></a>
                          </div>
                          <div class="twelve columns tags">
                              <span class=" three columns tag_singleton etudiant_one" title="">
                                  34
                              </span>
                              <span class=" three columns tag_singleton etudiant_two" title="">
                                  28
                              </span>
                              <span class=" three columns tag_singleton etudiant_three" title="">
                                  21
                              </span>
                              <span class=" three columns tag_singleton etudiant_four" title="">
                                  7
                              </span>
                          </div>
                      </div><!-- end student overview -->
                      </a>
                      <div class="three columns examCenterOverview">
                          <span class="twelve columns view_title"> Centres </span>
                          <div class="eight columns totalEntries">
                              <span> 18 </span>
                          </div>
                          <div class="three columns add">
                              <a href="#"><span> + </span></a>
                          </div>
                          <div class="twelve columns tags">
                              <span class=" three columns tag_singleton centre_one" title="">
                                  34
                              </span>
                              <span class=" three columns tag_singleton centre_two" title="">
                                  28
                              </span>
                              <span class=" three columns tag_singleton centre_three" title="">
                                  21
                              </span>
                              <span class=" three columns tag_singleton centre_four" title="">
                                  7
                              </span>
                          </div>
                      </div><!-- end centers overview -->
                  </div><!-- end currentSessionOverview -->
                  <div class="five columns createSession">
                    <a href="montage_session.aspx" class="createSessionBtn" >Creer une session</a>
                  </div><!-- end createSession -->
              </div><!-- container -->
            </div> <!-- end main -->
<!-- = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = -->
            <footer> <!-- release the display to see the footer -->
               <div class="container">
                <div class="one column logo">
                        <a href="#">
                            <img src="images/logo_udem_foot.png"/>
                       </a>
                </div> <!-- logo -->
                  <!-- +++ -->
                <div class="four columns copyright">
                    <span class="twelve columns"> &copy;copyright 2015 - tous droits reservés </span>
                </div> <!-- user menu dropDown -->
              </div> <!-- end container -->
            </footer>
      </div> <!-- wrapper -->
    </form>
</body>
</html>
