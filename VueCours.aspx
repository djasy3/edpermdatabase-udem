﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EdPermProd.master" AutoEventWireup="true" CodeFile="VueCours.aspx.cs" Inherits="VueCours" ClientIDMode="AutoID" %>

<%@ PreviousPageType VirtualPath="~/DBoard.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="App_Themes/css/course_view.css" rel="stylesheet"/>
    <script src="App_Themes/js/coursVuesMod.js"></script>
    <script src="App_Themes/js/bootstrap.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ajax" runat="server"/>
    <div class="ten columns alerts_admin">
        <span> Creation du cours, reussie !! </span>
    </div><!-- end alerts admin -->
    <p class="twelve columns view_title">
        <span> Prière de cliquer sur un cours pour accéder aux étudiants inscrits de ce cours 
            <asp:Label ID="va" runat="server" ForeColor="Red" />
        </span>
    </p> <!-- end view title -->
    <div class="seven columns course_list">
        <asp:ListView ID="lstCours" runat="server" GroupPlaceholderID="gpePlcHolder" ItemPlaceholderID="itmPlcHolder">
            <LayoutTemplate>
                <ul class="twelve columns">
                    <asp:PlaceHolder ID="gpePlcHolder" runat="server" />
                </ul>
            </LayoutTemplate>
            <GroupTemplate>
                <asp:PlaceHolder ID="itmPlcHolder" runat="server" />
            </GroupTemplate>
            <ItemTemplate> 
                <li>
                    <asp:LinkButton ID="detailsCours" runat="server" CommandArgument='<%# Eval("c_id") %>' OnClick="detailsCours_Click" Text='<%# Eval("c_sigle") %>' CssClass="five columns" PostBackUrl="~/VueDetailCours.aspx" />
                    <div class="offset-by-three four columns summary">
                        <span class=""> <%# SessionAccess.nbreEtudiantInscrit(Sid, int.Parse(Eval("c_id").ToString())) %> </span>
                        <span style=""> <%# SessionAccess.nbreEtudiant(Sid, int.Parse(Eval("c_id").ToString())) %> </span>
                        <span class=""> <%# SessionAccess.nbreCentreExams(Sid, int.Parse(Eval("c_id").ToString())) %> </span>
                        <span class=""> 3/6 </span>
                    </div>
                </li> 
            </ItemTemplate>
            <EmptyDataTemplate>
                <li> <a href="#"> Aucun cours </a></li>
            </EmptyDataTemplate>
        </asp:ListView>
    </div><!-- end course list -->
    <div class="three columns legend">
        <span class="twelve columns"> Etudiants inscrits </span>
        <span class="twelve columns"> Etudiants ayants déjà confirmé </span>
        <span class="twelve columns"> Centres d'examens </span>
        <span class="twelve columns"> Taches déjà complétées </span>
    </div>
</asp:Content>

