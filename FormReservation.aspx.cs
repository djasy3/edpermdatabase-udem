﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FormReservation : System.Web.UI.Page
{
    CurrentSession sessionCourante = SessionAccess.getCurrentSession();

    public int Nrc
    {
        get { return ExamAccess.getNrc(sigle_cours.Text); }
    }
    //
    public Button BtnFinal
    {
        get { return finalBtn; }
        set { finalBtn = value; }
    }
    //
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.msg.Visible = false;
            mmEndrtPanel.Visible = false;
            dfrtEndrtWizard.Visible = false;
            //dfrtEndrtPanel.Visible = false;
            //
            mmPlcHnb.Visible = false;
            intraHnb.Visible = false;
            finalHnb.Visible = false;
            //
            BtnFinal.Visible = true;
            BtnFinal.Enabled = false;
            //
            errorText.Visible = false;
        }
        //index = 0 = tous les deux examens, intra et final se déroulent à la meme place !
        if (chckVille.SelectedIndex == 0)
        {
            BtnFinal.Visible = true;

            if (this.mmPlc.InOutSelectedValue == "nb")
            {
                BtnFinal.Enabled = true;
                mmPlcHnb.Visible = false;
            }
            else if (this.mmPlc.InOutSelectedValue == "hnb")
            {
                BtnFinal.Enabled = true;
                mmPlcHnb.Visible = true;
            }
            else
            {
                mmPlcHnb.Visible = false;
                BtnFinal.Enabled = false;
            }
        }//intra et final se déroulent à des endroits différents
        else if (chckVille.SelectedIndex == 1)
        {
            BtnFinal.Visible = false;
            //for intra
            if (intraPlc.InOutSelectedValue == "nb")
            {
                intraHnb.Visible = false;
            }
            else if (intraPlc.InOutSelectedValue == "hnb")
            {
                intraHnb.Visible = true;
            }
            else
            {
                intraHnb.Visible = false;
            }
            //for finale
            if (finalPlc.InOutSelectedValue == "nb")
            {
                finalHnb.Visible = false;
            }
            else if (finalPlc.InOutSelectedValue == "hnb")
            {
                finalHnb.Visible = true;
            }
            else
            {
                finalHnb.Visible = false;
            }

        }

    }
    //
    protected void chckVille_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (chckVille.SelectedIndex == 0)
        {
            mmEndrtPanel.Visible = true;
            //dfrtEndrtPanel.Visible = false;
            dfrtEndrtWizard.Visible = false;
            //suiteHnb.Visible = false;
            //Clear();
        }
        else if (chckVille.SelectedIndex == 1)
        {
            //dfrtEndrtPanel.Visible = true;
            dfrtEndrtWizard.Visible = true;
            mmEndrtPanel.Visible = false;
            //suiteHnb.Visible = false;
            //Clear();
        }
        else
        {
            this.msg.Visible = true;
            mmEndrtPanel.Visible = false;
            //dfrtEndrtPanel.Visible = false;
            dfrtEndrtWizard.Visible = false;
            BtnFinal.Enabled = false;
        }
    }
    //méthode pour confirmer la reservation sans superviseur
    protected void save(string typeExam, int idLieuExamen)
    {
        if (!ExamAccess.aDejaConfirmeExamen(ni.Text, Nrc, sessionCourante.Id))
        {
            //si l'étudiant est inscrit, on met à jour ses infos dans la base;
            SessionAccess.updateEtudiant(ni.Text, nom.Text, prenom.Text, email.Text, adresse.Text, phone_jour.Text, phone_nuit.Text, "");
            //ensuite on enregistre l'examen à présenter
            ExamAccess.setExam(Nrc, typeExam, ni.Text, idLieuExamen, sessionCourante.Id);
            //ensuite on fait la reservation
            ExamAccess.setReservation(ExamAccess.getIdCentre(Nrc, ni.Text, typeExam, sessionCourante.Id));
        }
        else
            throw new Exception("Une confirmation a déjà été faite pour ce cours et pour cet étudiant\n veuillez consulter votre e-mail pour avoir le lien de modification");

    }
    //méthode pour confirmer la reservation avec superviseur
    protected void save(string typeExam, int idLieuExamen, string superviseur)
    {
        if (!ExamAccess.aDejaConfirmeExamen(ni.Text, Nrc, sessionCourante.Id))
        {
            //si l'étudiant est inscrit, on met à jour ses infos dans la base;
            SessionAccess.updateEtudiant(ni.Text, nom.Text, prenom.Text, email.Text, adresse.Text, phone_jour.Text, phone_nuit.Text, "");
            //ensuite on enregistre l'examen à présenter
            ExamAccess.setExam(Nrc, typeExam, ni.Text, superviseur, idLieuExamen, sessionCourante.Id);
            //ensuite on fait la reservation
            ExamAccess.setReservation(ExamAccess.getIdCentre(Nrc, ni.Text, typeExam, sessionCourante.Id));
        }
        else
            throw new Exception("Une confirmation a déjà été faite pour ce cours et pour cet étudiant\n veuillez consulter votre e-mail pour avoir le lien de modification");
    }
    //
    protected void intraFinalConfirmPlace(Controls_LieuxExams Lieux, Controls_ExamHorsNB ExamHnb, string typeExam)
    {
        string temp;//recupère l'id du superviseur
        //les deux examens se passeront au nb et que les villes sont présentes
        if (Lieux.InOutSelectedValue == "nb")
        {
            if (Lieux.VilleNb != "autre")
            {
                save(typeExam, int.Parse(Lieux.VilleNb));
            }
            else//l'endroit spécifié pour faire l'examen n'était pas prédéfinit
            {
                //on commence par enregistrer le nouveau lieu
                //on vérifie ensuite s'il existe déjà dans la base de données
                if (!ExamAccess.lieuExiste(Lieux.Ville, "nb", "canada"))
                {
                    ExamAccess.setLieuxExamens(Lieux.Adresse, Lieux.Ville, "nb", "canada");
                    //ensuite on récupère l'id du nouveau lieux enregistré pour le spécifier dans la reservation
                    // eet on save enfin
                    save(typeExam, ExamAccess.idLieuxExam(Lieux.Ville, "nb", "canada"));
                }
                else
                    throw new Exception("La ville que vous essayez d'enregistrer existe déjà. Verifiez dans la liste déroulante");
            }
        }
        else if (Lieux.InOutSelectedValue == "hnb")//même place mais hors du nouveau brunswick
        {
            if (ExamHnb.YorNExamBfr == "oui")//il a déjà eu un examen par le passée
            {
                if (ExamHnb.YExamBfr == "oui")//il veut garder le même superviseur
                {
                    //on récupère le cours et l'année durant laquelle il a passé cet examen
                    temp = ExamAccess.getSupId(ExamHnb.SessionCours, ExamHnb.AncienCours, ni.Text);
                }
                else if (ExamHnb.YExamBfr == "non")//il veut avoir un nouveau superviseur
                {
                    //méthode pour enregistrer un nouveau superviseur
                    ExamAccess.setSuperviseur(ExamHnb.Sup.nom, ExamHnb.Sup.prenom, ExamHnb.Sup.titrePoste, ExamHnb.Sup.nomEts, ExamHnb.Sup.couriel, ExamHnb.Sup.phoneJr, ExamHnb.Sup.adresse, ExamHnb.Sup.langue, "Non Aprv.", false);
                    temp = ExamAccess.getSupId(ExamHnb.Sup.couriel);//ensuite on prend son id
                }
                else
                    throw new Exception("Vous devez spécifier si vous avez déjà eu un superviseur ou pas dans le passée?");
            }
            else if (ExamHnb.YorNExamBfr == "non")//il n'a jamais eu un superviseur par le passé
            {
                //méthode pour enregistrer un nouveau !
                ExamAccess.setSuperviseur(ExamHnb.Sup.nom, ExamHnb.Sup.prenom, ExamHnb.Sup.titrePoste, ExamHnb.Sup.nomEts, ExamHnb.Sup.couriel, ExamHnb.Sup.phoneJr, ExamHnb.Sup.adresse, ExamHnb.Sup.langue, "Non Aprv.", false);
                temp = ExamAccess.getSupId(ExamHnb.Sup.couriel);//ensuite on prend son id
            }
            else
                throw new Exception("Vous devez spécifier si vous avez déjà eu un examen ou pas dans le passée?");

            //on passe ensuite au receuillement des info sur les villes
            if (Lieux.VilleHnb != "autre")
            {
                save(typeExam, int.Parse(Lieux.VilleHnb), temp);
            }
            else //l'endroit spécifié pour faire l'examen n'était pas prédéfinit
            {
                //on commence par enregistrer le nouveau lieu
                //on vérifie ensuite s'il existe déjà dans la base de données
                if (!ExamAccess.lieuExiste(Lieux.Ville, Lieux.Etat, Lieux.Pays))
                {
                    ExamAccess.setLieuxExamens(Lieux.Adresse, Lieux.Ville, Lieux.Etat, Lieux.Pays);
                    //ensuite on récupère l'id du nouveau lieux enregistré pour le spécifier dans la reservation
                    save(typeExam, ExamAccess.idLieuxExam(Lieux.Ville, Lieux.Etat, Lieux.Pays));
                }
                else
                    throw new Exception("La ville que vous essayez d'enregistrer existe déjà. Verifiez dans la liste déroulante");
            }
        }
    }
    //
    protected void confimerReservation()
    {
        try
        {
            if (Nrc == 0)
                throw new Exception("Le sigle du cours ne correspond à aucun cours");
            //on vérifie si ce cours n'a pas encore été confirmé
            //on vérifie si l'étudiant est inscrit
            if (SessionAccess.estInscrit(ni.Text, sessionCourante.Id, Nrc))
            {
                if (chckVille.SelectedIndex == 0)//si les deux examens intra et final se passe au même endroit
                {
                    intraFinalConfirmPlace(mmPlc, mmPlcHnb, "intra");
                    intraFinalConfirmPlace(mmPlc, mmPlcHnb, "final");
                }
                else//les deux aux endroit différents
                {
                    intraFinalConfirmPlace(intraPlc, intraHnb, "intra");
                    intraFinalConfirmPlace(finalPlc, finalHnb, "final");
                }
                errorText.Text = "merci d'avoir confirmé";
                errorText.Visible = true;
            }
            else
                throw new Exception("Vous n'êtes pas inscrit à ce cours à la session actuelle?");
        }
        catch (Exception ex)
        {
            errorText.Text = ex.Message;
            errorText.Visible = true;
        }
    }
    //
    protected void finalBtn_Click(object sender, EventArgs e)
    {
        confimerReservation();
        //enfin on rédirige vers une page de remerciement !
    }
    //
    protected void dfrtEndrtWizard_ActiveStepChanged(object sender, EventArgs e)
    {
        Button cnfrm = dfrtEndrtWizard.FindControl("FinishNavigationTemplateContainerID$FinishButton") as Button;
        if (dfrtEndrtWizard.ActiveStepIndex == 1)
        {
            if (finalPlc.InOutSelectedValue == "nb")
            {
                cnfrm.Visible = false;
                BtnFinal.Visible = true;
                BtnFinal.Enabled = true;
            }
            else if (finalPlc.InOutSelectedValue == "hnb")
            {
                cnfrm.Visible = false;
                BtnFinal.Visible = true;
                BtnFinal.Enabled = true;
            }
            else
            {
                cnfrm.Visible = false;
                BtnFinal.Visible = false;
                BtnFinal.Enabled = false;
            }
        }
    }
}